---
title: Télédétection - Approfondissemnt
author: Marc Lang
html:
  toc: true
  embeb_local_images: true
  embed_svg: true
  cdn: true
  offline: true
export_on_save:
    html: True
---

<center><img src="img/logo_ensat.png" title="" width="50%"></center>
<center><section style="font-size:300%"> Télédétection - Approfondissement </section></center>

<center><section style="font-size:200%; color:#9d1300; width: 65%"> TD - 1 : Opérations sur tableaux numpy </section></center>

<center><section style="font-size:150%"> Marc Lang </section></center>

<br>
<br>
<br>

<span style="font-size:200%"> Table des matières </span>

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=3 orderedList=false} -->
<!-- code_chunk_output -->

- [Préambule](#préambule)
  - [Les données](#les-données)
  - [Quelques conventions](#quelques-conventions)
    - [Les espaces](#les-espaces)
    - [Nom variables](#nom-variables)
    - [Format du fichier](#format-du-fichier)
- [TD 1 - Manipulation de matrices avec Numpy](#td-1-manipulation-de-matrices-avec-numpy)
  - [Un peu de théorie](#un-peu-de-théorie)
    - [Création de matrices](#création-de-matrices)
    - [Propriétés des arrays](#propriétés-des-arrays)
    - [Matrice et opérations](#matrice-et-opérations)
    - [Manipulation des matrices](#manipulation-des-matrices)
    - [Indexation des matrices](#indexation-des-matrices)
  - [Exercices](#exercices)
    - [Exercice 1:  Un peu de pratique](#exercice-1-un-peu-de-pratique)
    - [Exercice 2: cas d'une image](#exercice-2-cas-dune-image)

<!-- /code_chunk_output -->


<!-- import de fichier de style -->



@import "less/question_answer.less"
@import "less/blocks.less"
@import "less/numbered_headsection.less"
@import "less/redish_section.less"
@import "less/block_code_even.less"
@import "less/inline_code_red.less"
@import "less/blue_highlight.less"
@import "less/image_zoom.less"
@import "less/center_table.less"
@import "less/link.less"
@import "less/fig_caption.less"
@import "less/counters.less"
@import "less/bold.less"
@import "less/styling.less"  
@import "less/sidebar_toc.less"


# Préambule

## Les données

Vous pourrez télécharger les données nécessaires à la bonne conduite de ce TD [ici](https://filesender.renater.fr/?s=download&token=65b081a2-49d8-4393-935a-fee9c94de5dc).

## Quelques conventions

Dans ce TD nous respecterons quelques conventions largement admises par la communauté Python. Vous trouverez ci-après un résumé des conventions issues du [pep8](https://www.python.org/dev/peps/pep-0008/) mais d'autres aussi.

Si vous ne les respectez pas, il se peut très bien que votre code fonctionne correctement MAIS votre code ne sera pas forcément très lisible et vous compliquerez la tâche des personnes qui reprendront éventuellement votre code (cette personne peut être votre moi future). C'est tout. Cela peut vous paraître anodin et pénible (ça l'est au début), mais sur des gros codes, le respect de ces conventions fait vraiment une différence. Ainsi, ce qui est présenté ci-dessous sont juste des conventions que je vous invite **fortement** à suivre.

Si vous souhaitez obtenir plus de détails, je vous invite à lire directement la [pep8](https://www.python.org/dev/peps/pep-0008/) ou cet [article](http://sametmax.com/le-pep8-en-resume/) du blog SametMax dont je me suis très largement inspiré.


<div class="block blue-block" >  
<p class="title"> Note : Spyder est votre ami</p>

Je vous propose d'utilise l'IDE (integrated development environment) Spyder dans ces TD. Entre autres, il vous avertira si vous ne respectez pas les conventions, ce qui est bien pratique.

</div>


### Les espaces

Les opérateurs doivent être entourés d’espaces.

Il faut faire ceci :

```python
variable = 'valeur'

ceci == cela

1 + 2
```

Et non:

```python
variable='valeur'

ceci==cela

1+2
```

Il y a deux exceptions notables.

La première étant qu’on groupe les opérateurs mathématiques ayant la priorité la plus haute pour distinguer les groupes :

```python
a = x*2 - 1
b = x*x + y*y
c = (a+b) * (a-b)
```

La seconde est le signe `=` dans la déclaration d’arguments et le passage de paramètres :

```python
def fonction(arg='valeur'): #  ça c'est ok
    pass

resultat = fonction(arg='valeur') #  ça aussi
```

On ne met pas d’espace à l’intérieur des parenthèses, crochets ou accolades.

Oui :

```python
2 * (3 + 4)

def fonction(arg='valeur'):

{str(x): x for x in range(10)}

val = dico['key']
```

Non :
```python
2 * ( 3 + 4 )

def fonction( arg='valeur' ):

{ str(x): x for x in range(10) }

val = dico[ 'key' ]
```

On ne met pas d’espace avant les deux points et les virgules, mais après oui.

Oui :

```python
def fonction(arg1='valeur', arg2=None):

dico = {'a': 1}
```

Non :

```python
def fonction(arg='valeur' , arg2=None) :

dico = {'a' : 1}
```

### Nom variables

Lettres seules, en minuscule : pour les boucles et les indices.

Exemple :

```python
for x in range(10):
    print(x)

i = get_index() + 12
print(ma_liste[i])
```

**Remarque** : j'explicite parfois les indices, surtout quand la boucle est longue et que plusieurs indices sont utilisés, pour les différenciés plus facilement.

Lettres minuscules + underscores : pour les modules, variables, fonctions et méthodes.

```python
une_variable = 10

def une_fonction():
    return None

class UneClasse:
    def une_methode_comme_une_autre(self):
        toto = 1
        return toto
```

Lettres majuscules + underscores : pour les (pseudo) constantes.

```python
MAX_SIZE = 100000  # à mettre après les imports
```

Camel case : nom de classe.

```python
class CeciEstUneClasse:
    def methodiquement(self):
        pass
```

### Format du fichier


#### Indentation

L'identation doit correspondre à quatre espaces (et non une tabulation). Ici, ce n'est pas une convention issue de la PEP8, c'est juste une pratique laaargmeent utilisée dans la comunauté Python.

#### Encoding

L'encoding en UTF8 :

```python
# -*- coding: utf-8 -*-
```


#### Longueur de ligne

Une ligne doit se limiter à 79 charactères. Cette limite, héritée des écrans touts petits, est toujours en vigeur car il est plus facile de scanner un code sur une courte colonne qu’en faisant des aller-retours constants. De plus, cette convention oblige à écrire un code propre.

Si vous dépassez quelques fois dans un fichier, ce n'est pas bien grave, gardez cela comme un idéal à atteindre.

Si une ligne est trop longue, il existe plusieurs manières de la racourcir :

```python
foo = la_chose_au_nom_si_long_quelle_ne_tient_pas_sur(
          une,
          ligne,
          de,
          fichier)
```

Ici l’indentation entre le nom de la fonction et des paramètres est légèrement différente pour mettre en avant la distinction.

Une variante :

```python
foo = la_chose_au_nom_si_long_quelle(ne, tient, pas, sur, une
                                     de, fichier)
```

Si c’est un appel chainé, on peut utiliser `\` pour mettre à la ligne :

```python

mon_image = gdal.Open('mon_fichier.tif', gdal.GA_ReadOnly)\
                .GetRasterBand(1)\
                .ReadAsArray()
```

Si c’est une structure de données, il est possible d'utiliser cette syntaxe.

```python
chiffres = [
    1, 2, 3,
    4, 5, 6,
]

contacts = {
    'Cleo': (),
    'Ramses': (
        ('maman', '0248163264'),
        ('papa', '01234567890'),
        ('mamie', '55555555'),
        ('momie', '066642424269')
    )
}
```


------------

# TD 1 - Manipulation de matrices avec Numpy

## Un peu de théorie

### Création de matrices

#### À partir de listes

##### À une dimension

```python
import numpy as np

# One dimension array
ma_liste = [1, 7, 8, 9]
ma_matrice = np.array(ma_liste)
print(ma_matrice)
```

    [1 7 8 9]


##### À deux dimensions

```python
# Two dimensions array
ma_liste = [[1, 7, 8, 9], [2, 4, 5, 11]]
ma_matrice = np.array(ma_liste)
print(ma_matrice)
```

    [[ 1  7  8  9]
     [ 2  4  5 11]]



#### À partir de fonctions


```python
import numpy as np

# array filled with zeros
a = np.zeros((2,2))
print(a)

# array filled with ones
b = np.ones((1,2))
print(b)

# array filled with a constant
c = np.full((2,2), 7)
print(c)

# Id array
d = np.eye(2)         
print(d)              

# random array
e = np.random.random((2,2))
print(e)  

# Array of evenly spaced values
f = np.arange(12)
print(f)  
# same as
f2 = np.array(range(12))
print(f2)  
```

    [[0. 0.]
     [0. 0.]]

    [[1. 1.]]

    [[7 7]
     [7 7]]

    [[1. 0.]
     [0. 1.]]

    [[0.06639168 0.84830778]
     [0.81326371 0.262063  ]]

    [ 0  1  2  3  4  5  6  7  8  9 10 11]

    [ 0  1  2  3  4  5  6  7  8  9 10 11]



### Propriétés des arrays

#### Type

Matrice est de type `numpy.ndarray` :


```python
a = np.array([[1, 7, 8, 9], [2, 4, 5, 11]])
print(type(a))
```

    <class 'numpy.ndarray'>



Elle contient des valeurs numériques ou booléenes (elle peut contenir autre chose mais nous n'aborderons pas cet aspect là en cours) qui ont eux même leur propre type :


```python
a = np.array([[1, 7, 8, 9], [2, 4, 5, 11]])
print(a)
print(a.dtype)
b = np.array([[1., 7, 8, 9], [2, 4, 5, 11]])
print(b)
print(b.dtype)
c = np.array([[True, False], [False, False]])
print(c)
print(c.dtype)
```

    [[ 1  7  8  9]
     [ 2  4  5 11]]

    int64

    [[ 1.  7.  8.  9.]
     [ 2.  4.  5. 11.]]

    float64

    [[ True False]
     [False False]]

    bool



Notez que l'ensemble des valeurs numériques d'une matrice a le même type. Lors de la création de la matrice, si tous les éléments n'ont pas le même type, numpy va tenter une conversion de certains élements pour rendre uniforme le type des élements.

Il est possible de convertir le type d'une matrice


```python
a = np.array([[1, 0, 1, 1], [0, 1, 2, 3]])
print("Matrice :\n", a)
print("De type :", a.dtype)


print('Conversion en float:\n', a.astype('float64'))
print('Conversion en bool:\n', a.astype('bool'))
```

    Matrice :
     [[1 0 1 1]
     [0 1 2 3]]

    De type : int64

    Conversion en float:
     [[1. 0. 1. 1.]
     [0. 1. 2. 3.]]

    Conversion en bool:
     [[ True False  True  True]
     [False  True  True  True]]

#### Dimensions

Vous pouvez accéder aux dimensions des `arrays` :


```python
a = np.array([[1, 7, 8, 9], [2, 4, 5, 11]])
print('a :\n', a)
print('Shape : ', a.shape)

b = np.random.rand(3, 4, 2)
print('b :\n', b)
print('Shape : ', b.shape)
```

    a :
     [[ 1  7  8  9]
     [ 2  4  5 11]]

    Shape :  (2, 4)

    b :
     [[[0.21999709 0.05882568]
      [0.15336886 0.84246363]
      [0.30747443 0.30996595]
      [0.52395515 0.88165891]]

     [[0.33887074 0.68092741]
      [0.07935638 0.25579979]
      [0.34907562 0.58129004]
      [0.06487302 0.12633039]]

     [[0.52729491 0.93616072]
      [0.73351584 0.9755346 ]
      [0.83580018 0.11191444]
      [0.77974892 0.66292929]]]

    Shape :  (3, 4, 2)



Ici, on a un `array` à deux lignes et deux colonnes et un autre `array` avec trois lignes et quatre colonnes et ce qu'on pourrait appeler deux bandes dans le cas d'une image de télédétection. Notez que dans le deuxième cas il n'est pas évident de déterminer les dimensions en visualisant la sortie du `print()`

Vous pouvez aussi accéder au nombre d'éléments d'une matrice via la commande :

```python
a = np.array([[1, 7, 8, 9], [2, 4, 5, 11]])
print('a :\n', a)
print('Size : ', a.size)
```

    a :
     [[ 1  7  8  9]
     [ 2  4  5 11]]

    Size :  8

### Matrice et opérations

#### Opérateurs communs

Les `np.array` supportent les opérateurs courants `+`,`-`, `/`, `*`, `**` :


```python
a = np.array([[1, 7, 8, 9], [2, 4, 5, 11]])
b = a + 5
print('a :\n', a)
print('b :\n', b)
print('a - b', a - b)
print('a / b', a / b)
print('a**2', a**2)
```

    a :
     [[ 1  7  8  9]
     [ 2  4  5 11]]

    b :
     [[ 6 12 13 14]
     [ 7  9 10 16]]

    a - b :
    [[-5 -5 -5 -5]
     [-5 -5 -5 -5]]

    a / b :
    [[0.16666667 0.58333333 0.61538462 0.64285714]
     [0.28571429 0.44444444 0.5        0.6875    ]]

    a**2 :
    [[  1  49  64  81]
     [  4  16  25 121]]



Les tests sur les matrices renvoient des tableaux de type `bool`:


```python
a = np.array([[1, 7, 8, 9], [2, 4, 5, 11]])
print(a > 4)
print((a > 4).dtype)
```

    [[False  True  True  True]
     [False False  True  True]]

    bool



Pour tester plusieurs conditions il n'est pas possible d'utiliser les opérateurs `and` et `or` comme pour les variables numériques. Si on fait :


```python
print((a > 2) and (a < 8))
```

```python
---------------------------------------------------------------------------

ValueError                                Traceback (most recent call last)

<ipython-input-92-aa18ebe4d7b9> in <module>
----> 1 print((a > 2) and (a < 8))


ValueError: The truth value of an array with more than one element is ambiguous. Use a.any() or a.all()
```

python nous renvoie une erreur. Utiliser dans ce cas les opérateurs `*` à la plance du `and` et `+` à la place du `or` qui permettent d'obtenir le résultat souhaité.


```python
print((a > 2) * (a < 8))
```

    [[False  True False False]
     [False  True  True False]]




```python
print((a < 4) + (a > 7))
```




    array([[ True, False,  True,  True],
           [ True, False, False,  True]])

#### Produits matriciels

La bibliothèque numpy permet aussi de faire des **opérations matricielles**:


```python
a = np.array([[1, 7, 8, 9], [2, 4, 5, 11]])
print('a :\n', a)

b = np.array([[1, 7],
              [8, 9],
              [2, 4],
              [5, 11]])
print('b :\n', b)
print(np.dot(a, b))
print(a.dot(b))
```

    a :
     [[ 1  7  8  9]
     [ 2  4  5 11]]

    b :
     [[ 1  7]
     [ 8  9]
     [ 2  4]
     [ 5 11]]

    [[118 201]
     [ 99 191]]

    [[118 201]
     [ 99 191]]


#### Satistiques sur les matrices


Un certain nombre de fonctions / méthodes sont proposées par numpy pour calculer des statistiques sur des tableaux.


```python
a = np.array([[1, 7, 8, 9], [2, 4, 5, 11]])
print('a :\n', a)

print(a.mean())
print(a.std())
print(a.min())
print(a.max())
```

    a :
     [[ 1  7  8  9]
     [ 2  4  5 11]]

    5.875

    3.2572035551988456

    1

    11



Ces opérations peuvent s'appliquer sur l'ensemble de la matrice mais également par colonne ou par ligne, et plus généralement sur une dimension donnée :


```python
a = np.array([[1, 7, 8, 9], [2, 4, 5, 11]])
print('a :\n',  a)

print('Sur la dimension 0 (i.e. les lignes) :', a.mean(axis=0))
print('Sur la dimension 1 (i.e. les colonnes) :', a.mean(axis=1))
```

    a :
     [[ 1  7  8  9]
     [ 2  4  5 11]]

    Sur la dimension 0 (i.e. les lignes) : [ 1.5  5.5  6.5 10. ]

    Sur la dimension 1 (i.e. les colonnes) : [6.25 5.5 ]



Vous pourrez trouver une liste exhaustive des fonctions mathématiques disponibles sur [la doc de numpy](https://numpy.org/doc/stable/reference/routines.math.html)

### Manipulation des matrices

#### Changement des dimensions


##### méthode Reshape

```python
a = np.arange(12)
print(a)
print('Shape :', a.shape)

print(a.reshape(3, 4))
print('Shape :', a.reshape(3, 4).shape)

print(a.reshape(-1, 12))
print('Shape :', a.reshape(-1, 12).shape)
print(a.reshape(1, -1))
print('Shape :', a.reshape(1, -1).shape)
```

    [ 0  1  2  3  4  5  6  7  8  9 10 11]

    Shape : (12,)

    [[ 0  1  2  3]
     [ 4  5  6  7]
     [ 8  9 10 11]]

    Shape : (3, 4)

    [[ 0  1  2  3  4  5  6  7  8  9 10 11]]

    Shape : (1, 12)

    [[ 0  1  2  3  4  5  6  7  8  9 10 11]]

    Shape : (1, 12)



Notez ici l'utilisation du `-1` : elle permet de définir une dimension dans la fonction `reshape()` et de définir automatiquement la dimension `-1` à partir des autres dimensions indiquées.

##### Ajouter/supprimer une dimension

Il est parfois nécessaire d'ajouter une ou plusieurs dimensions à un tableau sans changer la taille des dimensions actuelles :

```python
a = np.arange(12)
print(a)
a_dim_supp = np.atleast_2d(a)
print(a_dim_supp)
print(a_dim_supp.shape)
a_dim_supp_v2 = np.atleast_3d(a)
print(a_dim_supp_v2)
print(a_dim_supp_v2.shape)
```

    [ 0  1  2  3  4  5  6  7  8  9 10 11]

    [[ 0  1  2  3  4  5  6  7  8  9 10 11]]
    (1, 12)

    [[[ 0]
      [ 1]
      [ 2]
      [ 3]
      [ 4]
      [ 5]
      [ 6]
      [ 7]
      [ 8]
      [ 9]
      [10]
      [11]]]
    (1, 12, 1)


Dans cet exemple, on a transformé un tableau de douze éléments en :
- un tableau avec une ligne et douze colonnes ;
- un tableau avec une ligne et douze colonne et une bande.

Vous noterez que le nombre d'éléments n'a pas changé, il est toujours de douze.

De manière inverse, il est parfois utile de supprimer des dimensions (qu'on considère alors superflues) avec un seul élément (une colonne, ou une bande) :

```python
a = np.arange(12)
print(a)
a_dim_supp = np.atleast_2d(a)
a_dim_supp_v2 = np.atleast_3d(a)

print(a_dim_supp)
print(np.squeeze(a_dim_supp))
print(np.squeeze(a_dim_supp).shape)

print(a_dim_supp_v2)
print(np.squeeze(a_dim_supp_v2))
print(np.squeeze(a_dim_supp_v2.shape))
```

    [ 0  1  2  3  4  5  6  7  8  9 10 11]

    [[ 0  1  2  3  4  5  6  7  8  9 10 11]]
    [ 0  1  2  3  4  5  6  7  8  9 10 11]
    (12,)

    [[[ 0]
      [ 1]
      [ 2]
      [ 3]
      [ 4]
      [ 5]
      [ 6]
      [ 7]
      [ 8]
      [ 9]
      [10]
      [11]]]
    [ 0  1  2  3  4  5  6  7  8  9 10 11]
    [ 1 12  1]


#### Transposée d'une matrice


```python
a = np.arange(12).reshape(3, 4)
print(a)
print('Shape :', a.shape)
print(a.T)
print('Shape :', a.T.shape)
```

    [[ 0  1  2  3]
     [ 4  5  6  7]
     [ 8  9 10 11]]

    Shape : (3, 4)

    [[ 0  4  8]
     [ 1  5  9]
     [ 2  6 10]
     [ 3  7 11]]

    Shape : (4, 3)



#### Trie d'une matrice


```python
a = np.random.rand(6)
print(a)
a.sort()
print('Matrice triée :', a)
```

    [0.23865278 0.42193736 0.52455777 0.7564862  0.29492756 0.74441202]

    Matrice triée : [0.23865278 0.29492756 0.42193736 0.52455777 0.74441202 0.7564862 ]



#### Concaténation des matrices


```python
a = np.arange(4).reshape(-1, 1)
b = np.arange(6, 10).reshape(-1, 1)
print('a :\n', a)
print('b :\n', b)
c = np.concatenate((a, b), axis=0)
print('Concaténation selon la dimension des colonnes :\n', c)
d = np.concatenate((a, b), axis=1)
print('Concaténation selon la dimension des lignes :\n', d)
```

    a :
     [[0]
     [1]
     [2]
     [3]]

    b :
     [[6]
     [7]
     [8]
     [9]]

    Concaténation selon la dimension des colonnes :
     [[0]
     [1]
     [2]
     [3]
     [6]
     [7]
     [8]
     [9]]

    Concaténation selon la dimension des lignes :
     [[0 6]
     [1 7]
     [2 8]
     [3 9]]

<div class="block red-block">
<p class="title"> Erreur courante : </p>

Vous ne pouvez pas concatener selon un axe qui n'existe pas : concatenez deux matrices "ligne" ne pourra pas donner une matrice à 2 dimensions.
```python
a = np.arange(4)
b = np.arange(6, 10)
c = np.concatenate((a, b), axis=1)
```

vous renverra :
```python
---------------------------------------------------------------------------
AxisError                                 Traceback (most recent call last)
<ipython-input-184-9d5d435a544b> in <module>
      1 a = np.arange(4)
      2 b = np.arange(6, 10)
----> 3 c = np.concatenate((a, b), axis=1)

<__array_function__ internals> in concatenate(*args, **kwargs)

AxisError: axis 1 is out of bounds for array of dimension 1
```

Il faut dans ce cas créer "artificiellement" la nouvelle dimension selon laquelle concaténer les deux matrices.
</div>


Vous pourrez trouver une liste exhaustive des fonctions de formattage des matrices sur [la doc de numpy](https://numpy.org/doc/stable/reference/routines.array-manipulation.html)

### Indexation des matrices

#### Accéder à un élement d'une matrice


```python
a = np.arange(12).reshape(3, 4)
print(a)
print('Première ligne, première colonne : ', a[0, 0])
print('Première ligne, quatrième colonne : ', a[0, 3])
print('Troisième ligne, première colonne : ', a[2, 0])
```

    [[ 0  1  2  3]
     [ 4  5  6  7]
     [ 8  9 10 11]]

    Première ligne, première colonne :  0

    Première ligne, quatrième colonne :  3

    Troisième ligne, première colonne :  8



####  Accéder à une ligne / colonne / bande d'une matrice


```python
a = np.arange(12).reshape(3, 4)
print("Tableau d'origine:\n", a)
print('Première ligne, toutes les colonnes:\n', a[0, :])
print('Toutes les lignes, troisième colonne:\n', a[:, 2])

a = np.arange(24).reshape(3, 4, 2)
print("Tableau d'origine:\n", a)
print('Toutes les lignes, toutes les colonnes, deuxième bande:\n', a[:, :, 1])
```

    Tableau d'origine:
     [[ 0  1  2  3]
     [ 4  5  6  7]
     [ 8  9 10 11]]

    Première ligne, toutes les colonnes:
     [0 1 2 3]

    Toutes les lignes, troisième colonne:
     [ 2  6 10]

    Tableau d'origine:
     [[[ 0  1]
      [ 2  3]
      [ 4  5]
      [ 6  7]]

     [[ 8  9]
      [10 11]
      [12 13]
      [14 15]]

     [[16 17]
      [18 19]
      [20 21]
      [22 23]]]

    Toutes les lignes, toutes les colonnes, deuxième bande:
     [[ 1  3  5  7]
     [ 9 11 13 15]
     [17 19 21 23]]



Même **remarque** que précédemment, notez que dans le deuxième cas il n'est pas évident de déterminer les dimensions en visualisant la sortie du `print()`. Le résultat de l'indexation n'est ici pas intuitif pour les tableaux d'une dimension > 2.

Accéder aux derniers élements sans connaître les dimensions:


```python
a = np.arange(12).reshape(3, 4)
print("Tableau d'origine:\n", a)

print('Dernière ligne de toutes les colonnes:\n', a[-1, :])
print('Dernière colonne de toutes les lignes:\n', a[:, -1])
```

    Tableau d'origine:
     [[ 0  1  2  3]
     [ 4  5  6  7]
     [ 8  9 10 11]]

    Dernière ligne de toutes les colonnes:
     [ 8  9 10 11]

    Dernière colonne de toutes les lignes:
     [ 3  7 11]



#### Slicing

##### accéder à une partie d'une ligne ou d'une colonne


```python
a = np.arange(12).reshape(3, 4)
print("Tableau d'origine:\n", a)

print('Ligne 2 à 3, toutes les colonnes:\n', a[1:3, :])
print('De manière équivalente:\n', a[1:, :])

print('Toutes les lignes, colonnes 1 à 2:\n', a[:, 0:2])
print('De manière équivalente:\n', a[:, :2])

print('Ligne 2 à 3, colonnes 1 à 2:\n', a[1:3, 0:2])
print('De manière équivalente:\n', a[1:, :2])

```

    Tableau d'origine:
     [[ 0  1  2  3]
     [ 4  5  6  7]
     [ 8  9 10 11]]

    Ligne 2 à 3, toutes les colonnes:
     [[ 4  5  6  7]
     [ 8  9 10 11]]

    De manière équivalente:
     [[ 4  5  6  7]
     [ 8  9 10 11]]

    Toutes les lignes, colonnes 1 à 2:
     [[0 1]
     [4 5]
     [8 9]]

    De manière équivalente:
     [[0 1]
     [4 5]
     [8 9]]

    Ligne 2 à 3, colonnes 1 à 2:
     [[4 5]
     [8 9]]

    De manière équivalente:
     [[4 5]
     [8 9]]



##### Accéder aux derniers élements sans connaître les dimensions


```python
a = np.arange(12).reshape(3, 4)
print("Tableau d'origine:\n", a)

print('Deux dernières lignes de toutes les colonnes:\n', a[-2:, :])
print('Colonne 2 et 3 de toutes les lignes:\n', a[:, -3:-1])
```

    Tableau d'origine:
     [[ 0  1  2  3]
     [ 4  5  6  7]
     [ 8  9 10 11]]

    Deux dernières lignes de toutes les colonnes:
     [[ 4  5  6  7]
     [ 8  9 10 11]]

    Colonne 2 et 3 de toutes les lignes:
     [[ 1  2]
     [ 5  6]
     [ 9 10]]



##### Accéder à une colonne sur deux:


```python
a = np.arange(12).reshape(3, 4)
print("Tableau d'origine:\n", a)

print('Une colonne sur deux:\n', a[:, 0:4:2])
print('De manière équivalente:\n', a[:, ::2])
```

    Tableau d'origine:
     [[ 0  1  2  3]
     [ 4  5  6  7]
     [ 8  9 10 11]]

    Une colonne sur deux:
     [[ 0  2]
     [ 4  6]
     [ 8 10]]

    De manière équivalente:
     [[ 0  2]
     [ 4  6]
     [ 8 10]]



#### Sélectionner des élements respectant une condition

On a vu plus haut qu'il est possible de tester des conditions sur des matrices :


```python
a = np.arange(12).reshape(3, 4)
print("Tableau d'origine:\n", a)
print("Élements supérieurs à 4 ?:\n", a > 4)
```

    Tableau d'origine:
     [[ 0  1  2  3]
     [ 4  5  6  7]
     [ 8  9 10 11]]

    Élements supérieurs à 4 ?:
     [[False False False False]
     [False  True  True  True]
     [ True  True  True  True]]



Il est possible d'extraire des élements d'une matrice à partir d'une matrice booléene :


```python
a = np.arange(12).reshape(3, 4)
print("Tableau d'origine:\n", a)
print("Élement supérieur à 4 ?:\n", a > 4)
print("Extraction des élements supérieurs à 4 ?:\n", a[a > 4])
```

    Tableau d'origine:
     [[ 0  1  2  3]
     [ 4  5  6  7]
     [ 8  9 10 11]]

    Élement supérieur à 4 ?:
     [[False False False False]
     [False  True  True  True]
     [ True  True  True  True]]

    Extraction des élements supérieurs à 4 ?:
     [ 5  6  7  8  9 10 11]



On remarque au passage que la matrice en sortie est une matrice à une dimension.

Il est ainsi possible d'extraire des valeurs d'une matrice à partir des condions sur une autre matrice. Prenons un tableau de valeurs aléatoires `x`, et une matrice `y` contenant des labels.


```python
x = np.random.rand(12).reshape(4, 3)
print("x:\n", x)
y = np.array([1,0,1,2])
print("y:\n", y)
```

    x:
     [[0.16626827 0.46535642 0.3672172 ]
     [0.86728804 0.734129   0.09177141]
     [0.54021365 0.14108181 0.1128911 ]
     [0.79018679 0.30712204 0.65376371]]

     y:
      [1 0 1 2]

Il est possible d'extraire les lignes de `x` qui correspondent aux éléments de `y` égales à 1 (ou n'importe quelle autre condition) :

```python
x = np.random.rand(12).reshape(4, 3)

y = np.array([1,0,1,2])
print(y==1)
print(x[y==1])
```

    [ True False  True False]

    [[0.46559391 0.4042566  0.42030929]
    [0.36890553 0.08200877 0.15924856]]


## Exercices

Les solutions de chaque question sont disponibles à chaque fois en cliquant sur "Solution". Elles sont cependant cachées dans un premier temps. Vous allez faire des erreurs, c'est normal. Vous retiendrez mieux la syntaxe numpy si vous cherchez les éléments de réponse dans ce document, je vous conseille ainsi de ne pas regarder la solution à la première difficulté rencontrée. Vous pouvez bien sûr regarder la solution si vous bloquez sur une question trop longtemps.


Dans ces exercices il peut exister, comme souvent, plusieurs syntaxes possibles. Je vous propose à chaque question une solution **mais ce n'est pas la seule possible**. Si vous n'avez pas la même, éxécutez les deux codes et observez si le résultat est bien le même.



### Exercice 1:  Un peu de pratique

1. Définir les deux listes suivantes:


```python
liste_1 = [4, 6, 1, 0]
liste_2 = [1, -6, 3, 6]
```

2. À partir de ces listes définir une matrice `a` et `b`
<div class="question">
	<input type="checkbox" id="1.2">
	<p><label for="1.2">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  a = np.array(liste_1)
  b = np.array(liste_2)
  ```
</span>
</div>




3. Définir une matrice `c` donc la première ligne correspond à la liste `liste_1` et la deuxième à la liste `liste_2`
<div class="question">
	<input type="checkbox" id="1.3">
	<p><label for="1.3">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  c = np.array([liste_1, liste_2])
  c
  ```
</span>
</div>


4. Définir une matrice `d` donc la première colonne correspond à la liste `liste_1` et la deuxième à la liste `liste_2`
<div class="question">
	<input type="checkbox" id="1.4">
	<p><label for="1.4">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  d = np.array([liste_1, liste_2]).T
  d
  ```
</span>
</div>

5. Afficher la soustraction de la première colonne par la deuxième colonne de la matrice `d`
<div class="question">
	<input type="checkbox" id="1.5">
	<p><label for="1.5">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  d = np.array([liste_1, liste_2]).T
  print(d[:,0] - d[:,1])
  ```
</span>
</div>

6. Afficher la soustraction de la deuxième ligne par la quatrième ligne de la matrice `d`
<div class="question">
	<input type="checkbox" id="1.6">
	<p><label for="1.6">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  d = np.array([liste_1, liste_2]).T
  print(d[1,:] - d[3,:])
  ```
</span>
</div>

7. Écrire des instuctions qui permettent d'afficher `True` si la moyenne de `d` est supérieure à 2 et `False` sinon.
<div class="question">
	<input type="checkbox" id="1.7">
	<p><label for="1.7">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  d = np.array([liste_1, liste_2]).T
  answer = d.mean() > 2
  print(d.mean())
  print(answer)
  ```
</span>
</div>

8. Calculer l'écart type de chaque colonne de la matrice `d`
<div class="question">
	<input type="checkbox" id="1.8">
	<p><label for="1.8">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  d = np.array([liste_1, liste_2]).T
  stds = d.std(axis = 0)
  print(stds)
  ```
</span>
</div>

9. Écrire des instructions qui renvoient une matrice bouléene contenant la réponse à la question "*la somme de la ligne est-elle paire ?*" pour toutes les lignes de `d`.
<div class="question">
	<input type="checkbox" id="1.9">
	<p><label for="1.9">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  d = np.array([liste_1, liste_2]).T
  sums = d.sum(axis=1)
  answer = sums % 2 == 0
  print(sums)
  print(answer)
  ```
</span>
</div>

10. Remplacer les valeurs supérieures à 2 de la matrice `d` par 777
<div class="question">
	<input type="checkbox" id="1.10">
	<p><label for="1.10">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  d = np.array([liste_1, liste_2]).T
  condition = d > 2
  answer = d.copy()
  answer[condition] = 777
  print(answer)
  ```
</span>
</div>

11. Remplacer les valeurs supérieures à 0 et inférieures à 6 de la matrice `d` par 888.
<div class="question">
	<input type="checkbox" id="1.11">
	<p><label for="1.11">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  d = np.array([liste_1, liste_2]).T
  condition = (d > 0) * (d < 6)
  answer = d.copy()
  answer[condition] = 888
  print(answer)
  ```
</span>
</div>

### Exercice 2: cas d'une image


Nous avons vu dans le précédent module que les images sont des matrices à 3 dimensions. Vous allez maintenant faire ces exercices de manipulation de matrice en faisant systématiquement l'analogie avec une image de télédection :

- La première dimension des matrices correspond aux lignes des images ;
- la deuxième dimension des matrices correspond aux colonnes des images ;
- la troisième dimension des matrices correspond aux bandes des images.

L'imagette (cf figure ci-dessous) que vous allez manipuler correspond à une sous-zone extraite d'une image Pléiades où les bandes correspondent aux domaines spectraux suivants :

- Bande 1 : Rouge
- Bande 2 : Vert
- Bande 3 : Bleu
- Bande 4 : Infra-Rouge

<figure>
	<img src="img/markdown-img-paste-20201124163706299.png" title="Image Pléiades en Infra-rouge couleur" width="40%">
	<figcaption>Imagette en Infra-rouge couleur.</figcaption>
</figure>


Nous n'avons pas encore vu comment charger une image de télédétection sous forme de tableau. Pour le moment, chargez simplement l'imagette `imagette_fabas.npy` qui a été enregistrée au format `.npy` (format numpy) :

```python
image = np.load('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/imagette_fabas.npy')
print(type(image))
```

2. Afficher le nombre de lignes, de colonnes et de bandes cette imagette ?
<div class="question">
	<input type="checkbox" id="2.2">
	<p><label for="2.2">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  print(image.shape)
  print('lignes : ', image.shape[0])
  print('colonnes : ', image.shape[1])
  print('bandes : ', image.shape[2])
  ```
  </span>
</div>

3. Quel est le type de l'image ?
<div class="question">
	<input type="checkbox" id="2.3">
	<p><label for="2.3">
	Solution
	</label></p>
	<span class="reponse">

  ```python  
    print(image.dtype)
  ```
</span>
</div>

4. Quel est le minimum de la bande 1 ?
<div class="question">
	<input type="checkbox" id="2.4">
	<p><label for="2.4">
	Solution
	</label></p>
	<span class="reponse">

  ```python
    print(image[:,:,0].min())
  ```
</span>
</div>

5. Quel est le maximum de la bande 3 ?
<div class="question">
	<input type="checkbox" id="2.5">
	<p><label for="2.5">
	Solution
	</label></p>
	<span class="reponse">

  ```python  
  print(image[:,:,2].max())
  ```
</span>
</div>

6. Quelle est la moyenne de la bande 2 ?
<div class="question">
	<input type="checkbox" id="2.6">
	<p><label for="2.6">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  print(image[:,:,1].mean())
  ```
</span>
</div>

7. Quel est le 98ième quantile (percentile en anglais) de la bande 4 (chercher sur la doc la fonction à appliquer)
<div class="question">
	<input type="checkbox" id="2.7">
	<p><label for="2.7">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  print(np.percentile(image[:, :, 3], 98))
  ```
</span>
</div>

8. Centrer la bande 4
<div class="question">
	<input type="checkbox" id="2.8">
	<p><label for="2.8">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  bande4 = image[:, :, 3]
  bande4_centre = bande4 - bande4.mean()
  print(bande4_centre)
  ```
</span>
</div>

9. Centrer réduire la bande 4
<div class="question">
	<input type="checkbox" id="2.9">
	<p><label for="2.9">
  Solution
	</label></p>
	<span class="reponse">

  ```python
  bande4 = image[:, :, 3]
  bande4_centre = bande4 - bande4.mean()
  bande4_standardise = bande4_centre / bande4.std()
  print(bande_4_standardise)
  ```
</span>
</div>

<div class="block blue-block">
<p class="title"> Rappel : Centrer-réduire (ou standardiser) </p>

Une variable centrée réduite est une variable qui :

- a une esperance $\mu = 0$;
- a un écart type $\sigma = 1$.

Centrer réduire une variable revient à calculer :

$$x = \frac{X - \mu}{\sigma}$$
</div>


10. Centrer réduire l'image (attention, la moyenne et l'écart type de chaque bande ne sont probablement pas les même.
<div class="question">
	<input type="checkbox" id="2.10">
	<p><label for="2.10">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  # moyenne et écart type des bandes
  # on veut moyenner les lignes et les colonnes Donc
  # axis = 1 et 0
  means = image.mean(axis=(0, 1))  
  stds = image.std(axis=(0, 1))
  image_standardise = (image-means) / stds
  print(image_standardise)

  # vous pouvez ainsi vérifier que la nouvelle image
  # a une moyenne de 0 et un écart type de 1
  print(image_standardise.mean(axis=(0,1)))
  print(image_standardise.std(axis=(0,1)))
  ```
</span>
</div>

11. Créer une nouvelle matrice avec l'ordre des bandes suivantes :
      - Bande 1 : Bleu ;
      - Bande 2 : Rouge ;
      - Bande 3 : Vert ;
      - Bande 4 : Infra-Rouge ;
<div class="question">
<input type="checkbox" id="2.11">
<p><label for="2.11">
  Solution
	</label></p>
	<span class="reponse">

  ```python
  n_lignes, n_colonnes, _  = image.shape
  ir = image[:,:, 3].reshape(n_lignes, n_colonnes, 1)
  r = image[:,:, 0].reshape(n_lignes, n_colonnes, 1)
  v = image[:,:, 1].reshape(n_lignes, n_colonnes, 1)
  b = image[:,:, 2].reshape(n_lignes, n_colonnes, 1)
  new_image = np.concatenate((b, v, r, ir), axis=2)
  ```
</span>
</div>


12. Calculer le NDVI de l'image
<div class="question">
	<input type="checkbox" id="2.12">
	<p><label for="2.12">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  ir = image[:,:, 3]
  r = image[:,:, 0]
  ndvi = (ir - r) / (ir + r)
  ```
</span>
</div>

13. L'eau a un ndvi < 0, créer un masque d'eau, c'est à dire une matrice contenant `True` pour chaque "pixel" d'eau.
<div class="question">
	<input type="checkbox" id="2.13">
	<p><label for="2.13">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  ir = image[:,:, 3]
  r = image[:,:, 0]
  ndvi = (ir - r) / (ir + r)
  masque_eau = ndvi < 0
  ```
</span>
</div>

14. Remplacer dans votre matrice de NDVI, les pixels d'eau par une valeur de nodata (par ex : -9999)
<div class="question">
	<input type="checkbox" id="2.14">
	<p><label for="2.14">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  ir = image[:,:, 3]
  r = image[:,:, 0]
  ndvi = (ir - r) / (ir + r)
  masque_eau = ndvi < 0
  ndvi[masque_eau] = -9999
  ```
</span>
</div>

15. Remplacer dans votre matrice image, les pixels d'eau par une valeur de nodata (par ex : -9999)
<div class="question">
	<input type="checkbox" id="2.15">
	<p><label for="2.15">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  ir = image[:,:, 3]
  r = image[:,:, 0]
  ndvi = (ir - r) / (ir + r)
  masque_eau = ndvi < 0
  image[masque_eau] = -9999
  ```
</span>
</div>
