---
title: Télédétection - Approfondissemnt
author: Marc Lang
html:
  toc: true
  embeb_local_images: true
  embed_svg: true
  cdn: true
  offline: true
export_on_save:
    html: True
---


<!-- import de fichier de style -->


@import "less/question_answer.less"
@import "less/blocks.less"
@import "less/numbered_headsection.less"
@import "less/redish_section.less"
@import "less/block_code_even.less"
@import "less/inline_code_red.less"
@import "less/blue_highlight.less"
@import "less/pd_table.less"
@import "less/image_zoom.less"
@import "less/link.less"
@import "less/fig_caption.less"
@import "less/counters.less"
@import "less/bold.less"
@import "less/styling.less"  
@import "less/sidebar_toc.less"

<div>

<center><img src="img/logo_ensat.png" title="" width="50%"></center>
<center><section style="font-size:300%"> Télédétection - Approfondissement </section></center>

<center><section style="font-size:200%; color:#9d1300; width: 65%"> TD - 2 : Opérations sur raster  </section></center>

<center><section style="font-size:150%"> Marc Lang </section></center>

<br>
<br>
<br>

<span style="font-size:200%"> Table des matières </span>

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=3 orderedList=false} -->
<!-- code_chunk_output -->

- [Objectifs](#objectifs)
- [GDAL](#gdal)
- [Ouvrir une image](#ouvrir-une-image)
- [Obtenir des informations](#obtenir-des-informations)
- [Charger l'image sous forme de tableau](#charger-limage-sous-forme-de-tableau)
  - [Charger toute l'image](#charger-toute-limage)
  - [Lire une partie d'une image](#lire-une-partie-dune-image)
- [Ecrire une image](#ecrire-une-image)
- [Bilan](#bilan)

<!-- /code_chunk_output -->

## Données

Vous pourrez télécharger les données nécessaires au bon déroulement de ce TD [ici](https://filesender.renater.fr/?s=download&token=ef74778b-f5c8-4b08-9d85-2939a48847d2).

## GDAL   

Nous utiliserons la librairie ==GDAL== pour la lecture et l'écriture des images.

De manière générale vous fonctionnerez selon les étapes suivantes lorsque vous manipulerez des images avec python :
1. **Ouvrir l'image** ;
2. **Obtenir des informations sur l'image** (nombre de lignes, de bandes, système de projection etc. ) ;
3. **Charger l'image** (ou une partie de l'image) sous forme de matrice ==numpy== ;
4. Faire des traitements (avec ==numpy== par ex, cf [TD1](sigma_labworks_advanced_1.html)) ;
5. **Enregistrer les données**.



## Ouvrir une image

```python
from osgeo import gdal

filename = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/imagette_fabas.tif'
data_set = gdal.Open(filename, gdal.GA_ReadOnly)
type(data_set)
```

    osgeo.gdal.Dataset

<div class="block green-block"><p class="title exercice"> </p>

1. Définissez une fonction `open_image(filename)`qui renvoie une instance de fichier gdal ouvert ;

2. Rajoutez l'affichage de texte dans la fonction pour informer l'utilisateur si l'image a bien été ouverte ou non (Astuce : pour savoir ce qu'il se passe quand une image n'est pas ouverte, essayer de lancer `gdal.Open(filename, GA_ReadOnly)` avec un fichier qui n'existe pas).
<div class="question">
	<input type="checkbox" id="2.1.1">
	<p><label for="2.1.1">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  def open_image(filename):
    """
    Open an image file with gdal

    Paremeters
    ----------
    filename : str
        Image path to open

    Return
    ------
    osgeo.gdal.Dataset
    """
    data_set = gdal.Open(filename, gdal.GA_ReadOnly)

    if data_set is None:
        print('Impossible to open {}'.format(filename))
        exit()
    else:
        print('{} is open'.format(filename))

    return data_set

  filename = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/imagette_fabas.tif'
  data_set = open_image(filename)
  ```
</span>
</div>
<br>
</div>


## Obtenir des informations

Maintenant que l'image est ouverte sous forme de `DataSet` vous pouvez maintenant accéder à certaines propriétés de l'image (certaines propriétés dépendent du format utilisé).


```python
print('Number of columns :', data_set.RasterXSize)
print('Number of lines :', data_set.RasterYSize)
print('Number of bands :', data_set.RasterCount)
```

    Number of columns : 445
    Number of lines : 501
    Number of bands : 4


<div class="block red-block">
<p class="title"> Erreur courante.</p>

Vous pouvez remarquer qu'à la différence des matrices ==numpy==, ==GDAL== utilise la sémantique `x` et `y` dans son API pour définir respectivement les colonnes et les lignes (attention aux confusions donc).
</div>

La taille des pixels (dans l'unité de la projection) et les coordonnées du pixel d'origine (en haut à gauche de l'image - comme pour ==numpy==, ouf) sont contenues dans un `tuple` retourné par la méthode `GetGeoTransform()` :


```python
geotransform = data_set.GetGeoTransform()
print("type de geotransform : {}".format(type(geotransform)))
print("geotransform : {}".format(geotransform))
if geotransform is not None:
    print('Origin = ({}, {})'.format(geotransform[0], geotransform[3]))
    print('Pixel Size = ({}, {})'.format(geotransform[1],geotransform[5]))
```

    type de geotransform : <class 'tuple'>
    geotransform : (527128.0, 2.0, 0.0, 6248618.0, 0.0, -2.0)
    Origin = (527128.0, 6248618.0)
    Pixel Size = (2.0, -2.0)


**Rappel** : les pixels ne sont pas toujours carrés (cf. cours sur les images radar).

Enfin vous pouvez obtenir d'autres informations comme le format de l'image utilisé, la projection ou encore les métadonnées (s'il y en a).


```python
print('Driver : {}'.format(data_set.GetDriver().ShortName))
print('Projection is : {}'.format(data_set.GetProjection()))
print('Metadata : {}'.format(data_set.GetMetadata()))
```

    Driver : GTiff
    Projection is : PROJCS["RGF93 / Lambert-93",GEOGCS["RGF93",DATUM["Reseau_Geodesique_Francais_1993",SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6171"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4171"]],PROJECTION["Lambert_Conformal_Conic_2SP"],PARAMETER["standard_parallel_1",49],PARAMETER["standard_parallel_2",44],PARAMETER["latitude_of_origin",46.5],PARAMETER["central_meridian",3],PARAMETER["false_easting",700000],PARAMETER["false_northing",6600000],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["X",EAST],AXIS["Y",NORTH],AUTHORITY["EPSG","2154"]]
    Metadata : {'AREA_OR_POINT': 'Area'}


**Remarque** : on voit que l'API de ==gdal== ne respecte pas la ==pep8== :/ .

<div class="block green-block"><p class="title exercice"></p>


1. Écrivez une fonction `get_image_dimension(data_set)` qui renvoie dans l'ordre les lignes, les colonnes, les bandes de l'instance `osgeo.gdal.Dataset` en entrée.
<div class="question">
	<input type="checkbox" id="3.1">
	<p><label for="3.1">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  def get_image_dimension(data_set):
      """
      get image dimensions

      Parameters
      ----------
      data_set : osgeo.gdal.Dataset

      Returns
      -------
      nb_col : int
      nb_lignes : int
      nb_band : int
      """

      nb_col = data_set.RasterXSize
      nb_lignes = data_set.RasterYSize
      nb_band = data_set.RasterCount
      print('Number of columns :', nb_col)
      print('Number of lines :', nb_lignes)
      print('Number of bands :', nb_band)

      return nb_col, nb_lignes, nb_band

  nb_col, nb_lignes, nb_band = get_image_dimension(data_set)
  ```
</span>
</div>


2. Écrivez une fonction `get_origin_coordinates(data_set)` qui renvoie les coordonnées du pixel d'origine de l'instance `osgeo.gdal.Dataset` en entrée.
<div class="question">
	<input type="checkbox" id="3.2">
	<p><label for="3.2">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  def get_origin_coordinates(data_set):
      """
      get origin coordinates

      Parameters
      ----------
      data_set : osgeo.gdal.Dataset

      Returns
      -------
      origin_x : float
      origin_y : float
      """
      geotransform = data_set.GetGeoTransform()
      origin_x, origin_y = geotransform[0], geotransform[3]
      print('Origin = ({}, {})'.format(origin_x, origin_y))

      return origin_x, origin_y

  origin_x, origin_y = get_origin_coordinates(data_set)
  ```
</span>
</div>



3. Écrivez une fonction `get_pixel_size(data_set)` qui renvoie la taille des pixels de l'instance `osgeo.gdal.Dataset` en entrée.
<div class="question">
	<input type="checkbox" id="3.3">
	<p><label for="3.3">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  def get_pixel_size(data_set):
      """
      get pixel size

      Parameters
      ----------
      data_set : osgeo.gdal.Dataset

      Returns
      -------
      psize_x : float
      psize_y : float
      """
      geotransform = data_set.GetGeoTransform()
      psize_x, psize_y = geotransform[1],geotransform[5]
      print('Pixel Size = ({}, {})'.format(psize_x, psize_y))

      return psize_x, psize_y

  psize_x, psize_y = get_pixel_size(data_set)
  ```
</span>
</div>
<br>
</div>

## Charger l'image sous forme de tableau

### Charger toute l'image

Le chargement d'une image sous forme de tableau se fait en deux étapes :
- le chargement d'une bande sous forme d'objet de classe `osgeo.gdal.Band` ;
- le chargement de la bande sous forme de tableau.


```python
i = 1  # for band = 1 (indexation starts at 1)
raster_band = data_set.GetRasterBand(i)
print(type(raster_band))
band = raster_band.ReadAsArray()
```

    <class 'osgeo.gdal.Band'>


L'objet `band` est bien un tableau ==numpy== :


```python
print(type(band))
```

    <class 'numpy.ndarray'>


**Remarque** : vous pouvez plus simplement écrire :


```python
i = 1  # for band = 1 (indexation starts at 1)
band = data_set.GetRasterBand(i).ReadAsArray()
```



<div class="block red-block">
<p class="title"> Erreur courante</p>

La méthode `ReadAsArray()` charge par défaut la totalité de l'image dans la RAM. Pous nos TDs, ça ne sera pas un problème, dans la vie d'un télédétecteur qui travaille avec des séries temporelles de plusieurs Gigas, ça peut être un problème. Le chargement peut faire planter votre programme si vous n'avez pas assez de RAM. **Vigilance donc !**
</div>

**Type des bandes**

Il est possible de connaitre le type de donnée d'une bande à partir de l'attribut `DataType` d'un objet de classe `osgeo.gdal.Band`. Cependant, ce type n'est pas très explicite. Pour le convertir en un type lisible, vous pouvez utiliser la fonction `gdal.GetDataTypeName()` :


```python
band = data_set.GetRasterBand(1)
print('Type DataType : {} --> pas très lisible'.format(band.DataType))
print("Band Type = {} --> lisible".format(gdal.GetDataTypeName(band.DataType)))
```

    Type DataType : 2 --> pas très lisible
    Band Type = UInt16 --> lisible


<div class="block blue-block">
<p class="title"> Astuce</p>

Le syntaxe du type des bandes de ==Gdal== et de ==numpy== n'est pas la même. Pour passer du type ==Gdal== à ==numpy==, il faut mettre tous les caractères en minuscule. Pour ce faire, vous pouvez utiliser la méthode `lower()` des chaînes de caractère :


```python
band = data_set.GetRasterBand(1)
gdal_data_type = gdal.GetDataTypeName(band.DataType)
numpy_data_type = gdal_data_type.lower()
print('gdal data type : {}'.format(gdal_data_type))
print('numpy data type : {}'.format(numpy_data_type))
```

    gdal data type : UInt16
    numpy data type : uint16

</div>

Lors de la création d'un tableau numpy, vous pouvez ainsi indiquer le type souhaité en fonction du type de l'image en entrée :


```python
import numpy as np
band = data_set.GetRasterBand(1)
gdal_data_type = gdal.GetDataTypeName(band.DataType)
numpy_data_type = gdal_data_type.lower()

my_array = np.ones((10,10), dtype=numpy_data_type)
print(my_array)
```

    [[1 1 1 1 1 1 1 1 1 1]
     [1 1 1 1 1 1 1 1 1 1]
     [1 1 1 1 1 1 1 1 1 1]
     [1 1 1 1 1 1 1 1 1 1]
     [1 1 1 1 1 1 1 1 1 1]
     [1 1 1 1 1 1 1 1 1 1]
     [1 1 1 1 1 1 1 1 1 1]
     [1 1 1 1 1 1 1 1 1 1]
     [1 1 1 1 1 1 1 1 1 1]
     [1 1 1 1 1 1 1 1 1 1]]


**Exception** :

L'encodage en 8 bits sera `'Byte'` pour ==gdal== tandis qu'il est noté `'uint8'` pour ==numpy==.



<div class="block green-block"><p class="title exercice"></p>

1. Créez une fonction `convert_data_type_from_gdal_to_numpy(gdal_data_type)` qui retourne un type ==numpy== à partir d'un type ==gdal==
    - astuce n°1 : l'entrée et la sortie sont des chaînes de caractère ;
    - astuce n°2 : il faut gérer le cas du type `'uint8'`.

<div class="question">
	<input type="checkbox" id="4.1">
	<p><label for="4.1">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  def convert_data_type_from_gdal_to_numpy(gdal_data_type):
      """
      convert data type from gdal to numpy style

      Parameters
      ----------
      gdal_data_type : str
          Data type with gdal syntax
      Returns
      -------
      numpy_data_type : str
          Data type with numpy syntax
      """
      if gdal_data_type == 'Byte':
          numpy_data_type = 'uint8'
      else:
          numpy_data_type = gdal_data_type.lower()
      return numpy_data_type

  # call example
  print(convert_data_type_from_gdal_to_numpy('Byte'))
  print(convert_data_type_from_gdal_to_numpy('Float64'))

  ```
</span>
</div>

2. Écrivez une fonction `load_img_as_array(filename)` qui charge **toutes** les bandes d'une image à partir du **chemin du fichier**.
    - astuce n°1 : n'hésitez pas à utiliser les fonctions précédemment définies ;
    - astuce n°2 : l'indexation d'un array commence à 0 et celles des bandes à 1 dans l'API gdal ;
    - astuce n°3 : attention au type des données, on supposera que toutes les bandes ont le même type.

<div class="question">
	<input type="checkbox" id="4.2">
	<p><label for="4.2">
	Solution
	</label></p>
	<span class="reponse">


  ```python
  import numpy as np

  def load_img_as_array(filename):
      """
      Load the whole image into an numpy array with gdal

      Paremeters
      ----------
      filename : str
          Path of the input image

      Returns
      -------
      array : numpy.ndarray
          Image as array
      """

      # Get size of output array
      data_set = open_image(filename)
      nb_col, nb_lignes, nb_band = get_image_dimension(data_set)

      # Get data type
      band = data_set.GetRasterBand(1)
      gdal_data_type = gdal.GetDataTypeName(band.DataType)
      numpy_data_type = convert_data_type_from_gdal_to_numpy(gdal_data_type)

      # Initialize an empty array
      array = np.empty((nb_lignes, nb_col, nb_band), dtype=numpy_data_type)

      # Fill the array
      for idx_band in range(nb_band):
          idx_band_gdal = idx_band + 1
          array[:, :, idx_band] = data_set.GetRasterBand(idx_band_gdal).ReadAsArray()

      # close data_set
      data_set = None
      band = None

      return array

  # exemple d'appel
  filename = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/imagette_fabas.tif'
  img = load_img_as_array(filename)
  ```
</span>
</div>
<br>
</div>

### Lire une partie d'une image

Toujours avec la méthode `ReadAsArray()` il est possible de lire la valeur d'un seul pixel :


```python
i = 1  # for band = 1 (indexation starts at 1)
value = data_set.GetRasterBand(i).ReadAsArray(4, 2, 1, 1)
print(value)
```

    [[210]]


Ici, nous avons chargé la valeur du pixel correspondant à la quatrième colonne et la deuxième ligne de l'image.

<div class="block red-block">
<p class="title"> Erreur courante </p>

**N°1 :** comme pour les bandes, l'indexation des lignes et des colonnes commence à 1 (et non pas 0 comme pour numpy).

**N°2 :** on a vu plus haut que les coordonnées d'une image sont exprimées  selon les axes x et y dans l'API de GDAL à l'inverse de numpy qui fonctionne en lignes et colonnes.
</div>


Il est également possible de lire une partie d'une image avec la méthode `ReadAsArray()` : les deux derniers arguments permettent de spécifier (dans l'ordre) le nombre de colonnes et de lignes.

```python
i = 1  # for band = 1 (indexation starts at 1)
sub_array = data_set.GetRasterBand(i).ReadAsArray(4, 2, 6, 10)
print(sub_array)
print('Dimensions of the sub array : {}'.format(sub_array.shape))
```

    [[210 221 243 251 248 256]
     [234 243 259 268 274 291]
     [281 291 296 307 328 350]
     [340 350 355 363 377 386]
     [384 390 395 396 395 398]
     [403 402 403 400 400 409]
     [416 415 415 413 413 425]
     [422 423 431 435 436 444]
     [431 435 445 453 455 462]
     [449 454 454 457 462 470]]
    Dimensions of the sub array : (10, 6)


**Remarque :** lire une image pixel par pixel consomme moins de mémoire vive mais prendra beaucoup plus de temps puisqu'il faudra lire chaque valeur une a une sur le disque dur. Si possible, il est *beaucoup* plus rapide de charger l'image en une seule fois. Si ce n'est pas possible, il faudra travailler par tuile.

## Ecrire une image

Une fois vos traitements réalisés avec numpy vous pouvez enregistrer le résultat dans un fichier. L'écriture d'un fichier se fait en plusieurs étapes :

1. L'initialisation d'un objet `Driver` ;
2. L'initialisation d'une image (`DataSet`) vide à partir l'objet de type `Driver`;
3. Le chargement du tableau numpy dans l'image, bande par bande ;
4. L'écriture à proprement parler dans un ficher.

Il faut d'abord créer une image vide. La création d'une image se fait à partir d'un objet de classe `osgeo.gdal.Driver` qui correspond au format de l'image en sortie.

Nous utiliserons dans ces exemples le format `GTiff`, un format d'image universel. Vous pourrez trouver [ici](https://gdal.org/drivers/raster/index.html) la liste des formats supportés par ==GDAL==.


```python
driver = gdal.GetDriverByName('GTiff')
print(type(driver))
```

    <class 'osgeo.gdal.Driver'>


Une fois l'objet de classe `Driver` créé, vous pouvez créer l'image (objet de classe `Dataset`) à partir de la méthode `Create()`. Il vous faudra renseigner :

- les dimensions de l'image ;
- le chemin du fichier en sortie ;
- le type de l'image.


```python
out_filename = '/tmp/test.tif'
nb_col = 4
nb_ligne = 4
nb_band = 2
gdal_dtype = gdal.GDT_Int32
output_data_set = driver.Create(out_filename, nb_col, nb_ligne, nb_band, gdal_dtype)
print(type(output_data_set))
```

    <class 'osgeo.gdal.Dataset'>


<div class="block blue-block">
<p class="title"> ASTUCE </p>

Vous pourrez trouver ci dessous un dictionnaire avec les correspondances entre les types en chaîne de caractère et les types gdal à renseigner dans la méthode `Create()`


```python
data_type_match = {'uint8': gdal.GDT_Byte,
                   'uint16': gdal.GDT_UInt16,
                   'uint32': gdal.GDT_UInt32,
                   'int16': gdal.GDT_Int16,
                   'int32': gdal.GDT_Int32,
                   'float32': gdal.GDT_Float32,
                   'float64': gdal.GDT_Float64}
```
</div>

Maintenant l'image vide créée, vous pouvez renseigner le système de projection ainsi que le `Geotransform` (taille du pixel, coordonnées du pixel à l'origine etc.) :


```python
output_data_set.SetGeoTransform(data_set.GetGeoTransform())  # "copy" the previous geotransform
output_data_set.SetProjection(data_set.GetProjection()) # "copy" the previous projection
```

    0


Enfin, vous pouvez écrire une matrice numpy bande par bande. De manière analogue à `ReadAsArray()`, la classe `Band` possède une méthode `WriteArray()` qui permet de charger un tableau numpy dans l'objet (`output_band` dans le code ci dessous). Il faut ensuite l'écrire sur le disque dur avec la méthode `FlushCache()` :


```python
img_test = np.arange(32).reshape(nb_ligne, nb_col, nb_band)

for idx_band in range(nb_band):
    output_band = output_data_set.GetRasterBand(idx_band + 1)
    output_band.WriteArray(img_test[:, :, idx_band])  # load img_test[:,:,output_band] in output_band
    output_band.FlushCache()  # write output_band on the hard drive

del output_band
output_data_set = None
```

<div class="block green-block"><p class="title exercice"></p>

Vous aller devoir définir une fonction qui permette d'écrire un tableau numpy dans un fichier. Vous allez améliorer votre fonction au fur et à mesure.

1. Faites une première fonction avec comme paramètres d'entrée :
    - un tableau ;
    - un format d'image sous forme de chaîne de charactère ;
    - un nom de fichier en sortie ;
    - le nombre de lignes, colonnes et bandes de l'image à écrire ;
    - le type de l'image à écrire ;
    - les information de projection et de "geo-transform".

<div class="question">
	<input type="checkbox" id="5.1">
	<p><label for="5.1">
	Solution
	</label></p>
	<span class="reponse">


  ```python
  def write_image(out_filename, array, nb_col, nb_ligne, nb_band, gdal_dtype,
                  transform, projection, driver_name):
      """
      Write a array into an image file.

      Parameters
      ----------
      out_filename : str
          Path of the output image.
      array : numpy.ndarray
          Array to write
      nb_col : int
      nb_ligne : int
      nb_band : int
      gdal_dtype : int
          Gdal data type (e.g. : gdal.GDT_Int32)
      transform : tuple
          GDAL Geotransform information same as return by
          data_set.GetGeoTransform()
      projection : str
          GDAL projetction information same as return by
          data_set.GetProjection()
      driver_name : str
          Any driver supported by GDAL
      Returns
      -------
      None
      """
      # Create DataSet
      driver = gdal.GetDriverByName(driver_name)
      output_data_set = driver.Create(out_filename, nb_col, nb_ligne, nb_band,
                                      gdal_dtype)
      output_data_set.SetGeoTransform(transform)
      output_data_set.SetProjection(projection)
      for idx_band in range(nb_band):
          output_band = output_data_set.GetRasterBand(idx_band + 1)
          output_band.WriteArray(array[:, :, idx_band])
          output_band.FlushCache()

      del output_band
      output_data_set = None

  # call example :

  # define parameters
  filename = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/imagette_fabas.tif'
  data_set = open_image(filename)
  nb_col, nb_ligne, nb_band = get_image_dimension(data_set)
  transform = data_set.GetGeoTransform()
  projection = data_set.GetProjection()
  driver_name = 'Gtiff'
  out_filename = '/tmp/test.tif'
  gdal_dtype = gdal.GDT_Int32

  # "do some processing"
  img_test = np.ones((nb_ligne, nb_col, nb_band))

  # write it
  write_image(out_filename, img_test, nb_col, nb_ligne, nb_band, gdal.GDT_Int32,
              transform, projection, driver_name)
  ```
</span>
</div>

2. La version 1 n'est pas très pratique car il faut systématiquement préciser les dimensions de l'image. Améliorez votre précédente fonction de telle sorte que :
    - les informations de dimensions de l'image aient des valeurs `None` par défaut ;
    - si l'utilisateur ne renseigne pas ces paramètres à l'appel de la fonction, la fonction les déduit automatiquement à partir du tableau en entrée.


<div class="question">
	<input type="checkbox" id="5.2">
	<p><label for="5.2">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  def write_image(out_filename, array, gdal_dtype, transform, projection, driver_name,
                  nb_col=None, nb_ligne=None, nb_band=None):
      """
      Write a array into an image file.

      Parameters
      ----------
      out_filename : str
          Path of the output image.
      array : numpy.ndarray
          Array to write
      nb_col : int (optional)
          If not indicated, the function consider the `array` number of columns
      nb_ligne : int (optional)
          If not indicated, the function consider the `array` number of rows
      nb_band : int (optional)
          If not indicated, the function consider the `array` number of bands
      gdal_dtype : int
          Gdal data type (e.g. : gdal.GDT_Int32)
      transform : tuple
          GDAL Geotransform information same as return by
          data_set.GetGeoTransform()
      projection : str
          GDAL projetction information same as return by
          data_set.GetProjection()
      driver_name : str
          Any driver supported by GDAL
      Returns
      -------
      None
      """
      # Get information from array if the parameter is missing
      nb_col = nb_col if nb_col is not None else array.shape[1]
      nb_ligne = nb_ligne if nb_ligne is not None else array.shape[0]
      nb_band = nb_band if nb_band is not None else array.shape[2]

      # Create DataSet
      driver = gdal.GetDriverByName(driver_name)
      output_data_set = driver.Create(out_filename, nb_col, nb_ligne, nb_band,
                                      gdal_dtype)
      output_data_set.SetGeoTransform(transform)
      output_data_set.SetProjection(projection)

      # Fill it and write image
      for idx_band in range(nb_band):
          output_band = output_data_set.GetRasterBand(idx_band + 1)
          output_band.WriteArray(array[:, :, idx_band])
          output_band.FlushCache()

      del output_band
      output_data_set = None

  # call example :

  # define parameters
  filename = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/imagette_fabas.tif'
  data_set = open_image(filename)
  transform = data_set.GetGeoTransform()
  projection = data_set.GetProjection()
  driver_name = 'Gtiff'
  out_filename = '/tmp/test.tif'
  gdal_dtype = gdal.GDT_Int32

  # "do some processing"
  nb_col, nb_ligne, nb_band = get_image_dimension(data_set)
  img_test = np.ones((nb_ligne, nb_col, nb_band))

  # write it
  write_image(out_filename, img_test, gdal.GDT_Int32,
              transform, projection, driver_name)
  ```
</span>
</div>

3. On peut encore améliorer notre fonction : modifiez la version précédente de telle sorte que :
    - au lieu de renseigner le type de sortie, la projection, le geotransform et le driver systématiquement, il soit possible de renseigner un nouveau paramètre `data_set`  à partir duquel obtenir ces informations (comme pour les dimension de l'image) ;
    - **Astuce** : vous pouvez obtenir le nom en chaine de caractère d'un driver via l'attribut `DataSet.GetDriver().ShortName`.

<div class="question">
	<input type="checkbox" id="5.3">
	<p><label for="5.3">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  def write_image(out_filename, array, data_set=None, gdal_dtype=None,
                  transform=None, projection=None, driver_name=None,
                  nb_col=None, nb_ligne=None, nb_band=None):
      """
      Write a array into an image file.

      Parameters
      ----------
      out_filename : str
          Path of the output image.
      array : numpy.ndarray
          Array to write
      nb_col : int (optional)
          If not indicated, the function consider the `array` number of columns
      nb_ligne : int (optional)
          If not indicated, the function consider the `array` number of rows
      nb_band : int (optional)
          If not indicated, the function consider the `array` number of bands
      data_set : osgeo.gdal.Dataset
          `gdal_dtype`, `transform`, `projection` and `driver_name` values
          are infered from `data_set` in case there are not indicated.
      gdal_dtype : int (optional)
          Gdal data type (e.g. : gdal.GDT_Int32).
      transform : tuple (optional)
          GDAL Geotransform information same as return by
          data_set.GetGeoTransform().
      projection : str (optional)
          GDAL projetction information same as return by
          data_set.GetProjection().
      driver_name : str (optional)
          Any driver supported by GDAL. Ignored if `data_set` is indicated.
      Returns
      -------
      None
      """
      # Get information from array if the parameter is missing
      nb_col = nb_col if nb_col is not None else array.shape[1]
      nb_ligne = nb_ligne if nb_ligne is not None else array.shape[0]
      array = np.atleast_3d(array)  # not asked in the instructions.
                                    # but it deals with the case a 2d
                                    # dimension array is passed.
      nb_band = nb_band if nb_band is not None else array.shape[2]


      # Get information from data_set if provided
      transform = transform if transform is not None else data_set.GetGeoTransform()
      projection = projection if projection is not None else data_set.GetProjection()
      gdal_dtype = gdal_dtype if gdal_dtype is not None \
          else data_set.GetRasterBand(1).DataType
      driver_name = driver_name if driver_name is not None \
          else data_set.GetDriver().ShortName

      # Create DataSet
      driver = gdal.GetDriverByName(driver_name)
      output_data_set = driver.Create(out_filename, nb_col, nb_ligne, nb_band,
                                      gdal_dtype)
      output_data_set.SetGeoTransform(transform)
      output_data_set.SetProjection(projection)

      # Fill it and write image
      for idx_band in range(nb_band):
          output_band = output_data_set.GetRasterBand(idx_band + 1)
          output_band.WriteArray(array[:, :, idx_band])  # not working with a 2d array.
                                                         # this is what np.atleast_3d(array)
                                                         # was for
          output_band.FlushCache()

      del output_band
      output_data_set = None

  # call example :

  # define parameters
  filename = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/imagette_fabas.tif'
  data_set = open_image(filename)
  out_filename = '/tmp/test.tif'

  # "do some processing"
  nb_col, nb_ligne, nb_band = get_image_dimension(data_set)
  img_test = np.ones((nb_ligne, nb_col, nb_band))

  # write it
  write_image(out_filename, img_test, data_set=data_set)
  ```
</span>
</div>
<br>
</div>


## Un peu de pratique

Vous avez maintenant défini des fonctions qui vous permettent de faire les étapes suivantes :

1. **Ouvrir l'image**  ---> `open_image()`;
2. **Obtenir des informations sur l'image** (nombre de lignes, de bandes, système de projection etc. )  ---> `get_image_dimension()`; `get_origin_coordinates()`, `get_image_dimension()`,  `get_pixel_size()`,
3. **Charger l'image** (ou une partie de l'image) sous forme de matrice ==numpy== ---> `load_img_as_array()`;
4. Faire des traitements (avec ==numpy== par ex);
5. **Enregistrer les données** --> `write_image()`.

Lors de futurs traitements, l'utilisation de ce genre de fonctions n'est pas obligatoire. Elle permet cependant d'avoir un code plus clair et plus facile à écrire. Par exemple vous pouvez facilement calculer le ndvi avec le bout de code suivant :


```python
data_type_match = {'uint8': gdal.GDT_Byte,
                   'uint16': gdal.GDT_UInt16,
                   'uint32': gdal.GDT_UInt32,
                   'int16': gdal.GDT_Int16,
                   'int32': gdal.GDT_Int32,
                   'float32': gdal.GDT_Float32,
                   'float64': gdal.GDT_Float64}
# define parameters
filename = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/imagette_fabas.tif'
out_ndvi_filename = '/tmp/mon_ndvi.tif'

# open and load data
data_set = open_image(filename)
img = load_img_as_array(filename)

# do some processingwith numpy: compute ndvi
ndvi = # ....

# write it
write_image(out_ndvi_filename, ndvi, data_set=data_set)
```

Le code sans fonction ci-dessous fonctionnera de la même manière, mais il aura été plus long à écrire, et il est un peu moins compréhensible :

```python
import gdal

# --- define parameters
filename = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/imagette_fabas.tif'
out_ndvi_filename = '/tmp/mon_ndvi.tif'

# --- open data
data_set = gdal.Open(filename, gdal.GA_ReadOnly)

# --- Get information
geotransform = data_set.GetGeoTransform()
projection = data_set.GetProjection()
nb_col = data_set.RasterXSize
nb_lignes = data_set.RasterYSize
nb_band = data_set.RasterCount
band = data_set.GetRasterBand(i).ReadAsArray()
band = data_set.GetRasterBand(1)
gdal_dtype = gdal.GetDataTypeName(band.DataType)

# --- Load data
# Initialize an empty array
array = np.empty((nb_lignes, nb_col, nb_band))

# Fill the array
for idx_band in range(nb_band):
    idx_band_gdal = idx_band + 1
    array[:, :, idx_band] = data_set.GetRasterBand(idx_band_gdal).ReadAsArray()

# close data_set
data_set = None
band = None

# --- do some processingwith numpy: compute ndvi
ndvi = # ....


# --- Write data
driver = gdal.GetDriverByName(driver_name)
output_data_set = driver.Create(out_ndvi_filename, nb_col, nb_ligne, nb_band,
                                gdal_dtype)
output_data_set.SetGeoTransform(geotransform)
output_data_set.SetProjection(projection)
output_band = output_data_set.GetRasterBand(1)
output_band.WriteArray(ndvi)
output_band.FlushCache()
del output_band
output_data_set = None
```

<div class="block blue-block">
<p class="title"> ASTUCE : des fonctions ? et pourquoi pas des classes ? </p>

On peut tout à fait imaginer la création d'une classe `Raster` qui permette une lecture et une écriture encore plus simple.

La logique est souvent la même : si on passe plus de temps à développer des fonctions ou des classes qui complexifient le code côté bibliothèque, c'est pour ensuite faciliter la tâche de l'utilisateur final (vous, mais pourquoi pas d'autres). Sur des petits bouts de code comme nos exemples, ça peut paraître superflu, mais pour des gros codes, ça peut tout changer.

Un autre avantage de l'encapsulation est que si vous changez demain la manière d'écrire un fichier, vous avez juste à changer la définition de la fonction `write_image()` sans changer tous les codes dans lesquels il y a une écriture d'image. C'est transparent pour l'utilisateur final (jusqu'à un certain point).

En pratique, toute personne qui utilise des bibliothèques Python pour le traitement d'image a développé ce genre de fonctions génériques de son côté.

</div>


<div class="block green-block"><p class="title exercice"> </p>

À l'aide des fonctions que vous avez créées, écrivez les instructions permettant de :

1. Créez une image NDVI à partir de l'image `imagette_fabas.tif`, nommez la couche résultante : `imagette_fabas_ndvi.tif`
<div class="question">
	<input type="checkbox" id="6.1">
	<p><label for="6.1">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import os
  data_type_match = {'uint8': gdal.GDT_Byte,
                     'uint16': gdal.GDT_UInt16,
                     'uint32': gdal.GDT_UInt32,
                     'int16': gdal.GDT_Int16,
                     'int32': gdal.GDT_Int32,
                     'float32': gdal.GDT_Float32,
                     'float64': gdal.GDT_Float64}

  # define parameters
  dirname = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/'
  out_dirname = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/results/'
  filename = os.path.join(dirname, 'imagette_fabas.tif')
  out_ndvi_filename = os.path.join(out_dirname, 'imagette_fabas_ndvi.tif')

  # load data
  data_set = open_image(filename)
  img = load_img_as_array(filename)

  # do some processingwith numpy: compute ndvi
  ir = img[:,:, 3].astype('float32')
  r = img[:,:, 0].astype('float32')
  ndvi = (ir - r) / (ir + r)

  # write it
  write_image(out_ndvi_filename, ndvi, data_set=data_set,
              gdal_dtype=data_type_match['float32'])
  ```
</span>
</div>

2. Créez une nouvelle image avec l'ordre des bandes suivant :

    - Bande 1 : Bleu
    - Bande 2 : Rouge
    - Bande 3 : Vert
    - Bande 4 : Infra-Rouge

    Nommez la couche résultante : `imagette_fabas_reordered.tif`

<div class="question">
	<input type="checkbox" id="6.2">
	<p><label for="6.2">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import os

  # define parameters
  dirname = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/'
  out_dirname = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/results/'
  filename = os.path.join(dirname, 'imagette_fabas.tif')
  out_filename = os.path.join(out_dirname, 'imagette_fabas_reordered.tif')

  # load data
  data_set = open_image(filename)
  img = load_img_as_array(filename)
  n_lignes, n_colonnes, nb_bands  = img.shape

  # ----- do some processingwith numpy: reorder

  # V1 : it's ok becouse we only have 4 bands
  ir = img[:,:, 3]
  ir = ir[:,:, np.newaxis]
  r = img[:,:, 0]
  r = r[:,: , np.newaxis]
  v = img[:,:, 1]
  v = v[:,: , np.newaxis]
  b = img[:,:, 2]
  b = b[:,: , np.newaxis]
  new_image = np.concatenate((b, v, r, ir), axis=2)

  # V2 : better if you have numerous bands to deal with
  new_order = [2, 1, 0, 3]
  new_image = np.atleast_3d(img[:,:, new_order[0]])
  for b in new_order[1:]:
      band = np.atleast_3d(img[:,:, b])
      new_image = np.append(new_image, band, axis=2)

  # write it
  write_image(out_filename, new_image, data_set=data_set)
  ```
</span>
</div>


3. L'eau a un ndvi < 0, créez un image de masque d'eau, c'est à dire une image contenant des 1 pour chaque pixel d'eau et 0 ailleurs. Nommez la couche résultante : `masque_eau.tif`

<div class="question">
	<input type="checkbox" id="6.3">
	<p><label for="6.3">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import os
  data_type_match = {'uint8': gdal.GDT_Byte,
                     'uint16': gdal.GDT_UInt16,
                     'uint32': gdal.GDT_UInt32,
                     'int16': gdal.GDT_Int16,
                     'int32': gdal.GDT_Int32,
                     'float32': gdal.GDT_Float32,
                     'float64': gdal.GDT_Float64}

  # define parameters
  dirname = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/results/'
  out_dirname = dirname
  filename = os.path.join(dirname, 'imagette_fabas_ndvi.tif')
  out_filename = os.path.join(out_dirname, 'masque_eau.tif')

  # load data
  data_set = open_image(filename)
  img = load_img_as_array(filename)


  # do some processingwith numpy: build a mask
  nb_col, nb_ligne, nb_band = get_image_dimension(data_set)
  masque_eau = img < 0

  # write it
  write_image(out_filename, masque_eau, data_set=data_set,
              gdal_dtype=data_type_match['uint8'])
  ```
</span>
</div>

4. Masquez les pixels d'eau de l'image de ndvi (par une valeur de nodata de -9999 par ex). Nommez la couche résultante : `imagette_fabas_ndvi_masked.tif`

<div class="question">
	<input type="checkbox" id="6.4">
	<p><label for="6.4">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import os
  data_type_match = {'uint8': gdal.GDT_Byte,
                     'uint16': gdal.GDT_UInt16,
                     'uint32': gdal.GDT_UInt32,
                     'int16': gdal.GDT_Int16,
                     'int32': gdal.GDT_Int32,
                     'float32': gdal.GDT_Float32,
                     'float64': gdal.GDT_Float64}

  # define parameters
  dirname = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/results/'
  out_dirname = dirname
  ndvi_filename = os.path.join(dirname, 'imagette_fabas_ndvi.tif')
  eau_filename = os.path.join(dirname, 'masque_eau.tif')
  out_filename = os.path.join(out_dirname, 'imagette_fabas_ndvi_masked.tif')

  # load data
  ndvi_data_set = open_image(ndvi_filename)
  ndvi = load_img_as_array(ndvi_filename)
  masque_eau = load_img_as_array(eau_filename)
  masque_eau = masque_eau.astype('bool')  # convert to boolean format

  # do some processing with numpy: mask the ndvi image
  nb_col, nb_ligne, nb_band = get_image_dimension(data_set)
  ndvi_masked = ndvi.copy()
  ndvi_masked[masque_eau] = -999

  # write it
  write_image(out_filename, ndvi_masked, data_set=ndvi_data_set)
  ```
</span>
</div>


5. Masquez les pixels d'eau de l'image d'origine (par une valeur de NoData de -9999 par ex). Nommez la couche résultante : `imagette_fabas_masked.tif`

<div class="question">
	<input type="checkbox" id="6.5">
	<p><label for="6.5">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import os
  import gdal
  data_type_match = {'uint8': gdal.GDT_Byte,
                     'uint16': gdal.GDT_UInt16,
                     'uint32': gdal.GDT_UInt32,
                     'int16': gdal.GDT_Int16,
                     'int32': gdal.GDT_Int32,
                     'float32': gdal.GDT_Float32,
                     'float64': gdal.GDT_Float64}

  # define parameters
  dirname = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/'
  out_dirname = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/results/'
  img_filename = os.path.join(dirname, 'imagette_fabas.tif')
  eau_filename = os.path.join(out_dirname, 'masque_eau.tif')
  out_filename = os.path.join(out_dirname, 'imagette_fabas_masked.tif')

  # load data
  img_data_set = open_image(img_filename)
  img = load_img_as_array(img_filename)
  masque_eau = load_img_as_array(eau_filename)
  masque_eau = masque_eau.astype('bool')  # convert to boolean format
  masque_eau = np.squeeze(masque_eau)  # supress the third dimension so
                                       # that it can be use img_masked

  # do some processing with numpy: mask the image
  img_masked = img.copy()
  img_masked[masque_eau] = 65535  # we want to keep the result as type uint16
                                  # so -999 is no longer an option
  img_masked = img_masked

  # write it
  write_image(out_filename, img_masked, data_set=img_data_set)
  ```
</span>
</div>

**Bonus**

6. Écrivez une nouvelle image, dans laquelle on pourra lire vos initiales dans l'étang (Vérifiez vos résultats dans QGIS).
   1. Commencez par une image monobande ;
   2. Puis refaites l'opération sur une image multi-bandes. "Dessinez" vos initiales de telle sorte qu'elles apparaissent en jaune dans une composition infra-rouge couleur.   

<br>
</div>
