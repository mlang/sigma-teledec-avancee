---
title: Page d'acceuil
author: Marc Lang
html:
  toc: false
  embeb_local_images: true
  embed_svg: true
  cdn: true
export_on_save:
    html: True
---

<center><img src="img/logo_ensat.png" title="" width="50%"></center>

<center><section style="font-size:300%"> Télédétection - Approfondissement </section></center>

<center><section style="font-size:200%; color:#9d1300; width: 65%"> SIGMA 2020-2021 </section></center>

<center><section style="font-size:150%"> Marc Lang </section></center>


<p> <br> <br></p>


@import "less/all_table_pd_like.less"
@import "less/table_pd_like.less"
@import "less/redish_section.less"
@import "less/link.less"
@import "less/blue_highlight.less"
@import "less/inline_code_red.less"
@import "less/iframe.less"

## Planning

<div class="my_table">

| Date                               | Durée |             Sujet             |
|:-----------------------------------|:-----:|:-----------------------------:|
| Lu 7 décembre 2020 - 8h30 - 12h30  |  4h   | Opérations sur tableaux numpy |
| Lu 14 décembre 2020 - 8h30 - 12h30 |  4h   |     Opérations sur raster     |
| Lu 4 Janvier 2020 - 13h30 - 17h30  |  4h   |   Classification - partie 1   |
| Me 6 Janvier 2020 - 8h30 - 12h30   |  4h   |   Classification - partie 2   |
| Me 13 Janvier 2020 - 8h30 - 12h30  |  4h   |         Visualisation         |
| Ve 15 Janvier 2020 - 13h30 - 15h30 |  2h   |          Évaluation           |

></div>


## Pré-requis

- [Installation des packages et OTB](installation.html)

## Travaux dirigés

- [Séance n°1](sigma_labworks_advanced_1.html)
- [Séance n°2](sigma_labworks_advanced_2.html)
- [Séance n°3](sigma_labworks_advanced_3.html)
- [Séance n°4](sigma_labworks_advanced_4.html)
- [Séance n°5](sigma_labworks_advanced_5.html)
