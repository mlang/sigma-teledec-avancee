---
title: Télédétection - Approfondissement
author: Marc Lang
html:
  toc: true
  embeb_local_images: true
  embed_svg: true
  cdn: true
  offline: true
export_on_save:
    html: True
---


<!-- import de fichier de style -->


@import "less/question_answer2.less"
@import "less/blocks.less"
@import "less/numbered_headsection.less"
@import "less/redish_section.less"
@import "less/block_code_even.less"
@import "less/inline_code_red.less"
@import "less/blue_highlight.less"
@import "less/pd_table.less"
@import "less/iframe.less"
@import "less/image_zoom.less"
@import "less/link.less"
@import "less/fig_caption.less"
@import "less/counters.less"
@import "less/bold.less"
@import "less/styling.less"  
@import "less/sidebar_toc.less"


<center><img src="img/logo_ensat.png" title="" width="50%"></center>
<center><section style="font-size:300%"> Télédétection - Approfondissement </section></center>

<center><section style="font-size:200%; color:#9d1300; width: 65%"> Pré-requis </section></center>

<br>
<center><section style="font-size:150%"> Marc Lang </section></center>


<p> <br> <br></p>



## Installation d'OTB

Vous pouvez télécharger le logiciel [ici](https://www.orfeo-toolbox.org/download/) et suivre les instructions d'installation [ici](https://www.orfeo-toolbox.org/CookBook/Installation.html).

**Note** : sur windows mieux vaut dézipper le dossier d'installation à un endroit où vous n'avez pas besoin des droits administrateur. Cela vous facilitera la vie plus tard.  

## Préparation de l'environnement de travail python


### Installation d'Anaconda

Pour nos TDs nous allons avoir besoin de plusieurs librairies python en plus des applications OTB. :

- gdal
- numpy
- pandas
- geopandas
- scikit-learn
- matplotlib
- plotly
- museotoolbox

Pour les installer, nous allons utiliser anaconda qui permet de gérer facilement l'installation des packages python. Anaconda vous permettra également d'utiliser l'IDE spyder et les notebooks ipython (ils sont inclus dans l'installation).

Pour tout système d'exploitation :

1. Téléchargez anaconda ([ici](https://www.anaconda.com/products/individual#Downloads)) ;
2. Installez anaconda.
3. Ouvrez Anaconda Navigator
3. Vérifiez que le channel `conda-forge` est bien dans votre liste de channels (voir Figure ci dessous), sinon rajoutez le.

<img src="img/installation/conda_forge.png" title="" width="100%">


### Création d'un nouvel environnement de travail

Certaines des librairies sont directement accessibles depuis l'installation d'anaconda par défaut, d'autres non. Je vous propose donc de vous créer un nouvel environnement anaconda contenant toutes les librairies nécessaires aux TDs. Vous pourrez télécharger <a href="https://mlang.frama.io/sigma-teledec-avancee/sigma.yml" download="sigma.yml">ici</a> un fichier `sigma.yml` vous permettant de les installer automatiquement.

1. Pour ce faire,  ouvrir une instance de "cmd.exe prompt" depuis l'Anaconda Navigator si vous êtes sur windows et un simple terminal si vous êtes sur Linux (ou MacOS).

2. Dans le terminal, taper la commande suivante si vous êtes à l'endroit où vous avez téléchargé le fichier `sigma.yml`:

    ```bash
    conda env create -f sigma.yml
    ```

    sinon il faut mettre le chemin complet (ou chemin relatif):


    ```bash
    conda env create -f  C:\Users\Erudel\Documents\teledec\sigma-teledec-avancee\sigma.yml
    ```

    L'installation de tous les packages peut prendre un peu de temps.

3. Une fois que le nouvel environnement `sigma-env` est créé (le nom est indiqué sur la première ligne du fichier `sigma.yml`, vous pouvez le changer si vous le souhaitez), vous pouvez l'activer via la commande :

    ```bash
    conda activate sigma-env
    ```

4. Vous pouvez maintenant vérifier dans une console python que les packages Python sont bien installés :

    4.1. Ouvrez une console python ou bien spyder depuis votre terminal:

    ```bash
    python
    ```

    ou :

    ```bash
    spyder
    ```

    4.2. Essayez l'import de quelques librairies et vérifiez que vous n'avez pas d'erreurs

    ```python
    from osgeo import gdal
    import pandas
    import geopandas
    import sklearn
    import plotly
    ```

### Configuration d'OTB dans le nouvel environnement de travail

Pour utiliser l'OTB en ligne de commande et via python, il faut configurer certaines variables d'environnement. Il est possible de faire en sorte que ces variables soient configurées à l'activation de votre nouvel envrionnement de travail `sigma-env`.

#### Windows

1. Ouvrir un terminal `cmd.exe` (depuis Anaconda navigator éventuellement)
2. Se déplacer dans le dossier suivant :

    ```bash
    cd C:\chemin\installation\anaconda3\envs\sigma-env\etc\conda\activate.d\
    ```

3. Créer un fichier `otb-var.bat` vide:

    ```bash
    type NUL > .\env_vars.bat
    ```

4. Éditer (avec le bloc note par ex.) le fichier `otb-var.bat` nouvellement créé comme suit (il ne faut pas lancer cette commande dans le terminal mais bien l'écrire de le fichier):

    ```
    call C:\chemin\installation\OTB-7.2.0-Win64\otbenv.bat
    ```

5. Activer (ou quitter puis ré-activer) l'environnement `sigma-env` depuis le terminal ('cmd.exe')

6. Lancer spyder ou une console python depuis le terminal puis exécuter les lignes de code suivantes :

    ```python
    import subprocess

    batcmd = 'otbcli_BandMath'
    # pour python >= 3.7
    result = subprocess.check_output(batcmd, shell=True, stderr=subprocess.STDOUT)
    print(result.decode())
    # sinon
    subprocess.run(batcmd)
    ```

    vous devriez obtenir quelque chose dans ce gout là :

    ```
    ERROR: Waiting for at least one parameter.
    This is the BandMath application, version 6.7.0
    Outputs a monoband image which is the result of a mathematical operation on several multi-band images.
    Complete documentation: https://www.orfeo-toolbox.org/CookBook/Applications/app_BandMath.html or -help
    Parameters:
    MISSING -il       <string list>    Input image list  (mandatory)
    MISSING -out      <string> [pixel] Output Image  [pixel=uint8/uint16/int16/uint32/int32/float/double/cint16/cint32/cfloat/cdouble] (default value is float) (mandatory)
    MISSING -exp      <string>         Expression  (mandatory)
            -ram      <int32>          Available RAM (MB)  (optional, off by default, default value is 256)
            -progress <boolean>        Report progress
            -help     <string list>    Display long help (empty list), or help for given parameters keys
    Use -help param1 [... paramN] to see detailed documentation of those parameters.
    Examples:
    otbcli_BandMath -il verySmallFSATSW_r.tif verySmallFSATSW_nir.tif verySmallFSATSW.tif -out apTvUtBandMathOutput.tif -exp "cos( im1b1 ) > cos( im2b1 ) ? im3b1 : im3b2"
    ```
    cela indique que les applis otb sont bien détectées.

#### Ubuntu

1. Ouvrir un terminal
2. Se déplacer dans le dossier suivant :

    ```bash
    cd '/chemin/installation/anaconda3/envs/sigma-env/etc/conda/activate.d/'
    ```

3. Créer un fichier `otb-var.sh` vide:

    ```bash
    touch otb-var.sh
    ```

4. Éditer (avec gedit par ex.) le fichier `otb-var.sh` nouvellement créé comme suit (il ne faut pas lancer cette commande dans le terminal mais bien l'écrire de le fichier):

    ```
    source C:/chemin/installation/OTB-7.2.0-Linux64/otbenv.profile
    ```

5. Activer (ou quitter puis ré-activer) l'environnement `sigma-env` depuis le terminal


6. Lancer spyder ou une console python depuis le terminal puis exécuter les lignes de codes suivantes :

    ```python
    import subprocess

    batcmd = 'otbcli_BandMath'
    # pour python >= 3.7
    result = subprocess.check_output(batcmd, shell=True, stderr=subprocess.STDOUT)
    print(result.decode())
    # sinon
    subprocess.run(batcmd)

    ```

    vous devriez obtenir quelque chose dans ce gout là :

    ```
    ERROR: Waiting for at least one parameter.
    This is the BandMath application, version 6.7.0
    Outputs a monoband image which is the result of a mathematical operation on several multi-band images.
    Complete documentation: https://www.orfeo-toolbox.org/CookBook/Applications/app_BandMath.html or -help
    Parameters:
    MISSING -il       <string list>    Input image list  (mandatory)
    MISSING -out      <string> [pixel] Output Image  [pixel=uint8/uint16/int16/uint32/int32/float/double/cint16/cint32/cfloat/cdouble] (default value is float) (mandatory)
    MISSING -exp      <string>         Expression  (mandatory)
            -ram      <int32>          Available RAM (MB)  (optional, off by default, default value is 256)
            -progress <boolean>        Report progress
            -help     <string list>    Display long help (empty list), or help for given parameters keys
    Use -help param1 [... paramN] to see detailed documentation of those parameters.
    Examples:
    otbcli_BandMath -il verySmallFSATSW_r.tif verySmallFSATSW_nir.tif verySmallFSATSW.tif -out apTvUtBandMathOutput.tif -exp "cos( im1b1 ) > cos( im2b1 ) ? im3b1 : im3b2"
    ```
    cela indique que les applis otb sont bien détectées.
