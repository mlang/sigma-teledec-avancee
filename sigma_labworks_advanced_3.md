---
title: Télédétection - Approfondissemnt
author: Marc Lang
html:
  toc: true
  embeb_local_images: true
  embed_svg: true
  cdn: true
  offline: true
export_on_save:
    html: True
---

<!-- import de fichier de style -->


@import "less/question_answer.less"
@import "less/numbered_headsection.less"
@import "less/redish_section.less"
@import "less/block_code_even.less"
@import "less/inline_code_red.less"
@import "less/blue_highlight.less"
@import "less/pd_table.less"
@import "less/image_zoom.less"
@import "less/link.less"
@import "less/fig_caption.less"
@import "less/counters.less"
@import "less/bold.less"
@import "less/styling.less"  
@import "less/link.less"
@import "less/fig_caption.less"
@import "less/counters.less"
@import "less/bold.less"
@import "less/styling.less"  
@import "less/sidebar_toc.less"
@import "less/blocks.less"

<center><img src="img/logo_ensat.png" title="" width="50%"></center>
<center><section style="font-size:300%"> Télédétection - Approfondissement </section></center>

<center><section style="font-size:200%; color:#9d1300; width: 65%"> TD - 3 : Mes premiers pas dans la classification avec python (partie 1) </section></center>

<br>
<center><section style="font-size:150%"> Marc Lang </section></center>


<p> <br> <br></p>




<span style="font-size:200%"> Table des matières </span>


<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=true} -->
<!-- code_chunk_output -->

1. [Présentation du TD](#présentation-du-td)
    1. [La garrigue montpelliéraine](#la-garrigue-montpelliéraine)
    2. [Les données](#les-données)
2. [Extraction des échantillons](#extraction-des-échantillons)
    1. [Principe](#principe)
    2. [Rasterisation](#rasterisation)
    3. [Lecture des valeurs de l'image](#lecture-des-valeurs-de-limage)
3. [Entrainement et validation du model](#entrainement-et-validation-du-model)
    1. [Entrainement](#entrainement)
    2. [Validation du modèle](#validation-du-modèle)
    3. [Validation : un peu de swag s'il vous plait](#validation-un-peu-de-swag-sil-vous-plait)
        1. [Matrice de confusion](#matrice-de-confusion)
        2. [Qualité des classes](#qualité-des-classes)
4. [Application du modèle à l'ensemble de l'image](#application-du-modèle-à-lensemble-de-limage)
    1. [Extraction de l'image](#extraction-de-limage)
    2. [Application du modèle sur l'image](#application-du-modèle-sur-limage)
    3. [Écriture de l'image](#écriture-de-limage)
5. [Synthèse](#synthèse)

<!-- /code_chunk_output -->



## Présentation du TD

L'objectif de ce TD est de vous initier à la classification d'image de télédétection à l'aide de Python.

### La garrigue montpelliéraine

Un enjeu majeur des milieux méditerranéens, et notamment des milieux de garrigue, est la fermeture de ces milieux qui entraîne une diminution de la biodiversité par la disparition de certains habitats et l’homogénéisation des paysages, homogénéisation qui entraîne elle-même une augmentation des risques d’incendies. **La compréhension de ces processus écologiques est étroitement liée à l’hétérogénéité spatiale relative à quatre strates : le sol nu, la strate herbacée, la strate buissonnante et la strate arborée.**

L'objectif de ce TD est ainsi de cartographier ces quatre strates.

Nous travaillerons sur le site Montagne de la Moure et Causse d’Aumelas. C'est un site d’importance communautaire Natura 2000, d’une superficie de 9369 ha et situé à environ 30 km à l’ouest de Montpellier (voir Figure ci-dessous).

Le paysage typique du causse est celui d’une zone de pelouses sèches en mosaïque avec des formations basses de chênes kermès et de genévriers, et où les affleurements rocheux dominent partout.

<figure>
	<img class="zoomable" src="img/td_classification/study_zone.png" title="" width="100%">
	<figcaption>Localisation du site d'étude. Image en composition infra-rouge couleur issus de la BD ORTHOIRC.</figcaption>
</figure>


### Les données

**Téléchargement**

Vous pourrez télécharger les données nécéssaires à ce TD [ici](https://filesender.renater.fr/?s=download&token=0172b993-7a79-4ab2-b618-ccb0d69660a9).

**Image**

Pour ce TD nous travaillerons à partir de la BD ORTHOIRC. Ce sont des images aériennes acquises par l'IGN à 50cm de résolution spatiale, avec trois bandes spectrales :
- bande 1 : infra-rouge ;
- bande 2 : rouge ;
- bande 3 : vert.

Nous avons en effet besoin ici d'images à très haute résolution spatiale afin d'avoir accès à l'hétérogénéité spatiale à une échelle très fine. Vous disposerez d'une image `aumelas_orthoirc.tif`

**Échantillons**

Vous disposerez aussi d'échantillons terrain pour chacune des strates à classer. Les échantillons sont stockés dans un fichier vecteur `sample_strata.shp`


<div class="block green-block"><p class="title exercice">  </p>

1. Téléchargez les données ;
2. Prenez en connaissance dans QGIS avant de commencer le TD.

</div>

**Des scripts**

Toutes les fonctions que vous avez développées dans les précédents TDs sont disponibles dans le script `read_and_write.py` (<a href="https://mlang.frama.io/sigma-teledec-avancee/scripts/read_and_write.py" download="read_and_write.py"> Télécharger</a>) :

- `open_image` ;
- `get_image_dimension` ;
- `get_origin_coordinates` ;
- `get_pixel_size` ;
- `convert_data_type_from_gdal_to_numpy` ;
- `load_img_as_array` ;
- `write_image`.

Gardez cela en tête lors du TD.

Vous disposerez aussi d'autres scripts que vous aborderez au fur et à mesure de ce TD :
- `classification.py` (<a href="https://mlang.frama.io/sigma-teledec-avancee/scripts/classification.py" download="classification.py">Télécharger</a>);
- `plots.py`(<a href="https://mlang.frama.io/sigma-teledec-avancee/scripts/plots.py" download="plots.py">Télécharger</a>);.

Afin de pouvoir importer ces scripts, n'oubliez pas d'ajouter leur chemin d'accès dans le `PYTHONPATH`. Vous pouvez le configurer depuis ==SPYDER== ou vous pouvez également le faire directement depuis votre propre script python en écrivant les instructions suivantes :

```python
import sys
sys.path.append('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/python_scripts')
```

**Remarque :** cette dernière solution, bien que fonctionnelle n'est pas recommandée.


**Classification ?**

Dans ce TD, nous aborderons une chaine de traitements simplifiée que nous complexifierons dans les séances futures.

 <figure>
	<img class="zoomable" src="img/td_classification/workflow_1.png" title="" width="100%">
	<figcaption>Processus de classification simplifié.</figcaption>
</figure>


Cette chaîne de traitements va nous permettre d'aborder les API des librairies suivantes :

- Pandas ;
- GeoPandas ;
- Scikit-Learn ;
- Matplotlib.

Vous apprendrez à :

- extraire les valeurs spectrales d'échantillons à l'aide ==Pandas== et ==GeoPandas== ;
- lancer des programmes extérieurs (comme OTB) en python ;
- lancer des modèles de classification à l'aide de ==Scikit-Learn== ;
- afficher des résultats de classification à l'aide de ==Matplotlib==, ==Pandas== et ==MuseoToolBox==.


## Extraction des échantillons

La première étape de la chaîne de traitements consiste à extraire les valeurs spectrales de l'image pour nos échantillons de référence.

### Principe

Dans le cas des fichiers de points, un point équivaut à un pixel. Trouver ce pixel est simple. Dans le cas de fichiers de polygones, un polygone équivaut à plusieurs pixels. Or, il est "compliqué" de récupérer les pixels contenus dans un polygone en utilisant la librairie ==GeoPandas==. Nous allons donc utiliser une autre approche.

Le principe est le suivant :

<figure>
	<img src="img/td_classification/extraction_polygone.png" title="" width="100%">
	<figcaption>Chaîne de traitements pour l'extraction d'échantillons sous forme de polygones.</figcaption>
</figure>

Il s'agit de créer une image d'échantillons en rasterisant le fichier vecteur. L'image créée aura les mêmes dimensions que l'image qu'on cherche à classer ainsi que la même taille de pixel. Une fois cette image obtenue, il est facile d'extraire les valeurs des pixels de l'image qui correspondent aux échantillons à l'aide de ==Numpy==.

### Rasterisation

Pour l'étape de rasterisation de notre fichier vecteur, nous allons utiliser l'application `Rasterization` de l'==OTB==. L'==OTB== possède une syntaxe qui permet d'appeler ses applications via une interface python :

```python
import otbApplication

app = otbApplication.Registry.CreateApplication("Rasterization")

app.SetParameterString("in", "qb_RoadExtract_classification.shp")
app.SetParameterString("out", "rasterImage.tif")
app.SetParameterFloat("spx", 1.)
app.SetParameterFloat("spy", 1.)

app.ExecuteAndWriteOutput()
```

Je vous propose cependant de ne pas utiliser cette interface et de passer par les applications en ligne de commande. Vous saurez alors lancer en ligne de commande d'autres applications que les applications ==OTB== (comme par ex. l'application [`gdal_polygonize.py`](https://gdal.org/programs/gdal_polygonize.html)).

<div class="block blue-block">
<p class="title"> DIGRESSION </p>

**Appel d'une application en ligne de commande**

Nous allons pour cela utiliser la bibliothèque `subprocess`:


```python
import subprocess
cmd = 'gdal_polygonize.py raster.tif raster_vectorisee.shp'

# if just want to run the command
subprocess.run(cmd)

# if you want to get shell output
result = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
```

Le principe est d'écrire la ligne de commande sous forme de chaîne de caractère, puis d'exécuter cette chaîne de caractère à l'aide des fonctions `run()` ou  `check_output()` si vous voulez pouvoir récupérer des éventuels messages d'erreur.


</div>

**Cas d'application pour l'application OTB `otbcli_Rasterization`**


```python
import os

# define parameters
my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
in_vector = os.path.join(my_folder, 'sample_strata.shp')
ref_image = os.path.join(my_folder, 'aumelas_orthoirc.tif')
out_image = os.path.splitext(in_vector)[0] + '.tif'
out_image = os.path.join(my_folder, 'sample_strata_id.tif')
field_name = 'id'  # field containing the numeric label of the classes

# define commande
cmd_pattern = ("otbcli_Rasterization -in {in_vector} -im {ref_image} -out {out_image}"
               " -mode attribute -mode.attribute.field {field_name}")
cmd = cmd_pattern.format(in_vector=in_vector, ref_image=ref_image,
                         out_image=out_image, field_name=field_name)

```

```python
# Execute command  (python >= 3.7)
result = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
print(cmd)
print(result.decode())
```
    otbcli_Rasterization -in /home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/sample_strata.shp -im /home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/aumelas_orthoirc.tif -out /home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/sample_strata_id.tif -mode attribute -mode.attribute.field id

    2020-12-29 23:21:17 (INFO) Rasterization: Default RAM limit for OTB is 256 MB
    2020-12-29 23:21:17 (INFO) Rasterization: GDAL maximum cache size is 1596 MB
    2020-12-29 23:21:17 (INFO) Rasterization: OTB will use at most 12 threads
    2020-12-29 23:21:17 (INFO) Rasterization: Input dataset extent is (748232, 6.2738e+06) (751219, 6.27679e+06)
    2020-12-29 23:21:17 (INFO) Rasterization: Input dataset projection reference system is: PROJCS["RGF93 / Lambert-93",GEOGCS["RGF93",DATUM["Reseau_Geodesique_Francais_1993",SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6171"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4171"]],PROJECTION["Lambert_Conformal_Conic_2SP"],PARAMETER["standard_parallel_1",49],PARAMETER["standard_parallel_2",44],PARAMETER["latitude_of_origin",46.5],PARAMETER["central_meridian",3],PARAMETER["false_easting",700000],PARAMETER["false_northing",6600000],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["X",EAST],AXIS["Y",NORTH],AUTHORITY["EPSG","2154"]]
    2020-12-29 23:21:17 (INFO) Rasterization: Output projection reference system is: PROJCS["RGF93 / Lambert-93",
        GEOGCS["RGF93",
            DATUM["Reseau_Geodesique_Francais_1993",
                SPHEROID["GRS 1980",6378137,298.257222101,
                    AUTHORITY["EPSG","7019"]],
                TOWGS84[0,0,0,0,0,0,0],
                AUTHORITY["EPSG","6171"]],
            PRIMEM["Greenwich",0,
                AUTHORITY["EPSG","8901"]],
            UNIT["degree",0.0174532925199433,
                AUTHORITY["EPSG","9122"]],
            AUTHORITY["EPSG","4171"]],
        PROJECTION["Lambert_Conformal_Conic_2SP"],
        PARAMETER["standard_parallel_1",49],
        PARAMETER["standard_parallel_2",44],
        PARAMETER["latitude_of_origin",46.5],
        PARAMETER["central_meridian",3],
        PARAMETER["false_easting",700000],
        PARAMETER["false_northing",6600000],
        UNIT["metre",1,
            AUTHORITY["EPSG","9001"]],
        AXIS["X",EAST],
        AXIS["Y",NORTH],
        AUTHORITY["EPSG","2154"]]
    2020-12-29 23:21:17 (INFO) Rasterization: Output origin: [748231, 6.2768e+06]
    2020-12-29 23:21:17 (INFO) Rasterization: Output size: [6000, 6000]
    2020-12-29 23:21:17 (INFO) Rasterization: Output spacing: [0.5, -0.5]
    2020-12-29 23:21:17 (INFO): Estimated memory for full processing: 686.607MB (avail.: 256 MB), optimal image partitioning: 3 blocks
    2020-12-29 23:21:17 (INFO): File /home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/sample_strata_id.tif will be written in 4 blocks of 3472x3472 pixels
    Writing /home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/sample_strata_id.tif...: 100% [**************************************************] (0s)



**Remarques** :
- si vous n'avez pas bien configuré votre environnement virtuel, vous ne pourrez pas lancer des applications ==OTB==, je pense notamment à la partie [Configuration d'OTB dans le nouvel environnement de travail](https://mlang.frama.io/sigma-teledec-avancee/installation.html#configuration-dotb-dans-le-nouvel-environnement-de-travail) ;
- par défaut, les pixels qui ne correspondent à aucun échantillon auront 0 comme valeur.

<div class="block green-block"><p class="title exercice"></p>

Appliquez la rasterisation à votre jeu d'échantillons et nommer le fichier `sample_strata.tif`.

</div>

### Lecture des valeurs de l'image


Dans la suite du TD, nous aurons besoin de formatter les données selon le format suivant :

<figure>
	<img src="img/td_classification/tableau_numpy_echantillons.png" title="" width="75%">
	<figcaption>Format des matrices d'échantillons X et Y.</figcaption>
</figure>


On récupère une matrice `X` contenant les valeurs spectrales des échantillons (chaque ligne de la matrice `X` représente un pixel et chaque colonne représente une bande spectrale) et une matrice `Y` contenant les labels numériques de ces images (voir le formattage attendu ci-dessus).



Vous disposez pour ce faire d'une fonction `get_samples_from_roi()` dans le script `classification.py`

<div class="block green-block"><p class="title exercice"> </p>

1. Utilisez cette fonction pour extraire les matrices `X` et `Y`;
2. Vérifiez que `X` et `Y` ont des dimensions cohérentes.

**Indice** : vous pouvez obtenir l'aide de la fonction en tapant `help(get_samples_from_roi)` une fois celle-ci importée. Vous saurez alors comment l'utiliser.

```python
import classification as cla
help(cla.get_samples_from_roi)
```

    Help on function get_samples_from_roi in module classification:

    get_samples_from_roi(raster_name, roi_name, value_to_extract=None, bands=None, output_fmt='full_matrix')
        The function get the set of pixel of an image according to an roi file
        (vector or raster). In case of raster format, both map should be of same
        size.

        Parameters
        ----------
        raster_name : string
            The name of the raster file, could be any file GDAL can open
        roi_name : string
            The path of the roi image.
        value_to_extract : float, optional, defaults to None
            If specified, the pixels extracted will be only those which are equal
            this value. By, defaults all the pixels different from zero are
            extracted.
        bands : list of integer, optional, defaults to None
            The bands of the raster_name file whose value should be extracted.
            Indexation starts at 0. By defaults, all the bands will be extracted.
        output_fmt : {`full_matrix`, `by_label` }, (optional)
            By default, the function returns a matrix with all pixels present in the
            ``roi_name`` dataset. With option `by_label`, a dictionnary
            containing as many array as labels present in the ``roi_name`` data
            set, i.e. the pixels are grouped in matrices corresponding to one label,
            the keys of the dictionnary corresponding to the labels. The coordinates
            ``t`` will also be in dictionnary format.

        Returns
        -------
        X : ndarray or dict of ndarra
            The sample matrix. A nXd matrix, where n is the number of referenced
            pixels and d is the number of variables. Each line of the matrix is a
            pixel.
        Y : ndarray
            the label of the pixel
        t : tuple or dict of tuple
            tuple of the coordinates in the original image of the pixels
            extracted




<div class="question">
	<input type="checkbox" id="6">
	<p><label for="6">
	Solution 1
	</label></p>
	<span class="reponse">

  ```python
  import os
  my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
  sample_filename = os.path.join(my_folder, 'sample_strata.tif')
  image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')
  X, Y, t = cla.get_samples_from_roi(image_filename, sample_filename)
  print(X.shape)
  print(Y.shape)
  ```

      (25557, 3)
      (25557, 1)
</span>
</div>

<br>

<div class="question">
	<input type="checkbox" id="6bis">
	<p><label for="6bis">
	Solution 2
	</label></p>
	<span class="reponse">

  ```python
  my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
  sample_filename = os.path.join(my_folder, 'sample_strata_id.tif')
  image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')
  _, Y_id, _ = cla.get_samples_from_roi(image_filename, sample_filename)
  ```
</span>
</div>

<br>
</div>

<div class="block green-block"><p class="title exercice"> (Facultatif, à faire pour la prochaine séance)</p>

Ouvrez le script `classification.py` et essayez de comprendre le principe de la fonction `get_samples_from_roi`, même si vous ne comprenez pas toutes les lignes de code.

</div>

## Entrainement et validation du model

Nous allons maintenant parler de la partie classification (ou *Machine Learning* si vous voulez faire chic). Nous verrons dans cette partie comment :

- réaliser l'apprentissage d'un classifieur à partir d'une matrice `X` d'échantillons ;
- évaluer ce modèle.

<figure>
	<img src="img/td_classification/entrainement_validation_v0.png" title="" width="100%">
	<figcaption>Entrainement et validation d'un modèle de classification (version simplifiée).</figcaption>
</figure>


Pour toutes ces parties, nous allons utliser la librairie ==Sickit-Learn==.

<div class="block blue-block">
<p class="title"> DIGRESSION : Scikit learn</p>

==Scikit-learn== est **LA** librairie python pour le Machine Learning. Vous pourrez y trouver :

- des modèles de classification supervisée ;
- des modèles de classification non supervisée ;
- des modèles de regression ;
- des fonctions de préparation des jeux de données ;
- de modèles de réduction de dimensions ;
- et d'autres ...

Vous trouverez aussi de nombreux exemples illustrés. Voyez ci-dessous un exemple de comparaison de classifieurs :

<figure>
	<img src="img/td_classification/comparaison_classifieur.png" title="" width="100%">
	<figcaption>Comparaison de classifieurs dans Sickit-Learn.</figcaption>
</figure>



Vous trouverez [ici](https://scikit-learn.org/stable/auto_examples/classification/plot_classifier_comparison.html) le code utilisé pour produire cette figure.


</div>


### Entrainement

L'entrainement consiste à déterminer les règles de décision qui permettront d'assigner un label à un pixel. Nous allons dans cet exemple utiliser l'algorithme de classification "arbre de décision" mais sachez que la syntaxe que nous verrons reste la même pour tout autre classifieur.

La première étape consiste toujours à initialiser un classifieur, ici un arbre de décision dont l'objet ==Scikit-learn== se nomme `DecisionTreeClassifier`.


```python
#from sklearn.svm import SVC
from sklearn import tree

clf = tree.DecisionTreeClassifier(max_leaf_nodes=10)
```

**C'est tout ? Et les Hyper-paramètres ?**

Vous avez raison ! Généralement les algorithmes de classification ont des paramètres qu'il faut définir *a priori de la classification*.

<!-- Dans le cas de SVM, ce sont principalement les paramètres, `C`, `gamma` et `kernel` qui doivent être définis avec précaution. Il existe également d'autres paramètres dont la liste est disponible [ici](https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html#sklearn.svm.SVC) -->

La liste des paramètres pour le classifieur `DecisionTreeClassifier` est disponible [ici](https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html#sklearn.tree.DecisionTreeClassifier). Ne nous préoccupons cependant pas de ça pour le moment, tout hyperparamètre possède une valeur par defaut. Nous utiliserons ces valeurs pour le moment.

L'étape suivante consiste à utiliser la méthode `fit` du modèle. Tout modèle de classification possède une méthode `fit` :


```python
# Train
clf.fit(X, Y)
```




    DecisionTreeClassifier(max_leaf_nodes=10)



Après cette étape, les règles de décision (ou paramètres du modèle) sont stockées dans l'objet `clf` et peuvent être alors appliquées à une image ou un jeu de validation.

Dans le cas des arbres de décision, ces paramètres peuvent être visualisés sous forme d'un arbre de décision :


```python
import matplotlib.pyplot as plt
fig, ax = plt.subplots(figsize=(20,10))
my_tree = tree.plot_tree(clf, ax=ax, fontsize=14,
                         feature_names=['IR', 'R', 'G'],
                         class_names=['SN', 'H', 'LB', 'LH'],
                         label='root', impurity=False, proportion=True,
                         precision=2)
```



<figure>
	<img src="img/td_classification/output_81_0.png" title="" width="100%">
	<figcaption>Arbre de décisions généré avec le classifieur "DecisionTreeClassifier".</figcaption>
</figure>


Notez bien que pour d'autres algorithmes, les règles de décisions peuvent être plus abstraites et difficilement interprétables.

### Validation du modèle

Maintenant que le modèle est entrainé, évaluons son pouvoir prédictif. Nous allons utiliser le modèle entrainé pour prédire un jeu de données dont on connaît les labels et comparer les deux.  La prédiction se fait via la méthode `predict` d'un classifieur :


```python
# Test
Y_predict = clf.predict(X)
```

<div class="block red-block">
<p class="title"> ERREUR COURANTE</p>

Dans notre cas, nous avons prédit les labels du jeu de données qui a servi pour l'entrainement et **c'est une très mauvaise pratique**. La qualité du modèle a de très grande chance d'être surestimée. Nous reviendrons sur ce point plus tard mais faisons abstraction pour le moment.

</div>

Comparons maintenant les labels prédits avec les véritables labels. Pour ce faire, ==Scikit-Learn== contient de nombreuses fonctions via le module `metrics` :


```python
from sklearn.metrics import confusion_matrix, classification_report, \
    accuracy_score
import numpy as np

# Compute the confusion matrix
cm = confusion_matrix(Y, Y_predict)

# Compute the quality metrics by classe.
report_str = classification_report(Y, Y_predict, labels=np.unique(Y))#, output_dict=True)

# Compute the overall accuracy
accuracy = accuracy_score(Y, Y_predict)
```

La matrice de confusion est retournée sous forme de tableau numpy.


```python
print(cm)
print(type(cm))
```

    [[4176    1    0    0]
     [   4 6166    0    0]
     [   0    1 5285  290]
     [   0    0  952 8682]]
    <class 'numpy.ndarray'>


Au besoin, vous pouvez l'enregistrer sous forme de fichier texte :


```python
np.savetxt('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/cm.txt', cm)
```

Le rapport de classification est disponible par défaut sous forme d'une chaîne de caractères qu'on peut afficher :


```python
print(report_str)
```

                  precision    recall  f1-score   support

               1       1.00      1.00      1.00      4177
               2       1.00      1.00      1.00      6170
               3       0.85      0.95      0.89      5576
               4       0.97      0.90      0.93      9634

        accuracy                           0.95     25557
       macro avg       0.95      0.96      0.96     25557
    weighted avg       0.95      0.95      0.95     25557



La qualité globale est retournée sous forme de `float` :


```python
print(accuracy)
```

    0.9511679774621434


<div class="block green-block"><p class="title exercice"> </p>

1. Entrainez votre premier modèle et évaluez sa qualité.

2. Relancez le code plusieurs fois et commentez l'éventuelle évolution :
   - des paramètres de classification ;
   - de la qualité du modèle.

</div>

### Validation : un peu de swag s'il vous plait

Pour le moment, vous avez obtenu des indices de qualité du modèle entrainé sous forme de texte ou de matrice : c'est encore difficile à exploiter, et *a fortiori* difficle à présenter. Voyons comment nous pourrions mettre en forme ces informations brutes.

#### Matrice de confusion

Nous allons mettre en forme la matrice de confusion sous forme de figure pour qu'elle soit plus lisible. Nous allons utiliser pour cela la librairie ==museotoolbox== (documentation [ici](https://museotoolbox.readthedocs.io/en/latest/)) qui permet, entre autres choses, de faire des matrices de confusion plus informatives qu'un simple `print` d'une matrice ==numpy==.

Nous allons notamment utiliser la classe `PlotConfusionMatrix` du module `charts` :


```python
from museotoolbox.charts import PlotConfusionMatrix
from matplotlib.pyplot import cm as colorMap

pltCm = PlotConfusionMatrix(cm, cmap=colorMap.YlGn)
pltCm.color_diagonal(diag_color=colorMap.Greens,
                         matrix_color=colorMap.Reds)
```


<figure>
	<img src="img/td_classification/output_102_0.png" title="" width="30%">
	<figcaption>Affichage d'une matrice de confusion à l'aide de la classe "PlotConfusionMatrix" - affichage basique.</figcaption>
</figure>


Pour le moment vous pouvez voir une matrice où les accords sont indiqués en vert  et les confusions en rouge. Ajoutons maintenant des informations textuelles :


```python
pltCm = PlotConfusionMatrix(cm, cmap=colorMap.YlGn)


pltCm.add_text(font_size=12)
pltCm.add_x_labels(np.unique(Y), rotation=45)
pltCm.add_y_labels(np.unique(Y))

pltCm.color_diagonal(diag_color=colorMap.Greens,
                         matrix_color=colorMap.Reds)

```

<figure>
	<img src="img/td_classification/output_104_1.png" title="" width="30%">
	<figcaption>Affichage d'une matrice de confusion à l'aide de la classe "PlotConfusionMatrix" - affichage avec le nombre de pixels.</figcaption>
</figure>


Vous avez maintenant le nombre d'échantillons qui est affiché, et les labels des classes qui ont été modifiés.

Une matrice de confusion est utile pour repérer les confusions entre les classes. C'est aussi à partir de celle-ci que vous pouvez avoir accès à la qualité de chaque classe. Il est possible de les afficher avec la classe `PlotConfusionMatrix` :


```python
pltCm = PlotConfusionMatrix(cm, cmap=colorMap.YlGn)

pltCm.add_text(font_size=12)
pltCm.add_x_labels(np.unique(Y), rotation=45)
pltCm.add_y_labels(np.unique(Y))
pltCm.color_diagonal(diag_color=colorMap.Greens,
                         matrix_color=colorMap.Reds)
pltCm.add_accuracy(invert_PA_UA=False, user_acc_label='Recall',
                    prod_acc_label='Precision')
pltCm.add_f1()
```

<figure>
	<img src="img/td_classification/output_106_0.png" title="" width="50%">
	<figcaption>Affichage d'une matrice de confusion à l'aide de la classe "PlotConfusionMatrix" - affichage avec le nombre de pixel et les indices de qualité par classe.</figcaption>
</figure>


Vous avez maintenant les informations les plus importantes pour évaluer la qualité de votre classification, et ce, d'un seul coup d'oeil.

#### Qualité des classes

Je vous propose de réaliser un autre graphique permettant de se concentrer sur la qualité des classes :

<figure>
	<img src="img/td_classification/output_122_0.png" title="" width="65%">
	<figcaption>Pécision, rappel et f1-score de chaque classe.</figcaption>
</figure>


Pour réaliser ce type de figure, nous allons utiliser le rapport de classification fourni par ==Scikit-learn==. Récupérons cette fois-ci les données sous forme de dictonnaire. Elles seront plus faciles à exploiter ensuite.


```python
report = classification_report(Y, Y_predict, labels=np.unique(Y), output_dict=True)
print(report)
print()
print(report['1'])
```

    {'1': {'precision': 0.999043062200957, 'recall': 0.9997605937275557,
    'f1-score': 0.999401699174345, 'support': 4177}, '2': {'precision':
    0.9996757457846952, 'recall': 0.9993517017828201, 'f1-score':
    0.9995136975198573, 'support': 6170}, '3': {'precision':
    0.8473625140291807, 'recall': 0.9478120516499282, 'f1-score':
    0.8947769406585965, 'support': 5576}, '4': {'precision':
    0.9676772180115916, 'recall': 0.9011833091135562, 'f1-score':
    0.9332473395678813, 'support': 9634}, 'accuracy':
    0.9511679774621434, 'macro avg': {'precision': 0.9534396350066061,
    'recall': 0.9620269140684651, 'f1-score': 0.95673491923017,
    'support': 25557}, 'weighted avg': {'precision': 0.9542785897740484,
    'recall': 0.9511679774621434, 'f1-score': 0.9516641820893712,
    'support': 25557}}

    {'precision': 0.999043062200957, 'recall': 0.9997605937275557, 'f1-
    score': 0.999401699174345, 'support': 4177}


Les indices de qualité sont accessibles classe par classe. Nous pourrions *parser* les informations afin d'extraire une liste de précisions et de rappels mais ce serait se compliquer la tache plus que nécessaire car ==Pandas== permet de faire ça automatiquement.

<div class="block blue-block">
<p class="title"> DIGRESSION </p>

**Qu'est ce que [==Pandas==](https://pandas.pydata.org/docs/user_guide/index.html#user-guide) et à quoi sert la librairie ?**

==Pandas== est une librairie qui s’appuie sur ==NumPy== et qui permet de manipuler de grands volumes de données structurées de manière simple et intuitive sous forme de tableaux. Si ==Numpy== est très performmant pour faire des calculs sur des données numériques, il n'est pas aisé d'indexer des données variées, de différents types. C'est possible, mais compliqué. C'est là où intervient [==Pandas==](https://pandas.pydata.org/docs/user_guide/index.html#user-guide).

Le type de base disponible dans la librairie est la Serie (`pandas.Series`) qui peut être considérée comme un tableau de données à une dimension (grossièrement équivalent à une liste). L’extension en deux dimensions est un DataFrame (`pandas.DataFrame`). C’est un simple tableau dont les séries constituent les colonnes. À les différences des tableaux ==Numpy== les lignes et les colonnes possèdent des index. Ces index sont:

- pour les colonnes,  les noms des colonnes (généralement des chaînes de caractères), on peut les afficher avec l'instruction `df.columns`;
- pour les lignes, des nombres entiers (numéros des lignes), ou des chaînes de caractères, ou encore des `DatetimeIndex` ou `PeriodIndex` pour les séries temporelles, on peut les afficher avec l'instruction `df.index`.

<figure>
	<img src="img/td_classification/panda.png" title="Source : https://portailsig.org/content/python-geopandas-ou-le-pandas-spatial.html" width="55%">
	<figcaption>Stucture des objets "DataFrame".</figcaption>
</figure>


Un `DataFrame` est l’équivalent d’une matrice ==NumPy== dont chaque élément d'une colonne (Serie) est de même type (n’importe quel type de données, objets Python, nombres, dates, textes, binaires) et qui peut contenir des valeurs manquantes. C’est aussi l’équivalent des Data Frame du langage R.

La syntaxe n'est pas évidente à prendre en main. Elle diffère sensiblement de celle de ==Numpy==, aussi nous utiliserons des syntaxes élémentaires pour manipuler ces objets.

Pour plus d'infos, vous pouvez vous référer à :

- ce [site](https://portailsig.org/content/python-geopandas-ou-le-pandas-spatial.html) dont sont issues bon nombre des explications que vous lisez ;
- la documentation de [==Pandas==](https://pandas.pydata.org/docs/user_guide/index.html#user-guide) : https://pandas.pydata.org/docs/user_guide/index.html#user-guide .

</div>


La première étape à faire est de convertir le dictionnaire en `DataFrame` :


```python
import pandas as pd
report_df = pd.DataFrame.from_dict(report)
report_df
```


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
      <th>accuracy</th>
      <th>macro avg</th>
      <th>weighted avg</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>precision</th>
      <td>0.999043</td>
      <td>0.999676</td>
      <td>0.847363</td>
      <td>0.967677</td>
      <td>0.951168</td>
      <td>0.953440</td>
      <td>0.954279</td>
    </tr>
    <tr>
      <th>recall</th>
      <td>0.999761</td>
      <td>0.999352</td>
      <td>0.947812</td>
      <td>0.901183</td>
      <td>0.951168</td>
      <td>0.962027</td>
      <td>0.951168</td>
    </tr>
    <tr>
      <th>f1-score</th>
      <td>0.999402</td>
      <td>0.999514</td>
      <td>0.894777</td>
      <td>0.933247</td>
      <td>0.951168</td>
      <td>0.956735</td>
      <td>0.951664</td>
    </tr>
    <tr>
      <th>support</th>
      <td>4177.000000</td>
      <td>6170.000000</td>
      <td>5576.000000</td>
      <td>9634.000000</td>
      <td>0.951168</td>
      <td>25557.000000</td>
      <td>25557.000000</td>
    </tr>
  </tbody>
</table>
</div>

Dans cet exemple, nous ne nous intéresserons qu'aux trois métriques suivantes :
- precision ;
- recall ;
- f1-score.

Débarrassons nous des autres informations :


```python
# drop columns (axis=1, same as numpy)
report_df = report_df.drop(['accuracy', 'macro avg', 'weighted avg'], axis=1)
# drop rows (axis=0, same as numpy)
report_df = report_df.drop(['support'], axis=0)
report_df
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>precision</th>
      <td>0.999043</td>
      <td>0.999676</td>
      <td>0.847363</td>
      <td>0.967677</td>
    </tr>
    <tr>
      <th>recall</th>
      <td>0.999761</td>
      <td>0.999352</td>
      <td>0.947812</td>
      <td>0.901183</td>
    </tr>
    <tr>
      <th>f1-score</th>
      <td>0.999402</td>
      <td>0.999514</td>
      <td>0.894777</td>
      <td>0.933247</td>
    </tr>
  </tbody>
</table>
</div>


Nous avons maintenant un tableau prêt à être exploité. Nous pourrions utiliser ces informations avec l'API de ==matplotlib==, mais certaines fonctions de ==matplotlib== (plus d'infos [ici](https://pandas.pydata.org/pandas-docs/stable/user_guide/visualization.html)) sont directement interfacées dans ==Pandas== :


```python
ax = report_df.T.plot.bar()
```

<figure>
	<img src="img/td_classification/output_116_0.png" title="" width="65%">
	<figcaption>Affichage de la qualité des classes avec Pandas, sans personnalisation. </figcaption>
</figure>


En passant par l'interface de ==Pandas== plutôt que celle de ==matplotlib==, les différents labels et légendes contenus dans les index de colonnes et de lignes du `DataFrame` sont automatiquement utilisés. Pratique non ?

Vous avez maintenant quelque chose de plus visuel mais il est possible de faire mieux. Il va falloir pour cela repasser par l'API de ==matplotlib==. Pas d'inquiétude, les deux API se marient plutôt bien.

Pour commencer, nous allons créer une figure avec ==matplotlib==, récupérer une instance de `matplotlib.axes._subplots.AxesSubplot` dans la variable `ax` et la donner comme argument à la méthode `bar` du `DataFrame` :


```python
import matplotlib.pyplot as plt
fig, ax = plt.subplots(figsize=(10,7))
ax = report_df.T.plot.bar(ax=ax)
```

<figure>
	<img src="img/td_classification/output_118_0.png" title="" width="65%">
	<figcaption>Affichage de la qualité des classes avec Pandas et Matplotlib, sans personnalisation.</figcaption>
</figure>


Nous allons maintenant ajouter des informations à notre graphique. Par exemple un titre et l'accord global du modèle de classification.


```python
import matplotlib.pyplot as plt
fig, ax = plt.subplots(figsize=(10,7))
ax = report_df.T.plot.bar(ax=ax, zorder=2)

# custom : information
ax.set_ylim(0.5, 1)
_ = ax.text(0.05, 0.95, 'OA : {:.2f}'.format(accuracy), fontsize=14)
_ = ax.set_title('Class quality estimation')
```

<figure>
	<img src="img/td_classification/output_120_0.png" title="" width="65%">
	<figcaption>Affichage de la qualité des classes avec Pandas et Matplotlib, avec ajout d'un titre et d'informations textuelles.</figcaption>
</figure>


Rajoutons maintenant quelques élements graphiques pour que les informations soient encore plus lisibles :


```python
import matplotlib.pyplot as plt
fig, ax = plt.subplots(figsize=(10,7))
ax = report_df.T.plot.bar(ax=ax, zorder=2)

# custom : information
ax.set_ylim(0.5, 1)
ax.text(0.05, 0.95, 'OA : {:.2f}'.format(accuracy), fontsize=14)
ax.set_title('Class quality estimation')

# custom : cuteness
# background color
ax.set_facecolor('ivory')
# labels
x_label = ax.get_xlabel()
ax.set_xlabel(x_label, fontdict={'fontname': 'Sawasdee'}, fontsize=14)
y_label = ax.get_ylabel()
ax.set_ylabel(y_label, fontdict={'fontname': 'Sawasdee'}, fontsize=14)
# borders
ax.spines["top"].set_visible(False)
ax.spines["right"].set_visible(False)
ax.spines["bottom"].set_visible(False)
ax.spines["left"].set_visible(False)
ax.tick_params(axis='x', colors='darkslategrey', labelsize=14)
ax.tick_params(axis='y', colors='darkslategrey', labelsize=14)
# grid
ax.minorticks_on()
ax.yaxis.grid(which='major', color='darkgoldenrod', linestyle='--',
                      linewidth=0.5, zorder=1)
ax.yaxis.grid(which='minor', color='darkgoldenrod', linestyle='-.',
                      linewidth=0.3, zorder=1)
```

<figure>
	<img src="img/td_classification/output_122_0.png" title="" width="65%">
	<figcaption>Affichage de la qualité des classes avec Pandas et Matplotlib, avec ajout d'un titre et d'informations textuelles et personnalisation de l'arrière plan.</figcaption>
</figure>


**Remarque** : il est improbable que vous reteniez toutes les méthodes présentées. Cette mise en forme vous est proposée pour que vous ayez un exemple à disposition. Vous pourrez y revenir si besoin. Vous pouvez également consulter la documentation de ==Matplotlib== si vous avez des besoins différents.

<div class="block green-block"><p class="title exercice"> </p>

1. Reproduisez les deux types de graphique vus ci-dessus ;
2. Testez éventuellement d'autres mises en forme.

</div>

## Application du modèle à l'ensemble de l'image

Le modèle est désormais entrainé et vous avez vérifié que sa qualité était satisfaisante, vous pouvez maintenant l'appliquer sur l'ensemble de l'image afin d'obtenir une carte des quatre strates de végétation.

<figure>
	<img class="zoomable" src="img/td_classification/application_model_v0.png" title="" width="100%">
	<figcaption>Chaine de traitements pour appliquer un modèle de classification.</figcaption>
</figure>


### Extraction de l'image

De le même manière que pour les échantillons, nous allons devoir formater l'image sous forme d'une matrice `X` avec les pixels en lignes et les bandes spectrales en colonnes.


<div class="block green-block"><p class="title exercice"></p>

À partir du fichier image en entrée, écrivez des instructions qui permettent d'obtenir une matrice `X` qui soit utilisable ensuite par ==scikitlearn==. Plusieurs choix s'offrent à vous :
1. vous pouvez utiliser de manière détournée une fonction que nous avons vu dans ce cours ;
2. vous pouvez charger l'image sous forme de matrice puis la re-formater.

<div class="question">
	<input type="checkbox" id="7">
	<p><label for="7">
	 Solution option 1
	</label></p>
	<span class="reponse">


  ```python
  import classification as cla
  import os

  my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
  image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')
  sample_filename = image_filename
  X_img, _, t_img = cla.get_samples_from_roi(image_filename, sample_filename)
  ```

  Ici nous avons détourné la fonction `get_samples_from_roi` de son usage initial. Pour rappel, la fonction permet d'extraire les valeurs `image_filename` la où les pixels de la première bande de `sample_filename` sont différents de 0. Si `sample_filename = image_filename`, tous les pixels de l'image seront extraits =). Dans ce cas, nous n'avons par contre pas besoin des sorties `Y`.


</span>
</div>
<br>
<div class="question">
	<input type="checkbox" id="7bis">
	<p><label for="7bis">
	 Solution option 2
	</label></p>
	<span class="reponse">

  ```python
  import read_and_write as rw
  import os
  my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
  image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')

  # load image
  image = rw.load_img_as_array(image_filename)

  # reshape image
  nb_band = image.shape[2]
  X_img2 = image.reshape((-1, nb_band))

  # remove pixel that equals to 0 on the first band
  X_img2 = X_img2[X_img2[:, 0] != 0]
  ```


</span>
</div>
<br>

**Remarque** : les deux solutions sont équivalentes.

```python
print(np.equal(X_img, X_img2).all())
```

    True

</div>


### Application du modèle sur l'image

Nous allons utiliser la méthode `predict` de notre classifieur `clf` précédemment entrainé sur notre matrice-image `X_img`:


```python
Y_predict = clf.predict(X_img)
Y_predict.shape
```


    (35989034,)


`Y_predict` contient maintenant l'ensemble des labels de l'image. Cependant, le format de la matrice n'est pas adapté pour l'écrire sous forme de fichier GeoTiff. Voyons comment la formatter sous forme d'une matrice à deux dimensions :  


```python
import read_and_write as rw
my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')

# Get image dimension
ds = rw.open_image(image_filename)
nb_row, nb_col, _ = rw.get_image_dimension(ds)

#initialization of the array
img = np.zeros((nb_row, nb_col, 1), dtype='uint8')
#np.Y_predict

img[t_img[0], t_img[1], 0] = Y_predict
```

Les coordonnées `t_img` retournées par la fonction `get_sample_from_roi` sont nécessaires pour reconstruire l'image à partir de la simple matrice `Y_predict` à une dimension.


### Écriture de l'image

Vous avez développé lors des précédents TDs une fonction `write_image`. Elle est maintenant disponible dans le script `read_and_write`.


<div class="block green-block"><p class="title exercice"></p>

Écrivez des instructions qui permettent d'appliquer le modèle sur l'ensemble de l'image puis d'écrire le résultat dans une image que vous nommerez `ma_premiere_classif_scikit.tif`.

<div class="question">
	<input type="checkbox" id="8">
	<p><label for="8">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import read_and_write as rw
  my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
  image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')
  out_filename = os.path.join(my_folder, 'ma_premiere_classif_scikit.tif')

  # get reference dataset
  ds = rw.open_image(image_filename)

  # just replace the number of band (1 instead of 3)
  rw.write_image(out_filename, img, data_set=ds, gdal_dtype=None,
                  transform=None, projection=None, driver_name=None,
                  nb_col=None, nb_ligne=None, nb_band=1)
  ```
</span>
</div>
<br>
Ouvrez maintenant votre image pour la visualiser et inspectez le résultat.

- êtes-vous satisfait·e·s de la qualité de la classification ?
- est-elle conforme à ce qu'indique votre analyse des indices de qualité ?
- repérez dans l'image un problème qui n'est pas mis en exergue par la matrice de confusion ou les indices de rappel et de précision.

</div>



## Synthèse

Maintenant que vous avez produit chaque étape séparément, vous allez pouvoir faire un script qui chaine toutes ces instructions.

<figure>
	<img class="zoomable" src="img/td_classification/workflow_1.png" title="" width="100%">
	<figcaption>Processus de classification simplifié.</figcaption>
</figure>



<div class="block green-block"><p class="title exercice"> </p>

Écrivez l'enchainement des instructions qui permettent, à partir d'une image et d'un jeu d'échantillons, d'obtenir une image classifiée et deux figures permettant d'interpréter sa qualité.

**Astuce** : vous avez à votre disposition dans un script `plots.py` les fonctions suivantes :

- `plot_cm`
- `plot_class_quality`

<div class="question">
	<input type="checkbox" id="9">
	<p><label for="9">
	Solution
	</label></p>
	<span class="reponse">



  ```python
  import sys
  sys.path.append('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/python_scripts')

  import os
  import numpy as np
  from sklearn import tree
  from sklearn.metrics import confusion_matrix, classification_report, \
      accuracy_score

  # personal librairies
  import classification as cla
  import read_and_write as rw
  import plots

  # 1 --- define parameters
  # inputs
  my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
  sample_filename = os.path.join(my_folder, 'sample_strata.tif')
  image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')
  # outputs
  out_classif = os.path.join(my_folder, 'ma_classif.tif')
  out_matrix = os.path.join(my_folder, 'ma_matrice.png')
  out_qualite = os.path.join(my_folder, 'mes_qualites.png')

  # 2 --- extract samples
  X, Y, t = cla.get_samples_from_roi(image_filename, sample_filename)

  # 3 --- Train
  clf = tree.DecisionTreeClassifier(max_leaf_nodes=10)
  clf.fit(X, Y)

  # 4 --- Test
  Y_predict = clf.predict(X)

  # compute quality
  cm = confusion_matrix(Y, Y_predict)
  report = classification_report(Y, Y_predict, labels=np.unique(Y),
                                 output_dict=True)
  accuracy = accuracy_score(Y, Y_predict)

  # display and save quality
  plots.plot_cm(cm, np.unique(Y), out_filename=out_matrix)
  plots.plot_class_quality(report, accuracy, out_filename=out_qualite)

  # 5 --- apply on the whole image
  # load image
  X_img, _, t_img = cla.get_samples_from_roi(image_filename, image_filename)

  # predict image
  Y_predict = clf.predict(X_img)

  # reshape
  ds = rw.open_image(image_filename)
  nb_row, nb_col, _ = rw.get_image_dimension(ds)

  img = np.zeros((nb_row, nb_col, 1), dtype='uint8')
  img[t_img[0], t_img[1], 0] = Y_predict

  # write image
  ds = rw.open_image(image_filename)
  rw.write_image(out_classif, img, data_set=ds, gdal_dtype=None,
              transform=None, projection=None, driver_name=None,
              nb_col=None, nb_ligne=None, nb_band=1)
  ```

</span>
</div>
<br>
</div>

<br><br><br>
