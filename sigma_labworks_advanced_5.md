---
title: Télédétection - Approfondissemnt
author: Marc Lang
html:
  toc: true
  embeb_local_images: true
  embed_svg: true
  cdn: true
  offline: true
export_on_save:
    html: True
---

<!-- import de fichier de style -->


@import "less/question_answer2.less"
@import "less/blocks.less"
@import "less/numbered_headsection.less"
@import "less/redish_section.less"
@import "less/block_code_even.less"
@import "less/inline_code_red.less"
@import "less/blue_highlight.less"
@import "less/pd_table.less"
@import "less/iframe.less"
@import "less/image_zoom.less"
@import "less/link.less"
@import "less/fig_caption.less"
@import "less/counters.less"
@import "less/bold.less"
@import "less/styling.less"  
@import "less/sidebar_toc.less"


<center><img src="img/logo_ensat.png" title="" width="50%"></center>
<center><section style="font-size:300%"> Télédétection - Approfondissement </section></center>

<center><section style="font-size:200%; color:#9d1300; width: 65%"> TD - 5 : Visualisation des données de télédétection en python </section></center>

<br>
<center><section style="font-size:150%"> Marc Lang </section></center>


<p> <br> <br></p>



<span style="font-size:200%"> Table des matières </span>


<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=true} -->
<!-- code_chunk_output -->

1. [Visualiser les échantillons](#visualiser-les-échantillons)
    1. [Histogramme 1D](#histogramme-1d)
        1. [Histogramme des échantillons par bande](#histogramme-des-échantillons-par-bande)
            1. [Affichage pour une bande](#affichage-pour-une-bande)
            2. [Affichage pour plusieurs bandes](#affichage-pour-plusieurs-bandes)
        2. [Histogramme de chaque classe pour une bande](#histogramme-de-chaque-classe-pour-une-bande)
            1. [Prise en main des subplots](#prise-en-main-des-subplots)
            2. [Affichage des classes](#affichage-des-classes)
        3. [Affichage interactif avec Plotly](#affichage-interactif-avec-plotly)
    2. [2D scatter plot](#2d-scatter-plot)
    3. [3D scatter plot](#3d-scatter-plot)
    4. [Un peu de pratique](#un-peu-de-pratique)
2. [Visualiser des images](#visualiser-des-images)
    1. [Affichage basique](#affichage-basique)
    2. [Améliorer le contraste de l'image](#améliorer-le-contraste-de-limage)

<!-- /code_chunk_output -->


## Visualiser les échantillons

Vous savez maintenant extraire des échantillons sous forme d'une matrice `X` et `Y` (cf [TD3](https://mlang.frama.io/sigma-teledec-avancee/sigma_labworks_advanced_4.html) et [TD4](https://mlang.frama.io/sigma-teledec-avancee/sigma_labworks_advanced_4.html)).

```python
import sys
sys.path.append('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/python_scripts')
import os
from sklearn.model_selection import train_test_split

# personal librairies
import classification as cla

# 1 --- define parameters
# inputs
my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
sample_filename = os.path.join(my_folder, 'sample_strata.tif')
image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')

# 2 --- extract samples
X, Y, t = cla.get_samples_from_roi(image_filename, sample_filename)
```

Nous allons voir dans cette partie comment les visualiser sous forme :

- d'histogramme 1D ;
- de scatter plot 2D ;
- de scatter plot 3D.

### Histogramme 1D

La première chose que vous pouvez faire pour explorer le comportement spectrale d'échantillons est d'afficher l'histogramme des leur valeurs pour une ou plusieurs bandes.

#### Histogramme des échantillons par bande

##### Affichage pour une bande

Nous allons pour cela utiliser la fonction `hist` de ==matplotlib== :


```python
import matplotlib.pyplot as plt
import plots

# create plot
fig, ax = plt.subplots(figsize=(10, 7))

# here we decide to plot the Red band
labels = ['R']
band = X[:, 1]
vals, bins, patch = ax.hist(band,  # band to display
                            bins=100, # number of binds
                            label=labels, # for the legend
                            density=True, # if counts are normalized
                            zorder=2)

# display the legend
ax.legend()

# custom the background
ax = plots.custom_bg(ax, x_grid=False)
```




<figure>
	<img src="img/td_visu/output_7_0.png" title="" width="75%">
	<figcaption>Histogramme de la bande Rouge.</figcaption>
</figure>



Dépendamment de la qualité radiométrique de votre image et du nombre `bins` d'intervalles, vous pouvez obtenir un histogramme plus ou moins bruité, plus ou moins lisse. Une solution pour faciliter la lecture d'un histogramme est de tracer une fonction qui interpole votre histogramme.

Pour ce faire, nous allons utiliser la fonction `KernelDensity` de ==Scikit Learn== ([documentation détaillée](https://scikit-learn.org/stable/modules/density.html#kernel-density)).


```python
import numpy as np
from sklearn.neighbors import KernelDensity

# get band to display
band = X[:, 1]

# get 100 values between the min and the max of band
X_plot = np.linspace(band.min(), band.max(), 100)[:, np.newaxis]

# define kernel density
kde = KernelDensity(bandwidth=3)
# learn kernel
kde.fit(band.reshape(-1, 1))
# predict on our X
log_dens = kde.score_samples(X_plot)
density = np.exp(log_dens)

# plot it
fig, ax = plt.subplots(figsize=(10, 7))
ax.plot(X_plot[:,0], density , color='orange',
        zorder=3, linewidth=1.5,
        linestyle='-')
```

<figure>
	<img src="img/td_visu/output_9_1.png" title="" width="75%">
	<figcaption>Interpolation de l'histogramme de la bande Rouge.</figcaption>
</figure>




Affichons maintenant les deux représentations superposées.


```python
fig, ax = plt.subplots(figsize=(10, 7))
labels = ['IR', 'R', 'G']
band = X[:, 1]
vals, bins, patch = ax.hist(band, bins=100, label=labels, density=True,
                           zorder=2)
ax.legend()
ax = plots.custom_bg(ax, x_grid=False)

# get 100 values between the min and the max of band
X_plot = np.linspace(band.min(), band.max(), 100)[:, np.newaxis]

# define kernel density
kde = KernelDensity(bandwidth=3)
# learn kernel
kde.fit(band.reshape(-1, 1))
# predict on our X
log_dens = kde.score_samples(X_plot)
density = np.exp(log_dens)

# plot it
ax.plot(X_plot[:,0], density , color='orange',
        zorder=3, linewidth=3,
        linestyle='-')
```

<figure>
	<img src="img/td_visu/output_11_1.png" title="" width="75%">
	<figcaption>Histogramme et son interpolation de la bande Rouge.</figcaption>
</figure>


<div class="block green-block"><p class="title exercice"></p>

Reproduisez cette figure et faites varier les paramètres `bins` et `bandwidth` et observez leur influence.

</div>



##### Affichage pour plusieurs bandes


Affichons maintenant plusieurs bandes sur le même graphique :

```python
import matplotlib.pyplot as plt
import plots
fig, ax = plt.subplots(figsize=(10, 7))
labels = ['IR', 'R', 'G']
vals, bins, patch = ax.hist(X, bins=100, label=labels, density=True)
ax.legend()
ax = plots.custom_bg(ax, x_grid=False)
```



<figure>
	<img src="img/td_visu/output_14_0.png" title="" width="75%">
	<figcaption>Histogrammes des bandes Infra-rouge, rouge et vert.</figcaption>
</figure>




La lecture des histogrammes n'est pas facile ici. Essayons de calculer et d'afficher leur interpolation.


```python
import numpy as np
from sklearn.neighbors import KernelDensity
import matplotlib.pyplot as plt
import plots

fig, ax = plt.subplots(figsize=(10, 7))

color = ['darkred', 'red', 'green']
labels = ['IR', 'R', 'G']
for idx_band, col, label in zip(range(3), color, labels):
    band = X[:, idx_band]    
    # get 100 values between the min and the max of band
    X_plot = np.linspace(band.min(), band.max(), 100)[:, np.newaxis]

    # define kernel density
    kde = KernelDensity(bandwidth=3)
    # learn kernel
    kde.fit(band.reshape(-1, 1))
    # predict on our X
    log_dens = kde.score_samples(X_plot)
    density = np.exp(log_dens)

    # plot it
    ax.plot(X_plot[:,0], density , color=col,
            zorder=3, linewidth=3,
            linestyle='-')
    # fill space between lines and the x axis (0)
    ax.fill_between(X_plot[:,0], 0, density, alpha=0.5, color=col, label=label)

ax.legend()
ax = plots.custom_bg(ax, x_grid=False)
```



<figure>
	<img src="img/td_visu/output_16_0.png" title="" width="75%">
	<figcaption>Histogrammes interpolés des bandes Infra-rouge, Rouge et Vert.</figcaption>
</figure>




C'est une donnée interpolée, mais elle est plus lisible : les différents modes sont ici facilement repérables.

<div class="block blue-block">
<p class="title"> ASTUCE : itérateurs </p>

La fonction `zip` permet d'itérer sur plusieurs itérateurs en même temps :


```python
iterateur1 = [1, 2, 3]
iterateur2 = ['premier', 'deuxième', 'troisième']
for it1, it2 in zip(iterateur1, iterateur2):
    print(it1)
    print(it2)
    print('--------')
```

    1
    premier
    New iteration
    2
    deuxième
    New iteration
    3
    troisième
    New iteration


Et si vous avez besoin d'un compteur, vous pouvez toujours utiliser la fonction `enumerate`:


```python
iterateur1 = [1, 2, 3]
iterateur2 = ['premier', 'deuxième', 'troisième']
for compteur, (it1, it2) in enumerate(zip(iterateur1, iterateur2)):
    print(it1)
    print(it2)
    print(compteur)
    print('-------')
```

    1
    premier
    0
    -------
    2
    deuxième
    1
    -------
    3
    troisième
    2
    -------


</div>

<div class="block blue-block">
<p class="title"> ASTUCE </p>

Voici un panel de couleurs que vous pouvez renseigner dans de nombreuses fonctions de ==Matplotlib== :


<figure>
	<img class="zoomable" src="img/td_visu/matplotlib_colors.png" title="" width="90%">
	<figcaption>Liste de couleurs utilisable dans l'API de matplotlib.</figcaption>
</figure>


</div>


<div class="block green-block"><p class="title exercice"></p>

Reproduire la figure ci-dessus et personnalisez la avec les couleurs de votre choix.

</div>


#### Histogramme de chaque classe pour une bande

Afin d'avoir l'affichage de plusieurs bandes de manière séparée, nous allons utiliser plusieurs subplots.

##### Prise en main des subplots

Jusqu'à présent nous utilisions la fonction `plt.suplots()` en ne renseignant comme argument que la taille de la figure. La fonction retournait alors une instance `matplotlib.figure.Figure` (que nous utilisons très peu) et une instance `matplotlib.axes._subplots.AxesSubplot` qui correspond à la zone d'affichage. C'est cette dernière classe que nous manipulons le plus.


```python
fig, ax = plt.subplots(figsize=(15, 10))
print(type(fig))
print(type(ax))
```

    <class 'matplotlib.figure.Figure'>
    <class 'matplotlib.axes._subplots.AxesSubplot'>




<figure>
	<img src="img/td_visu/output_28_1.png" title="" width="50%">
	<figcaption>Création d'une figure matplotlib contenant un seul objet AxesSubplot (vide).</figcaption>
</figure>




Il est possible de renseigner le nombre de lignes (`nrows`) et de colonnes (`ncols`) souhaitées. Ces arguments ont une valeur de `1` par défaut. Si `nrows > 1` ou `ncols > 1`, la fonction ne retourne plus une instance de `matplotlib.axes._subplots.AxesSubplot` mais une matrice ==Numpy== de `matplotlib.axes._subplots.AxesSubplot`. Les tableaux ==Numpy== sont partout !


```python
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(15, 10))
print(type(axes))
print(axes.shape)
print(type(axes[0,0]))
```

    <class 'numpy.ndarray'>
    (2, 2)
    <class 'matplotlib.axes._subplots.AxesSubplot'>




<figure>
	<img src="img/td_visu/output_30_1.png" title="" width="75%">
	<figcaption>Création d'une figure matplotlib contenant quatre objets AxesSubplot (vides).</figcaption>
</figure>




Il est ainsi possible d'itérer sur les `axes` d'un suplot. Plus précisément, nous allons itérer sur le tableau "aplati" des intances  (on transforme le tableau 2D en tableau 1D) grâce à la méthode `flatten()` des tableaux ==Numpy==:


```python
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(15, 10))
for i, ax in enumerate(axes.flatten()):
    data = range(0, i + 2)
    ax.plot(data, data)
```



<figure>
	<img class="zoomable" src="img/td_visu/output_32_0.png" title="" width="95%">
	<figcaption>Création d'une figure matplotlib contenant quatre objets AxesSubplot.</figcaption>
</figure>



Nous pouvons dons afficher dans chacun des sublots l'histogramme des échantillons pour une bande :

```python
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(15, 10))
list_title = ['IR', 'R', 'G']
my_iterator = zip(axes.flatten(), list_title)
for i, (ax, title) in enumerate(my_iterator):
    band = X[:, i]
    vals, bins, patch = ax.hist(band, bins=100,  density=True, zorder=2)
    ax.set_title(title)
    ax = plots.custom_bg(ax, x_grid=False)

_ = axes[1][1].axis('off')
```



<figure>
	<img class="zoomable" src="img/td_visu/output_33_0.png" title="" width="90%">
	<figcaption>Histogrammes des bandes Infra-rouge, Rouge et Vert dans subplopts distincts.</figcaption>
</figure>




Rajoutons les histogrammes interpollés :


```python
import numpy as np
from sklearn.neighbors import KernelDensity

fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(15, 10))
list_title = ['IR', 'R', 'G']
my_iterator = zip(axes.flatten(), list_title)
for i, (ax, title) in enumerate(my_iterator):    
    band = X[:, i]
    vals, bins, patch = ax.hist(band, bins=100, label=labels, density=True,
                           zorder=2)

    # get 100 values between the min and the max of band
    X_plot = np.linspace(band.min(), band.max(), 100)[:, np.newaxis]

    # define kernel density
    kde = KernelDensity(bandwidth=3)
    # learn kernel
    kde.fit(band.reshape(-1, 1))
    # predict on our X
    log_dens = kde.score_samples(X_plot)
    density = np.exp(log_dens)

    # plot it
    ax.plot(X_plot[:,0], density ,zorder=3, linewidth=3,
            linestyle='-', color='orange')
    ax.set_title(title)
    ax = plots.custom_bg(ax, x_grid=False)
axes[1][1].axis('off')
```




    (0.0, 1.0, 0.0, 1.0)





<figure>
	<img class="zoomable" src="img/td_visu/output_35_1.png" title="" width="90%">
	<figcaption>Histogrammes et leur interpolation des bandes Infra-rouge, Rouge et Vert dans subplopts distincts.</figcaption>
</figure>




##### Affichage des classes

Pour afficher chaque classe nous allons utiliser un mode de sortie différent de la fonction `get_samples_from_roi` en précisant l'argument `output_fmt='by_label'` :


```python
import sys
sys.path.append('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/python_scripts')
import os
from sklearn.model_selection import train_test_split

# personal librairies
import classification as cla

# 1 --- define parameters
# inputs
my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
sample_filename = os.path.join(my_folder, 'sample_strata.tif')
image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')

# 2 --- extract samples
dict_X, ditc_Y, dict_t = cla.get_samples_from_roi(image_filename, sample_filename, output_fmt='by_label')
```

La sortie retournée par `get_samples_from_roi` n'est plus une matrice `X` mais un dictionnaire de matrices `X`, un pour chaque classe.

Les clefs des dictionnaires sont les labels des classes :


```python
print(dict_X.keys())
```

    dict_keys([1, 2, 3, 4])


On peut ainsi obtenir une matrice `X` pour un label en particulier. Vous trouverez ci-dessous un exemple pour la classe sol nu :


```python
print(dict_X[1])
```

    [[175 193 195]
     [178 194 196]
     [174 193 192]
     ...
     [162 181 180]
     [168 187 187]
     [167 189 189]]



<div class="block green-block"><p class="title exercice"></p>


Comment pourrait-on extraire seuleument les pixels de la classe sol nu de la matrice `X` sans passer par le mode `output_fmt='by_label'` de la fonction `get_samples_from_roi` ?

<div class="question">
	<label>Solution
	<input type="checkbox">
	<i class="arrow"></i>
	<span class="reponse">

  ```python
  condition = (Y==1)
  condition.shape
  condition = condition[:,0]
  X_1 = X[condition]
  print(X_1.shape)
  ```

      (4177, 3)

  On vérifie que la sortie de `get_samples_from_roi` avec le paramètre `output_fmt='by_label'` est bien égale à la matrice qu'on vient de créer :

  ```python
  np.equal(X_1, dict_X[1]).all()
  ```

      True


</span>
</label>
</div>

<br>

</div>





<div class="block blue-block">
<p class="title"> ASTUCE : itération sur les dictionnaires</p>

On peut itérer sur les éléments d'un dictionnaire grâce à la méthode `items()`:


```python
a_dict = {'first_key' : 777, 'second_key': [1, 2, 3]}
for key, value in a_dict.items():
    print('key :', key)
    print('value :', value)
    print('---------')
```

    key : first_key
    value : 777
    ---------
    key : second_key
    value : [1, 2, 3]
    ---------


</div>


Affichons les histogrammes de chaque classe par bande, rajoutons au passage les histogrammes pour le ndvi :


```python
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(15, 10))
list_title = ['IR', 'R', 'G', 'ndvi']
my_iterator = zip(axes.flatten(), list_title)
for i, (ax, title) in enumerate(my_iterator):    
    for class_number, X in dict_X.items():
        if i ==3 :
            X = X.astype('int16') # Warning, you'll have unexpected results
                                  # if you keep data type as 'uint8'
                                  # try it if you're curious
            ir = X[:, 0]
            r =  X[:, 1]
            band = (ir -r) / (ir + r)
        else:
            band = X[:, i]
        vals, bins, patch = ax.hist(band, bins=100,  density=True, label=class_number, alpha=0.5)
    ax.set_title(title)
    ax = plots.custom_bg(ax, x_grid=False, minor=False)
    ax.legend()

```

<figure>
	<img class="zoomable" src="img/td_visu/output_47_0.png" title="" width="90%">
	<figcaption>Histogramme des bandes Infra-rouge, Rouge, Vert et NDVI dans subplopts distincts, avec une distinction pour chaque subplots des différentes classes. </figcaption>
</figure>


En utilisant seuleument les histogrammes interpolés :


```python
from sklearn.neighbors import KernelDensity
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(15, 10))
list_title = ['IR', 'R', 'G', 'ndvi']
my_iterator = zip(axes.flatten(), list_title)
color = ['tan', 'palegreen', 'limegreen', 'darkgreen']
for i, (ax, title) in enumerate(my_iterator):    
    for (class_number, X), col in zip(dict_X.items(), color):
        if i ==3 :
            X = X.astype('int16')
            band = (X[:, 0] - X[:, 1]) / (X[:, 0] + X[:, 1])
            bandwidth = 0.01
        else:
            band = X[:, i]
            bandwidth = 3

        # get 100 values between the min and the max of band
        X_plot = np.linspace(band.min(), band.max(), 100)[:, np.newaxis]

        # define kernel density
        kde = KernelDensity(bandwidth=bandwidth)
        # learn kernel
        kde.fit(band.reshape(-1, 1))
        # predict on our X
        log_dens = kde.score_samples(X_plot)
        density = np.exp(log_dens)

        # plot it
        ax.plot(X_plot[:,0], density ,zorder=3, linewidth=3,
                linestyle='-', color=col)
        # fill with color the space between the curve and the line x=0
        ax.fill_between(X_plot[:,0], 0, density, alpha=0.5, color=col, label=class_number)
        ax.set_title(title)
        ax = plots.custom_bg(ax, x_grid=False)
    ax.set_title(title)
    ax = plots.custom_bg(ax, x_grid=False, minor=False)
    ax.legend()
```

<figure>
	<img class="zoomable" src="img/td_visu/output_49_0.png" title="" width="90%">
	<figcaption>Histogrammes interpolés des bandes Infra-rouge, Rouge, Vert et NDVI dans subplopts distincts, avec une distinction pour chaque subplots des différentes classes. </figcaption>
</figure>


#### Affichage interactif avec Plotly

==Plolty== est une librairie permettant de faire des graphiques interactifs qu'il est possible d'exporter ensuite au format html ou au format image. Vous trouverez [ici](https://plotly.com/python/) un lien explicitant son usage.

Il est possible de produire des graphiques via deux interfaces :

- le module `plotly.express` qui permet de produire simplement des graphiques à partir de `DataFrame` de la librairie ==Pandas== (décidément, tout est lié !) ;
- le module `plotly.graph_objects` qui permet de produire des graphiques plus avancées mais avec une interface plus complexe.

Nous allons principalement utiliser le module `plotly.express` dans cette séance.

La première fonction que nous allons utiliser est la fonction `histogram`. La première étape consiste à convertir nos matrices `X` et `Y` en `DataFrame` :


```python
# concatenate X and Y
data = np.concatenate((X, Y), axis=1)
# Conversion in data frame
df = pd.DataFrame(data, columns=['IR', 'R', 'G', 'id'])  
# Convert class label in str data type (necessary for the proper use
# of the histogram function)
df['id'] = df['id'].astype(str)
```

On peut ensuite afficher le contenu de ce DataFrame.

```python
import plotly.express as px
# define a correspondance dictionnary between class and color
color = {'1':'tan', '2':'palegreen', '3':'limegreen', '4':'darkgreen'}
fig = px.histogram(df,
                   x="R",  # band to plot
                   color="id", # column containing the class label
                   color_discrete_map=color, # color dictionnary
                   opacity=0.5,
                   barmode='overlay',
                   height = 700, width =1000) # figure output size

# output parameters
out_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
out_figure = os.path.join(out_folder, '1D_histogram.html')

# export to html file
fig.write_html(out_figure)
```


<figure>
	<iframe id="igraph" scrolling="no" style="border:none; background-color:#00000000" seamless="seamless" src="https://mlang.frama.io/sigma-teledec-avancee/plotly/1D_histogram.html" height="700" width="800" frameborder="0" allowtransparency="true"></iframe>       
	<figcaption>Histogrammes des quatres classe pour la bande rouge en utilisant la bibliothèque Plotly.</figcaption>
</figure>


Les avantages par rapport aux figures de ==Matplotlib== sont que vous pouvez :
- obtenir des informations au passage de la souris sur les données ;
- décider d'afficher ou non une classe.

<div class="block green-block"><p class="title exercice"></p>

Testez la production de graphiques interactifs avec la fonction `histogram`.

</div>


### 2D scatter plot

Une autre possiblité pour explorer le comportement spectrale d'échantillons est d'afficher conjointement leur valeur pour deux bandes à l'aide d'un scatter plot à deux dimensions.




```python
import plots
import matplotlib.pyplot as plt

# get desired bands
r = X[:, 1]
ir = X[:, 0]

# plot it
fig, ax = plt.subplots(figsize=(10, 10))
ax.plot(r, ir, '.', alpha=0.1, ms=5)

# custom the graph
plots.custom_bg(ax)
ax.set_ylabel('IR')
ax.set_xlabel('R')
```

<figure>
	<img src="img/td_visu/output_58_1.png" title="" width="75%">
	<figcaption>Scatter Plot 2D dans le plan spectrale Rouge (x) et Infra-rouge (y).</figcaption>
</figure>


De la même manière que pour les histogrammes 1D, vous pouvez afficher des couleurs différentes pour chaque classe. Utilisons pour cela le dictionnaire `dict_X` :


```python
import plots
import matplotlib.pyplot as plt

# define colors
color = ['tan', 'palegreen', 'limegreen', 'darkgreen']

# define a new figure
fig, ax = plt.subplots(figsize=(10, 10))

# plot each class
for (class_number, X), col in zip(dict_X.items(), color):

    r = X[:, 1]
    ir = X[:, 0]
    ax.plot(r, ir, '.',
            alpha=0.1, # set transparency
            ms=5, # set point size
            color=col,
            label=class_number)

#custom grah
plots.custom_bg(ax)
ax.set_ylabel('IR')
ax.set_xlabel('R')
ax.legend()
```

<figure>
	<img src="img/td_visu/output_60_1.png" title="" width="75%">
	<figcaption>Scatter Plot 2D des classes dans le plan spectrale Rouge (x) et Infra-rouge (y).</figcaption>
</figure>


Certaines classes ont des comportements spectraux similaires et il n'est pas facile de les distinguer.

<div class="block green-block"><p class="title exercice"></p>

Reproduisez la figure ci-dessus et jouez sur les paramètres de transparence `alpha` et de taille des points (`ms`) pour essayer d'améliorer le rendu.

</div>



Une alternative est d'utiliser la fonction `scatter` de ==poltly== :


```python
import pandas as pd
import plotly.express as px


# convert data into dataframe
data = np.concatenate((X, Y), axis=1)
df = pd.DataFrame(data, columns=['IR', 'R', 'G', 'id'])
df['id'] = df['id'].astype(str)

# define color dictionnary
color = {'1':'tan', '2':'palegreen', '3':'limegreen', '4':'darkgreen'}

# plot data
fig = px.scatter(df,
                 x="R", y="IR",  # desired bands
                 color="id",  ## column containing the class label
                 hover_data=['id'],
                 color_discrete_map=color,  #color dictionnary
                 opacity=0.5,  # point opacity
                 height = 1000, width =1000)  # output figure size

# output parameters
out_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
out_figure = os.path.join(out_folder, '2D_scatter_plot.html')

#export graph
fig.write_html(out_figure)
```

<figure>
	<iframe id="igraph" scrolling="no" style="border:none; background-color:#00000000" seamless="seamless" src="https://mlang.frama.io/sigma-teledec-avancee/plotly/2D_scatter_plot.html" height="700" width="800" frameborder="0" allowtransparency="true"></iframe>   
	<figcaption>Scatter Plot 2D des classes dans le plan spectrale Rouge (x) et Infra-rouge (y) en utilisant la bibliothèque Plotly.</figcaption>
</figure>


Certaines classes sont toujours superposées mais vous pouvez afficher successivement les différentes classes pour une meilleure lisibilité.

<div class="block green-block"><p class="title exercice"></p>

Testez la production de scatter plot 2D interactifs avec la fonction `scatter`.

</div>

### 3D scatter plot

Nous allons cette fois afficher les valeurs spectrales des échantillons de trois bandes en même temps en utilisant la fonction `scatter_3D` de ==Plotly==.

Encore une fois, il faut préparer le `DataFrame` à afficher.

```python
import pandas as pd

color = {'1':'tan', '2':'palegreen', '3':'limegreen', '4':'darkgreen'}
data = np.concatenate((X, Y), axis=1)
df = pd.DataFrame(data, columns=['IR', 'R', 'G', 'id'])
df['id'] = df['id'].astype('int').astype(str)
```

Puis vous pouvez utiliser la fonction `scatter_3D`.

```python
import plotly.express as px
fig = px.scatter_3d(df, x="R", y="IR", z="G",
                    color="id",
                    hover_data=['id'],
                    color_discrete_map=color,
                    opacity=0.5,
                    height = 1000, width =1000)
out_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
out_figure = os.path.join(out_folder, '3D_scatter_plot.html')
fig.write_html(out_figure)
```

<figure>
	<iframe id="igraph" scrolling="no" style="border:none; background-color:#00000000" seamless="seamless" src="https://mlang.frama.io/sigma-teledec-avancee/plotly/3D_scatter_plot.html" height="700" width="800" frameborder="0" allowtransparency="true"></iframe>
	<figcaption>Scatter Plot 3D des échantillons dans l'espace spectrale Rouge (x), Infra-rouge (y) et Vert (z) .</figcaption>
</figure>


La taille des points vous parait certainement trop grande. Il n'est malheureusement pas possible de définir leur taille via la fonction `scatter_3d` du module `express` (voir [ici](https://plotly.com/python-api-reference/generated/plotly.express.scatter_3d.html) la documentation de la fonction). C'est une des limites de ce module. Pour changer la taille des points, il faut mettre à jour *a posteriori* l'objet `fig` à l'aide de la méthode `update_traces`.


```python
import plotly.express as px
fig = px.scatter_3d(df, x="R", y="IR", z="G",
                    color="id",
                    hover_data=['id'],
                    color_discrete_map=color,
                    opacity=0.5,
                    height = 1000, width =1000)
fig.update_traces(marker=dict(size=2,
                              line=dict(width=0.2,
                                        color='DarkSlateGrey')
                             )
                 )
out_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
out_figure = os.path.join(out_folder, '3D_scatter_plot_V2.html')
fig.write_html(out_figure)

```

<figure>
	<iframe id="igraph" scrolling="no" style="border:none; background-color:#00000000" seamless="seamless" src="https://mlang.frama.io/sigma-teledec-avancee/plotly/3D_scatter_plot_V2.html" height="700" width="800" frameborder="0" allowtransparency="true"></iframe>
	<figcaption>Scatter Plot 3D des échantillons dans l'espace spectrale Rouge (x), Infra-rouge (y) et Vert (z) avec une taille des points diminuée.</figcaption>
</figure>


**Remarque** : Ce n'est pas le cas des fonctions du module `express` mais pour le module `graph_objects` les paramètres d'entrée sont souvent à fournir sous forme de dictionnaire ce qui donne une syntaxe peu commune en python. C'est dû au fait que la librairie ==Plotly== est à l'origine une librairie JavaScript où cette syntaxe est plus commune.


<div class="block green-block"><p class="title exercice"></p>

Reproduisez cet affichage 3D avec cette fois ci les bandes Rouge, Infra Rouge, et le NDVI :

1. Construisez un `DataFrame` contenant le NDVI ;
2. Affichez les bonnes bandes avec `scatter_3d`.

<div class="question">
  <label>Solution
  <input type="checkbox">
  <i class="arrow"></i>
	<span class="reponse">

  ```python
  import pandas as pd

  color = {'1':'tan', '2':'palegreen', '3':'limegreen', '4':'darkgreen'}
  r = X[:, 1].astype('int16')
  ir = X[:, 0].astype('int16')
  ndvi = (ir - r)/(ir + r)
  ndvi = np.atleast_2d(ndvi).T
  data = np.concatenate((X, Y, ndvi), axis=1)
  df = pd.DataFrame(data, columns=['IR', 'R', 'G', 'id', "ndvi"])
  df['id'] = df['id'].astype('int').astype(str)
  ```


  ```python
  import plotly.express as px
  fig = px.scatter_3d(df, x="R", y="IR", z="ndvi",
                      color="id",
                      hover_data=['id'],
                      color_discrete_map=color,
                      opacity=0.5,
                      height = 1000, width =1000)
  fig.update_traces(marker=dict(size=2,
                                line=dict(width=0.2,
                                          color='DarkSlateGrey')),
                    )
  out_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
  out_figure = os.path.join(out_folder, '3D_scatter_plot_V2_ndvi.html')
  fig.write_html(out_figure)
  ```

  <figure>
	<center><iframe id="igraph" scrolling="no" style="border:none; background-color:#00000000" seamless="seamless" src="https://mlang.frama.io/sigma-teledec-avancee/plotly/3D_scatter_plot_V2_ndvi.html" height="700" width="800" frameborder="0" allowtransparency="true"></iframe></center>
	<figcaption>Scatter Plot 3D des échantillons dans l'espace spectrale Rouge (x), Infra-rouge (y) et Ndvi (z)</figcaption>
  </figure>


</span>
</div>
<br>

Le NDVI apporte-t-il selon vous plus d'informations que les bandes Rouge et Infra-rouge ?

</div>

<!-- selector=dict(mode='markers') -->

### Un peu de pratique

<div class="block green-block">
<p class="title"> EXERCICE (Facultatif)</p>

Sur un plot 2D ou 3D (au choix) interactif :

1. Faites en sorte que l'identifiant du polygone auquel appartient un pixel s'affiche au survol de la souris

Vous aurez besoin pour cela d'une image d'échantillons qui contient cette fois-ci les identifiants des polyones (au lieu de leur classe) vous pouvez télécharger l'image `sample_strata_id.tif` [ici](https://filesender.renater.fr/?s=download&token=b9a8576b-82e6-4eeb-8413-17dab160d575)

2. affichez les échantillons d'une seule classe avec comme contrainte que les pixels appartenant à des polygones différents aient une couleur différente (et donc où les pixels d'un même polygone ont la même couleur).
</div>


## Visualiser des images

Nous allons voir dans cette partie comment afficher des images avec ==Matplotlib==. Nous allons afficher l'image `imagette_fabas.tif` que vous avez téléchargée pendant le deuxième TD.

### Affichage basique

La première étape consiste à charger l'image sous forme d'un tableau ==Numpy==.


```python
import sys
sys.path.append('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/python_scripts')
import matplotlib.pyplot as plt
import read_and_write as rw
```


```python
fabas_filename = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/imagette_fabas.tif'
img = rw.load_img_as_array(fabas_filename)
```

Il faut ensuite utiliser la fonction `imshow` de ==Matplotlib==:


```python
import matplotlib.pyplot as plt
fig, ax = plt.subplots(figsize=(10, 10))
ax.imshow(img)
ax.axis("off")  # delete borders
```

    Clipping input data to the valid range for imshow with RGB data ([0..1] for floats or [0..255] for integers).


<figure>
	<img src="img/td_visu/output_86_2.png" title="" width="50%">
	<figcaption>Affichage de l'imagette avec les paramètres par défaut.</figcaption>
</figure>




Regardons un extrait de la [documentation](https://matplotlib.org/3.3.3/api/_as_gen/matplotlib.pyplot.imshow.html) de cette fonction :

<center><img src="img/td_visu/extrait_doc.png" title="" width="95%"></center>

<div class="block green-block"><p class="title exercice"></p>

Il est possible d'utiliser plusieurs formats de matrice-image en entrée : lequel avons nous utilisé et est-ce le bon ?

</div>

<div class="block green-block"><p class="title exercice"></p>

Affichez l'image en composition infrarouge couleur :

<figure>
	<img src="img/td_visu/output_93_2.png" title="" width="50%">
	<figcaption>Affichage de l'imagette en Infra-rouge couleur, sans bande de transparence.</figcaption>
</figure>


<div class="question">
  <label>Solution
  <input type="checkbox">
  <i class="arrow"></i>
	<span class="reponse">

  ```python
  irc = img[:, :, :3].copy()
  irc[:, :,0] = img[:, :,3]
  irc[:, :,1] = img[:, :,0]
  irc[:, :,2] = img[:, :,1]
  fig, ax = plt.subplots(figsize=(10, 10))
  ax.imshow(irc)
  ax.axis("off")
  ```

      Clipping input data to the valid range for imshow with RGB data ([0..1] for floats or [0..255] for integers).

</span>
</div>

<br>

</div>




### Améliorer le contraste de l'image


Nous allons maintenant voir comment améliorer le contraste d'une image.

Par défaut,  `imshow` considère que le maximum des données est de 255 et que le minimum est 0 pour les matrices d'entiers. Or, notre image est codée en 'uint16' et le maximum des valeurs est supérieur à 255. Regardons l'histogramme de la bande IR pour s'en assurer :


<figure>
	<img src="img/td_visu/output_98_0.png" title="" width="75%">
	<figcaption>Histogramme interpolé de la bande Infra-rouge. Les limites en rouge indiquent la plage de valeurs utilisée par la fonction imshow.</figcaption>
</figure>



<div class="question">
  <label>Code utilisé pour générer la figure ci-dessus.
  <input type="checkbox">
  <i class="arrow"></i>
	<span class="reponse">


  ```python
  import matplotlib.pyplot as plt
  import plots
  from sklearn.neighbors import KernelDensity

  fig, ax = plt.subplots(figsize=(10, 10))

  band = img[:,:,3].flatten()
  # get 100 values between the min and the max of band
  X_plot = np.linspace(band.min(), band.max(), 100)[:, np.newaxis]

  # define kernel density
  kde = KernelDensity(bandwidth=5)
  # learn kernel
  kde.fit(band.reshape(-1, 1))
  # predict on our X
  log_dens = kde.score_samples(X_plot)
  density = np.exp(log_dens)

  # plot it
  ax.plot(X_plot[:,0], density , color='royalblue',
          zorder=3, linewidth=3,
          linestyle='-')
  ax.fill_between(X_plot[:,0], 0, density, alpha=0.5, color='royalblue', label=label)
  ax.legend()
  ax = plots.custom_bg(ax, x_grid=False)
  min, max = ax.get_ylim()
  _ = ax.vlines([0, 255], min, max, color='red', ls='--')
  ```

</span>
</div>



Les lignes en rouge indiques les valeurs prises en compte par `imshow`. Toutes les valeurs supérieures à 255 sont coupées et sont changées en 255 (d'où les messages d'avertissement `Clipping input data to the valid range for imshow with RGB data ([0..1] for floats or [0..255] for integers).`). C'est pour ça que l'image apparait top lumineuse et peu contrastée dans l'exemple précédent.

Pour remédier à ce problème, nous pouvons ré-échelonner les valeurs de notre image pour qu'elles soient comprises entre 0 et 255 en utilisant la formule ci-dessous :

$$img_{scaled} = min_{dst} + \frac{max_{dst} - min_{dst}}{max_{src} - min_{src}} \times (img - min_{src})$$

Avec :
- $min_{dst}$ et  $max_{dst}$, le minimum et le maximum de la nouvelle plage de valeur ;
- $min_{src}$ et $max_{src}$,  le minimum et le maximum de l'ancienne plage de valeur.

En Python on peut traduire la formule par :


```python
min_src = img.min(axis=(0, 1))
max_src = img.max(axis=(0, 1))
min_dst = 0
max_dst = 255
img_rescale = min_dst + (max_dst-min_dst) / (max_src-min_src) * (img-min_src)
```

On peut vérifier que le maximum de chaque bande est bien maintenant de 255 :


```python
print(img_rescale.max(axis=(0,1)))
```

    [255. 255. 255. 255.]


L'histogramme de la bande Infra rouge est bien compris entre 0 et 255 :


<figure>
	<img src="img/td_visu/output_107_0.png" title="" width="75%">
	<figcaption>Histogramme interpolé de la bande Infra-rouge ré-échelonnée de 0 à 255. Les limites en rouges indiquent les quantiles 2 et 98 de la distribution. </figcaption>
</figure>


<div class="question">
  <label>Code utilisé pour générer la figure ci-dessus.
  <input type="checkbox">
  <i class="arrow"></i>
	<span class="reponse">


  ```python
  import matplotlib.pyplot as plt
  import plots
  from sklearn.neighbors import KernelDensity

  fig, ax = plt.subplots(figsize=(10, 10))

  band = img_rescale[:,:,3].flatten()
  # get 100 values between the min and the max of band
  X_plot = np.linspace(band.min(), band.max(), 100)[:, np.newaxis]

  # define kernel density
  kde = KernelDensity(bandwidth=1)
  # learn kernel
  kde.fit(band.reshape(-1, 1))
  # predict on our X
  log_dens = kde.score_samples(X_plot)
  density = np.exp(log_dens)

  # plot it
  ax.plot(X_plot[:,0], density , color='royalblue',
          zorder=3, linewidth=3,
          linestyle='-')
  ax.fill_between(X_plot[:,0], 0, density, alpha=0.5, color='royalblue', label='IR')
  ax.set_xlim(0, 255)
  ax.legend()
  ax = plots.custom_bg(ax, x_grid=False)

  p2 = np.percentile(band, 2)
  p98 = np.percentile(band, 98)
  min, max = ax.get_ylim()
  _ = ax.vlines([p2 , p98] , min, max, color='red', ls='--')
  ```

</span>
</div>



Et le contraste de notre image s'est amélioré :


```python
import matplotlib.pyplot as plt
fig, ax = plt.subplots(figsize=(10, 10))
irc = img_rescale[:, :, :3].copy().astype('uint8')
irc[:, :,0] = img_rescale[:, :,3]
irc[:, :,1] = img_rescale[:, :,0]
irc[:, :,2] = img_rescale[:, :,1]
ax.imshow(irc)
ax.axis("off")
```


<figure>
	<img src="img/td_visu/output_109_1.png" title="" width="50%">
	<figcaption>Affichage de l'imagette en composition Infrarouge couleur ajustant le contraste par rapport au minimum et maximum des bandes. </figcaption>
</figure>



Nous venons de faire l'équivalent de l'opération sous QGIS:

<figure>
	<img src="img/td_visu/qgis_min_max.png" title="" width="60%">
	<figcaption>Configuration sous QGIS d'un affichage de l'imagette en composition Infrarouge couleur ajustant le contraste par rapport au minimum et maximum des bandes.</figcaption>
</figure>


Une pratique courante est également d'étirer l'histogramme des bandes du 2ième quantile au 98ième quantile. Au lieu de fixer `min_src = img.min(axis=(0,1))` on prendra `min_src = np.percentile(img, 2, axis=(0, 1))`.

Il faudra cependant prendre garde à bien changer les valeurs inférieures et supérieures aux quantiles avant l'échelonnage :


```python
max_src = np.percentile(img, 98, axis=(0, 1))
min_src = np.percentile(img, 2, axis=(0, 1))
min_dst = 0
max_dst = 255

for idx_band, (p2, p98) in enumerate(zip(min_src, max_src)):

    img[(img[:,:, idx_band] < p2), idx_band] = p2
    img[(img[:,:, idx_band] > p98), idx_band] = p98
```

Puis échelonner :


```python
img_rescale = min_dst + (max_dst - min_dst) / (max_src - min_src) * (img - min_src)
```

On peut vérifier que les nouveaux maxima sont bien 255 :


```python
img_rescale.max(axis=(0,1))
```

    array([255., 255., 255., 255.])


L'histogramme est ainsi plus étiré :

<figure>
	<img src="img/td_visu/output_120_0.png" title="" width="75%">
	<figcaption>Histogramme interpolé de la bande Infra-rouge ré-échelonnée de 0 à 255 en considérant les quantiles 2 et 98. </figcaption>
</figure>


<div class="question">
  <label>Code utilisé pour afficher la figure ci-dessus.
  <input type="checkbox">
  <i class="arrow"></i>
	<span class="reponse">


  ```python
  import matplotlib.pyplot as plt
  import plots
  from sklearn.neighbors import KernelDensity

  fig, ax = plt.subplots(figsize=(10, 10))


  band = img_rescale[:,:,3].flatten()
  # get 100 values between the min and the max of band
  X_plot = np.linspace(band.min(), band.max(), 100)[:, np.newaxis]

  # define kernel density
  kde = KernelDensity(bandwidth=2)
  # learn kernel
  kde.fit(band.reshape(-1, 1))
  # predict on our X
  log_dens = kde.score_samples(X_plot)
  density = np.exp(log_dens)

  # plot it
  ax.plot(X_plot[2:-2,0], density[2:-2] , color='royalblue',
          zorder=3, linewidth=3,
          linestyle='-')
  ax.fill_between(X_plot[3:-3,0], 0, density[3:-3], alpha=0.5, color='royalblue', label=label)
  ax.legend()
  ax.set_xlim(0, 255)
  ax = plots.custom_bg(ax, x_grid=False)
  ```

</span>
</div>



Et le contraste de l'image est encore amélioré :


```python
import matplotlib.pyplot as plt
fig, ax = plt.subplots(figsize=(10, 10))
irc = img_rescale[:, :, :3].copy().astype('uint8')
irc[:, :,0] = img_rescale[:, :,3]
irc[:, :,1] = img_rescale[:, :,0]
irc[:, :,2] = img_rescale[:, :,1]
ax.imshow(irc)
ax.axis("off")
```


<figure>
	<img src="img/td_visu/output_122_1.png" title="" width="50%">
	<figcaption> Affichage de l'imagette en composition Infrarouge couleur ajustant le contraste par rapport aux quantiles 2 et 98 des bandes.</figcaption>
</figure>


Nous venons de faire l'équivalent de l'opération sous QGIS:

<figure>
	<img src="img/td_visu/qgis_percentile.png" title="" width="60%">
	<figcaption>Configuration sous QGIS d'un affichage de l'imagette en composition Infrarouge couleur ajustant le contraste par rapport aux quantiles 2 et 98 des bandes.</figcaption>
</figure>

<div class="block green-block"><p class="title exercice"></p>

Testez différents affichages :

1. Composition colorée vraies couleurs :
  - En étirant la dynamique de l'image par rapport au minimum et maximum de chaque bande.
  - En étirant la dynamique de l'image par rapport aux quantiles 2 et 98 chaque bande.
2. Composition colorée Infra-rouge couleurs :
  - En étirant la dynamique de l'image par rapport au minimum et maximum de chaque bande.
  - En étirant la dynamique de l'image par rapport aux quantiles 2 et 98 chaque bande.


</div>

<br>
<br>
<br>
