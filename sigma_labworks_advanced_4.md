---
title: Télédétection - Approfondissemnt
author: Marc Lang
html:
  toc: true
  embeb_local_images: true
  embed_svg: true
  cdn: true
  offline: true
export_on_save:
    html: True
---

<!-- import de fichier de style -->


@import "less/question_answer.less"
@import "less/blocks.less"
@import "less/numbered_headsection.less"
@import "less/redish_section.less"
@import "less/block_code_even.less"
@import "less/inline_code_red.less"
@import "less/blue_highlight.less"
@import "less/pd_table.less"
@import "less/image_zoom.less"
@import "less/link.less"
@import "less/fig_caption.less"
@import "less/counters.less"
@import "less/bold.less"
@import "less/styling.less"  
@import "less/sidebar_toc.less"


<center><img src="img/logo_ensat.png" title="" width="50%"></center>
<center><section style="font-size:300%"> Télédétection - Approfondissement </section></center>

<center><section style="font-size:200%; color:#9d1300; width: 65%"> TD - 4 : Mes premiers pas dans la classification avec python (partie 2) </section></center>

<br>
<center><section style="font-size:150%"> Marc Lang </section></center>


<p> <br> <br></p>



<span style="font-size:200%"> Table des matières </span>


<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=true} -->
<!-- code_chunk_output -->

1. [Objectif](#objectif)
2. [Séparation d'un jeu d'échantillons de référence en jeux d'entraînement et de validation](#séparation-dun-jeu-déchantillons-de-référence-en-jeux-dentraînement-et-de-validation)
    1. [Cas où beaucoup d'échantillons sont disponibles](#cas-où-beaucoup-déchantillons-sont-disponibles)
    2. [Les jeux d'entraînement et de validation sont-ils vraiment indépendants ?](#les-jeux-dentraînement-et-de-validation-sont-ils-vraiment-indépendants)
        1. [Extraction des échantillons à partir d'un fichier de points](#extraction-des-échantillons-à-partir-dun-fichier-de-points)
            1. [Principe](#principe)
            2. [Conversion des coordonnées géographiques en coordonnées images](#conversion-des-coordonnées-géographiques-en-coordonnées-images)
            3. [Extraction des coordonnées géographiques d'un fichier de points](#extraction-des-coordonnées-géographiques-dun-fichier-de-points)
            4. [Lecture des valeurs et formattage](#lecture-des-valeurs-et-formattage)
        2. [Classification à partir des échantillons "point"](#classification-à-partir-des-échantillons-point)
    3. [Cas où peu d'échantillons sont disponibles](#cas-où-peu-déchantillons-sont-disponibles)
        1. [Répéter plusieurs fois la validation](#répéter-plusieurs-fois-la-validation)
            1. [Matrice de confusion](#matrice-de-confusion)
            2. [Calcul de l'Accord global moyen](#calcul-de-laccord-global-moyen)
            3. [Moyenne des rapports de classification](#moyenne-des-rapports-de-classification)
            4. [Affichage des qualités](#affichage-des-qualités)
        2. [Cross validation](#cross-validation)
        3. [Et la classification dans tout ça ?](#et-la-classification-dans-tout-ça)
3. [Regroupement de classes (compromis entre précision thématique et statistique)](#regroupement-de-classes-compromis-entre-précision-thématique-et-statistique)
    1. [Évaluation de la qualité](#évaluation-de-la-qualité)
    2. [Regroupement de classe d'une image classifiée](#regroupement-de-classe-dune-image-classifiée)

<!-- /code_chunk_output -->

## Objectif

L'objectif de ce TD est de complexifier petit à petit la chaîne de traitements que vous avez vue précédemment. Nous allons :

- utiliser différentes stratégies de validation afin d'obtenir une meilleure estimation de la qualité des classifications produites ;
- découvrir la librairie ==GeoPandas== lors de l'extraction d'un jeu d'échantillons sous forme de fichier de points ;
- appronfondir votre pratique des API de ==Matplotlib== et ==Pandas== ;
- apprendre à regrouper des classes et évaluer l'effet de cette opération sur la qualité de vos cartes.

## Séparation d'un jeu d'échantillons de référence en jeux d'entraînement et de validation

Dans le précédent TD nous avons vu un chaîne de traitements simplifiée dans laquelle la validation du modèle se faisait sur le même jeu de données que celui qui avait servi à faire la validation, ce qui est une mauvaise chose car nous n'avons aucune idée de la qualité de notre de classification sur des pixels inconnus. Il est alors impossible de vérifier qu'il n'y ait pas eu de sur-apprentissage et que la qualité du modèle soit sur-estimée.

### Cas où beaucoup d'échantillons sont disponibles

Il faut en réalité toujours séparer son jeu d'échantillons entre un jeu d'apprentissage (ou "train") et un jeu de validation (ou "test"). Vous pouvez voir ci-dessous la chaîne de traitements mis à jour :

<figure>
	<img class="zoomable" src="img/td_classification_part2/classification_process_v2.png" title="" width="100%">
	<figcaption>Chaine traitements de classification avec estimation de la qualité du modèle sur un jeu de validation indépendant.</figcaption>
</figure>



Dans le cas où vous disposez de nombreux échantillons, comme pour ceux issus de `sample_strata.shp`(plus de 25 000 pixels), vous pouvez conserver une majorité des échantillons pour la validation du modèle pour avoir une estimation de la qualité plus robuste. Nous garderons dans cette partie 70% des pixels pour la validation.

Cette opération peut se faire avec la fonction `train_test_split` du module `model_selection` de ==Scikit-Learn== :


```python
import sys
sys.path.append('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/python_scripts')
import os
from sklearn.model_selection import train_test_split

# personal librairies
import classification as cla


# 1 --- define parameters
# inputs
my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
sample_filename = os.path.join(my_folder, 'sample_strata.tif')
image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')

# 2 --- extract samples
X, Y, t = cla.get_samples_from_roi(image_filename, sample_filename)

# split samples
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.7)
```

Cette fonction permet de séparer de manière aléatoire vos matrices de variables (`X`) et de labels (`Y`) en deux jeux différents.

**Remarque** : vous pouvez au besoin renseigner l'argument `random_state` pour pouvoir appliquer la même séparation "aléatoire" sur plusieurs jeux de données différents (voir détails sur la [documentation](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.train_test_split.html#sklearn.model_selection.train_test_split) de la fonction).

<div class="block green-block"><p class="title exercice"></p>

1. Testez plusieurs fois la fonction `train_test_split` et vérifiez que la séparation est bien aléatoire ;
2. Rajoutez l'étape de séparation des échantillons dans le script de classification complet que vous avez produit lors de la dernière séance et corrigez le en conséquence ;
3. Lancez la classification  :

    3.1. Commentez les résultats : sont-ils similaires à ce que vous avez trouvé dans le précédent TD ?
    3.2. Re-lancez plusieurs fois la classification pour voir si vous obtenez des résultats différents ?
    3.3. Changez la proportion d'échantillons utilisée pour l'entraînement pour voir si cela influence les résultats.

<div class="question">
	<input type="checkbox" id="1">
	<p><label for="1">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import sys
  sys.path.append('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/python_scripts')

  import os
  import numpy as np
  from sklearn import tree
  from sklearn.model_selection import train_test_split
  from sklearn.metrics import confusion_matrix, classification_report, \
      accuracy_score

  # personal librairies
  import classification as cla
  import read_and_write as rw
  import plots

  # 1 --- define parameters
  # inputs
  my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
  sample_filename = os.path.join(my_folder, 'sample_strata.tif')
  image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')

  # Sample parameters
  test_size = 0.7

  # outputs
  suffix = '_v2'
  out_classif = os.path.join(my_folder, 'ma_classif{}.tif'.format(suffix))
  out_matrix = os.path.join(my_folder, 'ma_matrice{}.png'.format(suffix))
  out_qualite = os.path.join(my_folder, 'mes_qualites{}.png'.format(suffix))

  # 2 --- extract samples
  X, Y, t = cla.get_samples_from_roi(image_filename, sample_filename)
  X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_size)


  # 3 --- Train
  #clf = SVC(cache_size=6000)
  clf = tree.DecisionTreeClassifier(max_leaf_nodes=10)
  clf.fit(X_train, Y_train)

  # 4 --- Test
  Y_predict = clf.predict(X_test)

  # compute quality
  cm = confusion_matrix(Y_test, Y_predict)
  report = classification_report(Y_test, Y_predict, labels=np.unique(Y_predict), output_dict=True)
  accuracy = accuracy_score(Y_test, Y_predict)

  # display and save quality
  plots.plot_cm(cm, np.unique(Y_predict), out_filename=out_matrix)
  plots.plot_class_quality(report, accuracy, out_filename=out_qualite)

  # 5 --- apply on the whole image

  # load image
  X_img, _, t_img = cla.get_samples_from_roi(image_filename, image_filename)

  # predict image
  Y_img_predict = clf.predict(X_img)

  # reshape
  ds = rw.open_image(image_filename)
  nb_row, nb_col, _ = rw.get_image_dimension(ds)

  #initialization of the array
  img = np.zeros((nb_row, nb_col, 1), dtype='uint8')
  img[t_img[0], t_img[1], 0] = Y_img_predict

  # write image
  ds = rw.open_image(image_filename)
  rw.write_image(out_classif, img, data_set=ds, gdal_dtype=None,
              transform=None, projection=None, driver_name=None,
              nb_col=None, nb_ligne=None, nb_band=1)
  ```


  <img  src="img/td_classification_part2/output_11_1.png" title="" width="40%">

  <img  src="img/td_classification_part2/output_11_2.png" title="" width="59%">

</span>
</div>
<br>

</div>


**Remarque** : dans ce cas la classification n'est produite qu'à partir de 30% des échantillons originaux.

### Les jeux d'entraînement et de validation sont-ils vraiment indépendants ?

Nous avons certes séparé de manière aléatoire nos pixels échantillons en un jeu de validation et d'entraînement mais ... sans tenir compte de leur appartenance à un même polygone ! Or, deux pixels appartenant au même polygone sont probablement très similaires : s'entraîner sur l'un et tester sur l'autre peut entraîner un biais dans l'estimation de la qualité du modèle.

Une solution (parmi d'autres) à ce problème est de ne sélectionner qu'un pixel de chaque polygone. Vous avez à votre disposition un fichier `sample_strata_centroides.shp` qui correspond à un fichier de points où chaque point est le centroïde d'un polygone du fichier `sample_strata.shp`.

L'utilisation de ce nouveau fichier implique en revanche d'extraire les valeurs spectrales à partir d'un fichier de points et non plus de polygones. Je vous propose ainsi une autre méthode que celle vue précédemment pour réaliser l'extraction.

#### Extraction des échantillons à partir d'un fichier de points

##### Principe

Dans le cas d'un fichier de points, le problème est assez évident : il s'agit de trouver le pixel sur lequel se trouve un point et d'extraire sa valeur. Nous allons donc passer par les étapes illustrées dans le schéma suivant :

<figure>
	<img class="zoomable" src="img/td_classification/extraction_point.png" title="" width="100%">
	<figcaption>Chaîne de traitements pour l'extraction d'échantillons sous forme de points.</figcaption>
</figure>


##### Conversion des coordonnées géographiques en coordonnées images

Il est possible de trouver les coordonnées *image* d'un pixel à partir de ses coordonnées géographiques, de la taille des pixels de l'image ($pixelWidth$ et $pixelHeight$), et des coordonnées à l'origine de l'image ($x_0$ et $y_0$) (voir Figure et équations ci-dessous).

<figure>
	<img src="img/td_classification/coordonnées.png" title="" width="50%">
	<figcaption>Lien entre coordonnées images et coordonnées géographiques (Source : cours de Mathieu Fauvel).</figcaption>
</figure>



$$x_s = \frac{(x − x_0)}{pixelWidth}$$
$$y_s = \frac{(y − y_0)}{pixelHeight}$$

<div class="block green-block"><p class="title exercice"> </p>

Définissez une fonction `xy_to_rowcol()` qui retourne la ligne et la colonne d'un pixel à partir des coordonnées géographiques $x$ et $y$ d'un pixel. Vous renseignerez en plus comme paramètre d'entrée le chemin de l'image.

**Astuce** : vous avez accès aux fonctions précédemment développées dans le script `read_and_write.py`.

<div class="question">
	<input type="checkbox" id="1.1">
	<p><label for="1.1">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import sys
  sys.path.append('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/python_scripts')
  import read_and_write as rw

  def xy_to_rowcol(x, y, image_filename):
      """
      Convert geographic coordinates into row/col coordinates

      Paremeters
      ----------
      x : float
        x geographic coordinate
      y : float
          y geographic coordinate
      image_filename : str
          Path of the image.

      Returns
      -------
      row : int
      col : int
      """
      # get image infos
      data_set = rw.open_image(image_filename)
      origin_x, origin_y = rw.get_origin_coordinates(data_set)
      psize_x, psize_y = rw.get_pixel_size(data_set)

      # convert x y to row col
      col = int((x - origin_x) / psize_x)
      row = - int((origin_y - y) / psize_y)

      return row, col
  ```
</span>
</div>
<br>
</div>


##### Extraction des coordonnées géographiques d'un fichier de points

Cette étape pose la question plus générale de la lecture de fichiers vecteur en Python. Comment faire ? Plusieurs librairies existent pour lire des fichiers vecteurs. Afin de varier la diversité des exemples vus pendant les cours, nous allons utiliser la librairie [==GeoPandas==](https://geopandas.org/) au lieu de la librairie ==OGR==. Par ailleurs, cela nous permettra d'approfondir l'usage l'API de la librairie [==Pandas==](https://pandas.pydata.org/docs/user_guide/index.html#user-guide) qui permet de gérer des tableaux de type `DataFrame` car  [==GeoPandas==](https://geopandas.org/) est basée sur cette dernière.

<div class="block blue-block">
<p class="title"> DIGRESSION </p>

**Qu'est ce que [==Pandas==](https://pandas.pydata.org/docs/user_guide/index.html#user-guide) et à quoi sert la librairie ?**

==Pandas== est une librairie qui s’appuie sur ==NumPy== et qui permet de manipuler de grands volumes de données structurées de manière simple et intuitive sous forme de tableaux. Si ==Numpy== est très performmant pour faire des calculs sur des données numériques, il n'est pas aisé d'indexer des données variées, de différents types. C'est possible, mais compliqué. C'est là où intervient [==Pandas==](https://pandas.pydata.org/docs/user_guide/index.html#user-guide).

Le type de base disponible dans la librairie est la Serie (`pandas.Series`) qui peut être considérée comme un tableau de données à une dimension (grossièrement équivalent à une liste). L’extension en deux dimensions est un DataFrame (`pandas.DataFrame`). C’est un simple tableau dont les séries constituent les colonnes. À les différences des tableaux ==Numpy== les lignes et les colonnes possèdent des index. Ces index sont:

- pour les colonnes,  les noms des colonnes (généralement des chaînes de caractères), on peut les afficher avec l'instruction `df.columns`;
- pour les lignes, des nombres entiers (numéros des lignes), ou des chaînes de caractères, ou encore des `DatetimeIndex` ou `PeriodIndex` pour les séries temporelles, on peut les afficher avec l'instruction `df.index`.

<figure>
	<img src="img/td_classification/panda.png" title="Source : https://portailsig.org/content/python-geopandas-ou-le-pandas-spatial.html" width="55%">
	<figcaption>Stucture des objets "DataFrame"</figcaption>
</figure>



Un `DataFrame` est l’équivalent d’une matrice ==NumPy== dont chaque élément d'une colonne (Serie) est de même type (n’importe quel type de données, objets Python, nombres, dates, textes, binaires) et qui peut contenir des valeurs manquantes. C’est aussi l’équivalent des Data Frame du langage R.

La syntaxe n'est pas évidente à prendre en main. Elle diffère sensiblement de celle de ==Numpy==, aussi nous utiliserons des syntaxes élémentaires pour manipuler ces objets.

Pour plus d'infos, vous pouvez vous référer à :

- ce [site](https://portailsig.org/content/python-geopandas-ou-le-pandas-spatial.html) dont sont issues bon nombre des explications que vous lisez ;
- la documentation de [==Pandas==](https://pandas.pydata.org/docs/user_guide/index.html#user-guide) : https://pandas.pydata.org/docs/user_guide/index.html#user-guide .


**Et ==GeoPandas== dans tout ça ?**

==GeoPandas== est en quelque sorte le PostGIS de ==Pandas== : elle rajoute une dimension spatiale aux données d'un `DataFrame` ainsi que la possibilité de réaliser des opérations géométriques sur les données.

Cette librairie rajoute les `GeoSeries` (géométries, en bleu clair sur la figure ci-dessous) et les `GeoDataFrames` (ou `DataFrame` avec une colonne  de type `GeoSeries`) aux structures de ==Pandas==. Le résultat final est une structure où tous les traitements de ==Pandas== sont possibles sur les colonnes oranges et où des traitements géomatiques sont en plus possibles sur la colonne bleu clair. Une ligne d'un `GeoDataFrame` contient donc les attributs et la géométrie d'un élément (point, ligne ou polygone). Enfin, nous verrons par la suite qu'une `GeoSeries` est en fait constituée d'objets de la bibliothèque ==Shapely==.

<figure>
	<img src="img/td_classification/geopandas.png" title="Source : https://portailsig.org/content/python-geopandas-ou-le-pandas-spatial.html" width="55%">
	<figcaption>Stucture des objets "GeoDataFrame"</figcaption>
</figure>



**Et donc ?**

Et donc les `GeoDataFrames` sont très utiles pour lire, écrire, manipuler des données vecteurs. Dans ces TDs nous ne ferons qu'effleurer les nombreuses possibilités qu'offre ==GeoPandas== car notre objectif est avant tout la manipulation de données raster. Gardez cependant dans un coin de votre tête l'existence de ces librairies.

</div>


Nous allons donc utiliser la librairie ==GeoPandas==. Avant de continuer, vous trouverez ci-dessous à titre illustratif un exemple de lecture de fichier vecteur avec la librairie ==OGR== (d'osgeo) :


```python
from osgeo import ogr

def get_geographic_coord_from_points(vector, field_name=None):
    """
    Get the geographic coordinates of a point vector file .
    Parameters
    ----------
    vector : str
        Path of the input vector file, any format supported by ogr.
    field_name : str, optional
        Field of the shapefile containing the id of each point.

    Return
    ------
    coord : list or dict
        If field_name is not indicated, list of coordinates (tuple) : [(x,y)].
        Else, dict of coordinates : {id_point : (x,y)}
    """
    # Get geographic coordinates of each point
    dataSource = ogr.Open(vector, 0)
    layer = dataSource.GetLayer()

    if not field_name:
        coord = []
    else:
        coord = {}
    for feature in layer:
        geom = feature.GetGeometryRef()
        pt = geom.Centroid().ExportToWkt()    # String
        coord_f = eval(','.join(pt.split(' ')[1:]))    # Transform into tuple
        if not field_name:
            coord.append(coord_f)
        else:
            coord[feature.GetField(field_name)] = coord_f
    driver = None
    return coord
```

Avec ==GeoPandas==, c'est beaucoup plus simple, il vous suffit de lire le fichier comme suit :


```python
import geopandas as gpd
fichier_point = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/sample_strata_centroides.shp'
gdf = gpd.read_file(fichier_point)
print('type of gdf : ', type(gdf))
gdf
```

    type of gdf :  <class 'geopandas.geodataframe.GeoDataFrame'>


<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>num</th>
      <th>class</th>
      <th>id</th>
      <th>geometry</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>sol nu</td>
      <td>527</td>
      <td>POINT (748365.906 6276577.033)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>4</td>
      <td>ligneux haut</td>
      <td>1</td>
      <td>POINT (748285.153 6276558.825)</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2</td>
      <td>herbe</td>
      <td>2</td>
      <td>POINT (748401.722 6276762.504)</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3</td>
      <td>ligneux bas</td>
      <td>3</td>
      <td>POINT (748325.353 6276728.435)</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1</td>
      <td>sol nu</td>
      <td>4</td>
      <td>POINT (748586.873 6276687.753)</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>519</th>
      <td>2</td>
      <td>herbe</td>
      <td>522</td>
      <td>POINT (750142.636 6273994.320)</td>
    </tr>
    <tr>
      <th>520</th>
      <td>2</td>
      <td>herbe</td>
      <td>523</td>
      <td>POINT (750336.872 6273995.490)</td>
    </tr>
    <tr>
      <th>521</th>
      <td>2</td>
      <td>herbe</td>
      <td>524</td>
      <td>POINT (750641.804 6273889.907)</td>
    </tr>
    <tr>
      <th>522</th>
      <td>2</td>
      <td>herbe</td>
      <td>525</td>
      <td>POINT (750915.411 6273803.283)</td>
    </tr>
    <tr>
      <th>523</th>
      <td>2</td>
      <td>herbe</td>
      <td>526</td>
      <td>POINT (751125.608 6274036.404)</td>
    </tr>
  </tbody>
</table>
<p>524 rows × 4 columns</p>
</div>


Puis d'accéder à la colonne `geometry` du `GeoDataFrame` :

```python
gdf.loc[:,'geometry']
```

    0      POINT (748365.906 6276577.033)
    1      POINT (748285.153 6276558.825)
    2      POINT (748401.722 6276762.504)
    3      POINT (748325.353 6276728.435)
    4      POINT (748586.873 6276687.753)
                        ...              
    519    POINT (750142.636 6273994.320)
    520    POINT (750336.872 6273995.490)
    521    POINT (750641.804 6273889.907)
    522    POINT (750915.411 6273803.283)
    523    POINT (751125.608 6274036.404)
    Name: geometry, Length: 524, dtype: geometry


ou d'accéder de manière élémentaire à une géométrie en particulier :


```python
a_geometry = gdf.loc[0,'geometry']
print(a_geometry)
print(type(a_geometry))
```

    POINT (748365.9064233786 6276577.033433291)
    <class 'shapely.geometry.point.Point'>

et enfin de récupérer les coordonnées d'un point :


```python
print(list(a_geometry.coords))
x = a_geometry.x
y = a_geometry.y
```

    [(748365.9064233786, 6276577.033433291)]

Vous pouvez également récupérer l'ensemble des coordonnées x, y à partir de la `GeoSeries` :  

```python
print(gdf.loc[:,'geometry'].x)
print(gdf.loc[:,'geometry'].y)
```

    0      748365.906423
    1      748285.153170
    2      748401.721924
    3      748325.352732
    4      748586.873167
               ...      
    519    750142.635951
    520    750336.872357
    521    750641.804023
    522    750915.410987
    523    751125.607798
    Length: 524, dtype: float64
    0      6.276577e+06
    1      6.276559e+06
    2      6.276763e+06
    3      6.276728e+06
    4      6.276688e+06
               ...     
    519    6.273994e+06
    520    6.273995e+06
    521    6.273890e+06
    522    6.273803e+06
    523    6.274036e+06
    Length: 524, dtype: float64

**Remarque** : les coordonnées récupérées sont de type `Series` :


```python
print(type(gdf.loc[:,'geometry'].y))
```

    <class 'pandas.core.series.Series'>


Vous pouvez extraire les données brutes (tableau ==numpy==) comme suit :


```python
list_x = gdf.loc[:,'geometry'].x.values
list_y = gdf.loc[:,'geometry'].y.values
print(type(list_x))
```

    <class 'numpy.ndarray'>

Au passage, nous avons vu :

- qu'une géométrie était une objet de type `Point` de la librairie ==Shapely== ;
- qu'on pouvait indexer un `DataFrame` (et *a fortiori* un `GeoDataFrame`) à l'aide des instructions `gdf.loc[mes_lignes, mes_colonnes]`.



<div class="block green-block"><p class="title exercice"> </p>

Écrivez une fonction qui renvoie les coordonnées géographiques d'un fichier de points en utilisant ==Geopandas==.
<div class="question">
	<input type="checkbox" id="1.2">
	<p><label for="1.2">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import geopandas as gpd

  def get_xy_from_file(filename):
      """
      Get x y coordinates from a vector point file

      Parameters
      ----------
      filename : str
          Path of the vector point file

      Returns
      -------
      list_x : np.array
      list_y : np.array
      """
      gdf = gpd.read_file(filename)
      geometry = gdf.loc[:, 'geometry']
      list_x = geometry.x.values
      list_y = geometry.y.values

      return list_x, list_y
  ```
</span>
</div>

Dans la suite du TD, vous pourrez aussi accéder à cette fonction depuis le script `read_and_write.py`.

<br>
</div>

Nous ne verrons pas plus d'éléments de l'API de la librairie ==Shapely==. Vous pourrez cependant trouver une documentation détaillée des fonctionnalités de cette librairie [ici](https://shapely.readthedocs.io/en/stable/manual.html).

Maintenant que nous savons extraire une liste de coordonnées géographiques d'un fichier de points, nous pouvons définir une fonction `get_row_col_from_file` qui renvoie les coordonnées *image* de chaque point d'un fichier de points :

```python
import sys
sys.path.append('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/python_scripts')
import read_and_write as rw

image_filename = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/aumelas_orthoirc.tif'
point_filename = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/sample_strata_centroides.shp'
list_row, list_col = rw.get_row_col_from_file(point_filename, image_filename)
```

Cette fonction est disponible dans le script `read_and_write.py`

<div class="block green-block"><p class="title exercice"> (Facultatif)</p>

Bien que cette fonction vous soit déjà fournie, essayez d'écrire cette fonction et comparez la ensuite avec celle du script `read_and_write.py`.

<div class="question">
	<input type="checkbox" id="1.3">
	<p><label for="1.3">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  def get_row_col_from_file(point_file, image_file):
      """
      Get row/col image coordinates from a vector point file
      and an image file

      Parameters
      ----------
      point_file : str
          Path of the vector point file
      image_file : str
          Path of the raster image file

      Returns
      -------
      list_row : np.array
      list_col : np.array
      """
      list_row = []
      list_col = []
      list_x, list_y = get_xy_from_file(point_file)
      for x, y in zip(list_x, list_y):
          row, col = xy_to_rowcol(x, y, image_file)
          list_row.append(row)
          list_col.append(col)
      return list_row, list_col
  ```
</span>
</div>
<br>
</div>



##### Lecture des valeurs et formattage

Vous savez maintenant :

- extraire les coordonnées géographiques d'un fichier de points ;
- convertir ces coordonnées en coordonnées image.

Par ailleurs, nous avons vu dans les précédents TDs comment accéder aux valeurs d'un pixel d'une image à partir de ses coordonnées :


```python
import read_and_write as rw

image_filename = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_intro/data/Pleiades/fabas_12_10_2013_reordered.tif'
point_filename = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/sample_l93.shp'
list_row, list_col = rw.get_row_col_from_file(point_filename, image_filename)

image = rw.load_img_as_array(image_filename)
X = image[(list_row, list_col)]
print(X)
```

    [[ 261  193  116  579]
     [ 259  185  122  633]
     [ 248  160  100  356]
     ...
     [ 307  256  181 1015]
     [ 302  255  176  934]
     [ 290  235  155  993]]


**Remarque** : dans la suite du TD, nous aurons besoin de formatter les données selon le format suivant :

<figure>
	<img src="img/td_classification/tableau_numpy_echantillons.png" title="" width="75%">
	<figcaption>Format des matrices d'échantillons X et Y.</figcaption>
</figure>


Chaque ligne de la matrice `X` représente un point et chaque colonne représente une bande spectrale.

<div class="block green-block"><p class="title exercice"></p>

La matrice `X` de l'exemple ci-dessus est-elle bien formatée ? Vérifiez le formellement en python.
<div class="question">
	<input type="checkbox" id="1.4">
	<p><label for="1.4">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import geopandas as gpd

  data_set = rw.open_image(image_filename)
  image_filename = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/aumelas_orthoirc.tif'
  fichier_point = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/sample_strata_centroides.shp'
  gdf = gpd.read_file(fichier_point)

  print('Shape of X :', X.shape)
  print('Number of bandes :', rw.get_image_dimension(data_set)[2])
  print('Number of samples : ', gdf.shape[0] )
  ```


</span>
</div>
<br>
</div>


**Et à quoi correspond ce tableau `Y` dans la figure ?**

Si la matrice `X` correspond aux valeurs spectrales de chaque échantillon (donc aux variables à utiliser pour la classification), la matrice `Y` correspond aux labels de chaque échantillon, c'est à dire à quelle classe ils appartiennent.

**Et comment l'obtient-on ?**

<div class="block green-block"><p class="title exercice"> </p>

À vous de me le dire.

Indices :
- on l'obtient à partir du fichier d'échantillons ;
- attention au format en sortie (voir figure ci dessus).

<div class="question">
	<input type="checkbox" id="1.5">
	<p><label for="1.5">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import geopandas as gpd
  import numpy as np

  fichier_point = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/sample_strata_centroides.shp'
  gdf = gpd.read_file(fichier_point)
  Y = gdf.loc[:, 'num'].values
  Y = np.atleast_2d(Y).T
  ```
</span>
</div>
<br>
</div>

#### Classification à partir des échantillons "point"

Vous savez maintenant extraire les valeurs spectrales d'échantillons sous forme de points, et nous n'avons plus de problèmes liés à des pixels appartenant au même polygone.

<div class="block green-block"><p class="title exercice"></p>

Écrivez une chaîne de traitements pour en entraîner et valider un modèle de classification avec :
- séparation des échantillons en deux jeux d'entraînement et de validation
- en utilisant le jeu d'échantillons `sample_strata_centroides.shp`

<div class="question">
	<input type="checkbox" id="2">
	<p><label for="2">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import sys
  sys.path.append('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/python_scripts')

  import os
  import numpy as np
  from sklearn import tree
  from sklearn.model_selection import train_test_split
  from sklearn.metrics import confusion_matrix, classification_report, \
      accuracy_score
  import geopandas as gpd

  # personal librairies
  import classification as cla
  import read_and_write as rw
  import plots

  # 1 --- define parameters
  # inputs
  my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
  sample_filename = os.path.join(my_folder, 'sample_strata_centroides.shp')
  image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')

  # Sample parameters
  test_size = 0.7
  is_point = True
  # if is_point is True
  field_name = 'num'

  # outputs
  suffix = '_v2bis'
  out_classif = os.path.join(my_folder, 'ma_classif{}.tif'.format(suffix))
  out_matrix = os.path.join(my_folder, 'ma_matrice{}.png'.format(suffix))
  out_qualite = os.path.join(my_folder, 'mes_qualites{}.png'.format(suffix))


  # 2 --- extract samples
  if not is_point :
      X, Y, t = cla.get_samples_from_roi(image_filename, sample_filename)
      X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_size)
  else :
      # get X
      list_row, list_col = rw.get_row_col_from_file(sample_filename, image_filename)
      image = rw.load_img_as_array(image_filename)
      X = image[(list_row, list_col)]

      # get Y
      gdf = gpd.read_file(sample_filename)
      Y = gdf.loc[:, field_name].values
      Y = np.atleast_2d(Y).T

      # split sample
      X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_size)


  # 3 --- Train
  #clf = SVC(cache_size=6000)
  clf = tree.DecisionTreeClassifier(max_leaf_nodes=10)
  clf.fit(X_train, Y_train)

  # 4 --- Test
  Y_predict = clf.predict(X_test)

  # compute quality
  cm = confusion_matrix(Y_test, Y_predict)
  report = classification_report(Y_test, Y_predict, labels=np.unique(Y_predict), output_dict=True)
  accuracy = accuracy_score(Y_test, Y_predict)

  # display and save quality
  plots.plot_cm(cm, np.unique(Y_predict), out_filename=out_matrix)
  plots.plot_class_quality(report, accuracy, out_filename=out_qualite)

  ```

  <img src="img/td_classification_part2/output_19_1.png" title="" width="39%">

  <img src="img/td_classification_part2/output_19_2.png" title="" width="60%">

</span>
</div>
<br>
</div>


### Cas où peu d'échantillons sont disponibles

Lorsque vous avez pallié au problème des échantillons appartenant au même polygone en utilisant le fichier `sample_strata_centroides.shp`, vous avez diminué de fait le nombre d'échantillons disponibles pour l'entraînement et la validation. Il est alors légitime de se poser la question suivante : **les performances de mon modèle de classification sont-elles influencées par le résultat de la séparation de mon jeux de données en un jeu de validation et d'entraînement ?**

Pour répondre à cette question, je vous propose deux options :
- répéter plusieurs fois l'entraînement et la validation de votre modèle ;
- utiliser une validation croisée.

#### Répéter plusieurs fois la validation

Dans cette partie, je vous propose de répéter plusieurs fois l'entraînement et la validation de votre modèle avec des séparations différentes de votre jeu d'échantillons à chaque itération (voir figure ci-dessous) :

<figure>
	<img class="zoomable" src="img/td_classification_part2/classification_process_v3.png" title="" width="100%">
	<figcaption>Chaîne de traitements de classification avec validation du modèle sur plusieurs itérations.</figcaption>
</figure>

Les indices de qualité seront produits plusieurs fois, vous devrez les moyenner à la fin pour obtenir une estimation moyenne de la qualité de votre modèle de classification.

<div class="block green-block"><p class="title exercice"></p>

Vous disposez d'une fonction `report_from_dict_to_df` qui permet de convertir en `DataFrame` un dictionnaire retourné par la fonction `classification_report` :


```python
def report_from_dict_to_df(dict_report):

    # convert report into dataframe
    report_df = pd.DataFrame.from_dict(dict_report)

    # drop unnecessary rows and columns
    report_df = report_df.drop(['accuracy', 'macro avg', 'weighted avg'], axis=1)
    report_df = report_df.drop(['support'], axis=0)

    return report_df
```

1. Écrivez une série d'instructions pour répéter `nb_iter = 30` fois l'entraînement et la validation d'un modèle de classification ;
2. Stocker :
    - les matrices de confusion dans une liste `list_cm`,
    - les accords globaux dans une liste `list_accuracy`.
    - les rapport de classification sous forme de **`DataFrame`** dans une liste `list_report` .

<div class="question">
	<input type="checkbox" id="3">
	<p><label for="3">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import sys
  sys.path.append('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/python_scripts')

  import os
  import numpy as np
  from sklearn import tree
  from sklearn.model_selection import train_test_split
  from sklearn.metrics import confusion_matrix, classification_report, \
      accuracy_score
  import geopandas as gpd
  import pandas as pd

  # personal librairies
  import classification as cla
  import read_and_write as rw
  import plots

  # 1 --- define parameters
  # inputs
  my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
  sample_filename = os.path.join(my_folder, 'sample_strata_centroides.shp')
  image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')

  # Sample parameters
  test_size = 0.5
  is_point = True
  # if is_point is True
  field_name = 'num'

  # outputs
  suffix = '_v2'
  out_classif = os.path.join(my_folder, 'ma_classif{}.tif'.format(suffix))
  out_matrix = os.path.join(my_folder, 'ma_matrice{}.png'.format(suffix))
  out_qualite = os.path.join(my_folder, 'mes_qualites{}.png'.format(suffix))

  # 2 --- extract samples
  if not is_point :
      X, Y, t = cla.get_samples_from_roi(image_filename, sample_filename)
  else :
      # get X
      list_row, list_col = rw.get_row_col_from_file(sample_filename, image_filename)
      image = rw.load_img_as_array(image_filename)
      X = image[(list_row, list_col)]

      # get Y
      gdf = gpd.read_file(sample_filename)
      Y = gdf.loc[:, field_name].values
      Y = np.atleast_2d(Y).T

  list_cm = []
  list_accuracy = []
  list_report = []

  for i in range(30):  
      # Split sample
      X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_size)


      # 3 --- Train
      #clf = SVC(cache_size=6000)
      clf = tree.DecisionTreeClassifier()
      clf.fit(X_train, Y_train)

      # 4 --- Test
      Y_predict = clf.predict(X_test)

      # compute quality
      list_cm.append(confusion_matrix(Y_test, Y_predict))
      list_accuracy.append(accuracy_score(Y_test, Y_predict))
      report = classification_report(Y_test, Y_predict,

                                     labels=np.unique(Y_predict), output_dict=True)
      # store them
      list_report.append(report_from_dict_to_df(report))
  ```


</span>
</div>
<br>
</div>


Il faut ensuite calculer la moyenne et éventuellement l'écart type des différentes itérations avant de pouvoir les afficher.

##### Matrice de confusion

**Calcul**


```python
nb_iter = 30
# compute mean of cm
array_cm = np.array(list_cm)
mean_cm = array_cm.mean(axis=0)
```

**Affichage**


```python
plots.plot_cm(mean_cm, np.unique(Y_predict))
```

<figure>
	<img src="img/td_classification_part2/output_36_1.png" title="" width="40%">
	<figcaption>Moyenne des matrices de confusion, rappels, précisions et f-scores de 30 itérations.</figcaption>
</figure>

##### Calcul de l'Accord global moyen

```python
array_accuracy = np.array(list_accuracy)
mean_accuracy = array_accuracy.mean()
std_accuracy = array_accuracy.std()
```

##### Moyenne des rapports de classification

```python
array_report = np.array(list_report)
mean_report = array_report.mean(axis=0)
std_report = array_report.std(axis=0)
```

#####  Affichage des qualités

Nous allons reconvertir les tableaux ==Numpy== en `DataFrame` de ==Pandas== pour afficher plus facilement les résultats :

```python
mean_df_report = pd.DataFrame(mean_report)
mean_df_report
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.967300</td>
      <td>0.949742</td>
      <td>0.661429</td>
      <td>0.615652</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.960181</td>
      <td>0.954622</td>
      <td>0.658473</td>
      <td>0.616680</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0.963369</td>
      <td>0.951792</td>
      <td>0.656258</td>
      <td>0.611690</td>
    </tr>
  </tbody>
</table>
</div>

Au passage, vous avez vu comment créer un `DataFrame` à partir d'un tableau ==Numpy==. Rajoutons maintenant les index à partir des précédents `DataFrame`.

```python
a_report = list_report[0]
an_index = a_report.index
print(an_index)
a_column_index = a_report.columns
print(a_column_index)
mean_df_report = pd.DataFrame(mean_report, index=an_index,
                              columns=a_column_index)
mean_df_report
```

    Index(['precision', 'recall', 'f1-score'], dtype='object')
    Index(['1', '2', '3', '4'], dtype='object')


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>precision</th>
      <td>0.967300</td>
      <td>0.949742</td>
      <td>0.661429</td>
      <td>0.615652</td>
    </tr>
    <tr>
      <th>recall</th>
      <td>0.960181</td>
      <td>0.954622</td>
      <td>0.658473</td>
      <td>0.616680</td>
    </tr>
    <tr>
      <th>f1-score</th>
      <td>0.963369</td>
      <td>0.951792</td>
      <td>0.656258</td>
      <td>0.611690</td>
    </tr>
  </tbody>
</table>
</div>

Faisons la même chose pour l'écart type :

```python
a_report = list_report[0]
an_index = a_report.index
print(an_index)
a_column_index = a_report.columns
print(a_column_index)
std_df_report = pd.DataFrame(std_report, index=an_index,
                            columns=a_column_index)
std_df_report
```

    Index(['precision', 'recall', 'f1-score'], dtype='object')
    Index(['1', '2', '3', '4'], dtype='object')

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>precision</th>
      <td>0.025745</td>
      <td>0.022252</td>
      <td>0.053467</td>
      <td>0.065903</td>
    </tr>
    <tr>
      <th>recall</th>
      <td>0.021208</td>
      <td>0.032486</td>
      <td>0.080829</td>
      <td>0.077525</td>
    </tr>
    <tr>
      <th>f1-score</th>
      <td>0.014853</td>
      <td>0.020531</td>
      <td>0.047793</td>
      <td>0.049819</td>
    </tr>
  </tbody>
</table>
</div>

Enfin, affichons les résultats :

```python
import matplotlib.pyplot as plt
fig, ax = plt.subplots(figsize=(10, 7))
ax = mean_df_report.T.plot.bar(ax=ax, zorder=2)
ax.set_ylim(0.5, 1)
_ = ax.text(1.5, 0.95, 'OA : {:.2f} +- {:.2f}'.format(mean_accuracy,
                                                      std_accuracy),
            fontsize=14)
```

<figure>
	<img src="img/td_classification_part2/output_49_0.png" title="" width="60%">
	<figcaption>Moyennes des précisions, rappels et f-scores sur 30 itérations.</figcaption>
</figure>


En rajoutant des barres d'erreurs correspondant aux écart-types :

```python
import matplotlib.pyplot as plt
fig, ax = plt.subplots(figsize=(10, 7))
ax = mean_df_report.T.plot.bar(ax=ax, yerr=std_df_report.T, zorder=2)
ax.set_ylim(0.5, 1)
_ = ax.text(1.5, 0.95, 'OA : {:.2f} +- {:.2f}'.format(mean_accuracy,
                                                      std_accuracy),
            fontsize=14)
```

<figure>
	<img src="img/td_classification_part2/output_51_0.png" title="" width="60%">
	<figcaption>Moyennes et écart-types des précisions, rappels et f-scores sur 30 itérations.</figcaption>
</figure>

En rajoutant quelques élements graphiques :

```python
import matplotlib.pyplot as plt
fig, ax = plt.subplots(figsize=(10, 7))
ax = mean_df_report.T.plot.bar(ax=ax, yerr=std_df_report.T, zorder=2)
ax.set_ylim(0.5, 1)
_ = ax.text(1.5, 0.95, 'OA : {:.2f} +- {:.2f}'.format(mean_accuracy,
                                                      std_accuracy),
            fontsize=14)
ax.set_title('Class quality estimation')

# custom : cuteness
# background color
ax.set_facecolor('ivory')
# labels
x_label = ax.get_xlabel()
ax.set_xlabel(x_label, fontdict={'fontname': 'Sawasdee'}, fontsize=14)
y_label = ax.get_ylabel()
ax.set_ylabel(y_label, fontdict={'fontname': 'Sawasdee'}, fontsize=14)
# borders
ax.spines["top"].set_visible(False)
ax.spines["right"].set_visible(False)
ax.spines["bottom"].set_visible(False)
ax.spines["left"].set_visible(False)
ax.tick_params(axis='x', colors='darkslategrey', labelsize=14)
ax.tick_params(axis='y', colors='darkslategrey', labelsize=14)
# grid
ax.minorticks_on()
ax.yaxis.grid(which='major', color='darkgoldenrod', linestyle='--',
              linewidth=0.5, zorder=1)
ax.yaxis.grid(which='minor', color='darkgoldenrod', linestyle='-.',
              linewidth=0.3, zorder=1)
```

<figure>
	<img src="img/td_classification_part2/output_53_0.png" title="" width="60%">
	<figcaption>Moyennes et écart-types des précisions, rappels et f-scores sur 30 itérations, avec personnalisation de l'arrière plan.</figcaption>
</figure>


<div class="block green-block"><p class="title exercice"></p>

Ecrivez une série d'instructions qui permette de répéter plusieurs fois l'entraînement et la validation et d'afficher la synthèse des résultats des résultats (compilation des instructions vues ci-dessus).

<div class="question">
	<input type="checkbox" id="4">
	<p><label for="4">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import sys
  sys.path.append('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/python_scripts')

  import os
  import numpy as np
  from sklearn import tree
  from sklearn.model_selection import train_test_split
  from sklearn.metrics import confusion_matrix, classification_report, \
      accuracy_score
  import geopandas as gpd
  import pandas as pd

  # personal librairies
  import classification as cla
  import read_and_write as rw
  import plots

  # 1 --- define parameters
  # inputs
  my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
  sample_filename = os.path.join(my_folder, 'sample_strata_centroides.shp')
  image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')

  # Sample parameters
  test_size = 0.5
  is_point = True
  # if is_point is True
  field_name = 'num'

  # outputs
  suffix = '_v3'
  out_classif = os.path.join(my_folder, 'ma_classif{}.tif'.format(suffix))
  out_matrix = os.path.join(my_folder, 'ma_matrice{}.png'.format(suffix))
  out_qualite = os.path.join(my_folder, 'mes_qualites{}.png'.format(suffix))

  # 2 --- extract samples
  if not is_point :
      X, Y, t = cla.get_samples_from_roi(image_filename, sample_filename)
  else :
      # get X
      list_row, list_col = rw.get_row_col_from_file(sample_filename, image_filename)
      image = rw.load_img_as_array(image_filename)
      X = image[(list_row, list_col)]

      # get Y
      gdf = gpd.read_file(sample_filename)
      Y = gdf.loc[:, field_name].values
      Y = np.atleast_2d(Y).T

  list_cm = []
  list_accuracy = []
  list_report = []

  for i in range(30):  
      # Split sample
      X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_size)


      # 3 --- Train
      #clf = SVC(cache_size=6000)
      clf = tree.DecisionTreeClassifier(max_leaf_nodes=10)
      clf.fit(X_train, Y_train)

      # 4 --- Test
      Y_predict = clf.predict(X_test)

      # compute quality
      list_cm.append(confusion_matrix(Y_test, Y_predict))
      list_accuracy.append(accuracy_score(Y_test, Y_predict))
      report = classification_report(Y_test, Y_predict,

                                     labels=np.unique(Y_predict), output_dict=True)
      # store them
      list_report.append(report_from_dict_to_df(report))


  # compute mean of cm
  array_cm = np.array(list_cm)
  mean_cm = array_cm.mean(axis=0)

  # compute mean and std of overall accuracy
  array_accuracy = np.array(list_accuracy)
  mean_accuracy = array_accuracy.mean()
  std_accuracy = array_accuracy.std()

  # compute mean and std of classification report
  array_report = np.array(list_report)
  mean_report = array_report.mean(axis=0)
  std_report = array_report.std(axis=0)
  a_report = list_report[0]
  mean_df_report = pd.DataFrame(mean_report, index=a_report.index,
                                columns=a_report.columns)
  std_df_report = pd.DataFrame(std_report, index=a_report.index,
                               columns=a_report.columns)

  # Display confusion matrix
  plots.plot_cm(mean_cm, np.unique(Y_predict))
  plt.savefig(out_matrix, bbox_inches='tight')


  # Display class metrics
  fig, ax = plt.subplots(figsize=(10, 7))
  ax = mean_df_report.T.plot.bar(ax=ax, yerr=std_df_report.T, zorder=2)
  ax.set_ylim(0.5, 1)
  _ = ax.text(1.5, 0.95, 'OA : {:.2f} +- {:.2f}'.format(mean_accuracy,
                                                        std_accuracy),
              fontsize=14)
  ax.set_title('Class quality estimation')

  # custom : cuteness
  # background color
  ax.set_facecolor('ivory')
  # labels
  x_label = ax.get_xlabel()
  ax.set_xlabel(x_label, fontdict={'fontname': 'Sawasdee'}, fontsize=14)
  y_label = ax.get_ylabel()
  ax.set_ylabel(y_label, fontdict={'fontname': 'Sawasdee'}, fontsize=14)
  # borders
  ax.spines["top"].set_visible(False)
  ax.spines["right"].set_visible(False)
  ax.spines["bottom"].set_visible(False)
  ax.spines["left"].set_visible(False)
  ax.tick_params(axis='x', colors='darkslategrey', labelsize=14)
  ax.tick_params(axis='y', colors='darkslategrey', labelsize=14)
  # grid
  ax.minorticks_on()
  ax.yaxis.grid(which='major', color='darkgoldenrod', linestyle='--',
                linewidth=0.5, zorder=1)
  ax.yaxis.grid(which='minor', color='darkgoldenrod', linestyle='-.',
                linewidth=0.3, zorder=1)
  plt.savefig(out_qualite, bbox_inches='tight')
  ```


  <img src="img/td_classification_part2/output_55_1.png" title="" width="37%">

  <img src="img/td_classification_part2/output_55_2.png" title="" width="59%">

</span>
</div>
<br>
</div>


#### Cross validation

Dans cette partie, je vous propose de répéter plusieurs fois l'entraînement et la validation de votre modèle avec des séparations de votre jeux d'échantillons différentes à chaque itération :

<figure>
	<img class="zoomable" src="img/td_classification_part2/classification_process_v4.png" title="" width="100%">
	<figcaption>Porcessus de classifications avec validation croisée.</figcaption>
</figure>

Cette fois-ci nous n'allons pas séparer les échantillons de manière tout à fait aléatoire. Nous allons créer $k$ paires de jeux de données complémentaires pour faire une validation croisée.

Le principe est de diviser les échantillons en $k$ groupes (appelés *folds* en anglais) de taille égale. Le modèle est entraîné sur $k-1$ *folds* et le *fold* restant est utilisé pour la validation. Le processus est alors répété $k$ fois en utilisant un *fold* différent à chaque fois pour la validation. Vous pouvez voir ci-dessous un exemple issu de la documentation de ==Scikit-learn== où $k=4$ pour trois classes à classer.

<figure>
	<img src="img/td_classification_part2/k_fold.png" title="" width="60%">
	<figcaption>Répartition des échantillons en 4 folds. La proportion des classes dans chaque fold ne correspond pas à la proportion initiale.</figcaption>
</figure>


On peut remarquer dans cet exemple que le label des classes n'est pas pris en compte lors de la création des $k$ *folds*. Il est alors possible de se retrouver avec des jeux de validation qui contiennent peu voire aucun échantillon d'une classe (surtout si une classe contient peu d'échantillons à la base). Il est dans ce cas possible d'imposer que la proportion initiale de chaque classe soit conservée dans les *folds*. Vous pouvez voir ci-dessous un exemple issu de la documentation de ==Scikit-learn== où $k=4$ pour trois classes à classer.

<figure>
	<img src="img/td_classification_part2/stratified_k_fold.png" title="" width="60%">
	<figcaption>Répartition des échantillons en 4 folds. La proportion des classes dans chaque fold correspond bien à la proportion initiale.</figcaption>
</figure>


C'est cette dernière méthode que nous allons tester. Nous allons pour cela utiliser la classe `StratifiedKFold` du module `model_selection`. Elle s'utilise comme suit :

```python
from sklearn.model_selection import StratifiedKFold, KFold
import numpy as np


X, Y = np.ones((18, 1)), np.hstack(([0] * 12, [5] * 6))
print('X :', X.T)
print('Y :', Y)

# Intiatilise
skf = StratifiedKFold(n_splits=3)
# Iterations on skf
for index_train, index_test in skf.split(X, Y):
    print(index_test)
    X_train, X_test = X[index_train], X[index_test]
    Y_train, Y_test = Y[index_train], Y[index_test]

    # train
    # ....
    # test
    # ....
    # etc...
```

    X : [[1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1. 1.]]
    Y : [0 0 0 0 0 0 0 0 0 0 0 0 5 5 5 5 5 5]
    [ 0  1  2  3 12 13]
    [ 4  5  6  7 14 15]
    [ 8  9 10 11 16 17]

On ne sépare pas directement les matrices `X` et `Y` en plusieurs *folds* mais on génère les indices qui permettront d'obtenir de manière itérative les différents *folds*.

<div class="block green-block"><p class="title exercice"></p>

- Implémentez cette nouvelle stratégie de validation en stockant comme précédemment les indices de qualité au fur et à mesure qu'ils sont produits ;
- calculez ensuite la synthèse de ces indices et affichez les résultats sous forme de graphique ;
- commentez vos résultats.

<div class="question">
	<input type="checkbox" id="5">
	<p><label for="5">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import sys
  sys.path.append('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/python_scripts')

  import os
  import numpy as np
  from sklearn import tree
  from sklearn.model_selection import train_test_split, KFold, StratifiedKFold
  from sklearn.metrics import confusion_matrix, classification_report, \
      accuracy_score
  import geopandas as gpd

  # personal librairies
  import classification as cla
  import read_and_write as rw
  import plots

  # 1 --- define parameters
  # inputs
  my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
  sample_filename = os.path.join(my_folder, 'sample_strata_centroides.shp')
  image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')

  # Sample parameters
  #test_size = 0.7
  nb_iter =5
  is_point = True
  # if is_point is True
  field_name = 'num'

  # outputs
  suffix = '_v4'
  out_classif = os.path.join(my_folder, 'ma_classif{}.tif'.format(suffix))
  out_matrix = os.path.join(my_folder, 'ma_matrice{}.png'.format(suffix))
  out_qualite = os.path.join(my_folder, 'mes_qualites{}.png'.format(suffix))


  # 2 --- extract samples
  if not is_point :
      X, Y, t = cla.get_samples_from_roi(image_filename, sample_filename)
  else :
      # get X
      list_row, list_col = rw.get_row_col_from_file(sample_filename, image_filename)
      image = rw.load_img_as_array(image_filename)
      X = image[(list_row, list_col)]

      # get Y
      gdf = gpd.read_file(sample_filename)
      Y = gdf.loc[:, field_name].values
      Y = np.atleast_2d(Y).T

  list_cm = []
  list_accuracy = []
  list_report = []

  # Iter on stratified K fold
  kf = StratifiedKFold(n_splits=nb_iter)
  for train, test in kf.split(X, Y):
      X_train, X_test = X[train], X[test]
      Y_train, Y_test = Y[train], Y[test]

      # 3 --- Train
      #clf = SVC(cache_size=6000)
      clf = tree.DecisionTreeClassifier()
      clf.fit(X_train, Y_train)

      # 4 --- Test
      Y_predict = clf.predict(X_test)

      # compute quality
      list_cm.append(confusion_matrix(Y_test, Y_predict))
      list_accuracy.append(accuracy_score(Y_test, Y_predict))
      report = classification_report(Y_test, Y_predict,

                                     labels=np.unique(Y_predict), output_dict=True)

      # store them
      list_report.append(report_from_dict_to_df(report))



  # compute mean of cm
  array_cm = np.array(list_cm)
  mean_cm = array_cm.mean(axis=0)

  # compute mean and std of overall accuracy
  array_accuracy = np.array(list_accuracy)
  mean_accuracy = array_accuracy.mean()
  std_accuracy = array_accuracy.std()

  # compute mean and std of classification report
  array_report = np.array(list_report)
  mean_report = array_report.mean(axis=0)
  std_report = array_report.std(axis=0)
  a_report = list_report[0]
  mean_df_report = pd.DataFrame(mean_report, index=a_report.index,
                                columns=a_report.columns)
  std_df_report = pd.DataFrame(std_report, index=a_report.index,
                               columns=a_report.columns)

  # Display confusion matrix
  plots.plot_cm(mean_cm, np.unique(Y_predict))
  plt.savefig(out_matrix, bbox_inches='tight')

  # Display class metrics
  fig, ax = plt.subplots(figsize=(10, 7))
  ax = mean_df_report.T.plot.bar(ax=ax, yerr=std_df_report.T, zorder=2)
  ax.set_ylim(0.5, 1)
  _ = ax.text(1.5, 0.95, 'OA : {:.2f} +- {:.2f}'.format(mean_accuracy,
                                                        std_accuracy),
              fontsize=14)
  ax.set_title('Class quality estimation')

  # custom : cuteness
  # background color
  ax.set_facecolor('ivory')
  # labels
  x_label = ax.get_xlabel()
  ax.set_xlabel(x_label, fontdict={'fontname': 'Sawasdee'}, fontsize=14)
  y_label = ax.get_ylabel()
  ax.set_ylabel(y_label, fontdict={'fontname': 'Sawasdee'}, fontsize=14)
  # borders
  ax.spines["top"].set_visible(False)
  ax.spines["right"].set_visible(False)
  ax.spines["bottom"].set_visible(False)
  ax.spines["left"].set_visible(False)
  ax.tick_params(axis='x', colors='darkslategrey', labelsize=14)
  ax.tick_params(axis='y', colors='darkslategrey', labelsize=14)
  # grid
  ax.minorticks_on()
  ax.yaxis.grid(which='major', color='darkgoldenrod', linestyle='--',
                linewidth=0.5, zorder=1)
  ax.yaxis.grid(which='minor', color='darkgoldenrod', linestyle='-.',
                linewidth=0.3, zorder=1)
  plt.savefig(out_qualite, bbox_inches='tight')
  ```

  <img src="img/td_classification_part2/output_69_1.png" title="" width="39%">

  <img src="img/td_classification_part2/output_69_2.png" title="" width="60%">

</span>
</div>
<br>
</div>


#### Et la classification dans tout ça ?

Vous avez vu dans la précédente partie deux manières d'estimer la qualité d'une classification lorsqu'on possède peu d'échantillons, mais ce genre de stratégie est tout à fait valable lorsque vous en possédez beaucoup.

Dans tous les cas, vous pouvez produire une carte maintenant que vous avez estimé sa (future) qualité. Vous pouvez d'ailleurs utiliser tous les échantillons à votre disposition pour entraîner le model. Vous obtiendrez une carte très proche de celle produite dans le précédent TD, mais avec une meilleure estimation de sa qualité.


## Regroupement de classes (compromis entre précision thématique et statistique)

Une possibilité pour améliorer votre classification est de regrouper des classes qui ont beaucoup de confusion entre elles *a posteriori*. Dans notre cas, la classe "Ligneux bas" (classe 3) et "Ligneux hauts" ont beaucoup de confusion (classe 4).

### Évaluation de la qualité

Pour évaluer la qualité d'une classification, vous comparez une matrice de référence `Y_test` avec une matrice `Y_predict` contenant les labels prédits par un modèle. Pour évaluer l'effet d'un regroupement de classe sur la qualité d'une classification il suffit de remplacer les valeurs de `Y_test` et `Y_predict` par celles souhaitées puis de comparer les deux matrices.

<div class="block green-block"><p class="title exercice"></p>

Évaluez l'effet du regroupement de la classe 4 dans la classe 3 (il vous suffit de changer 3-4 lignes de codes dans votre script de classification de la partie précédente).


<div class="question">
	<input type="checkbox" id="6">
	<p><label for="6">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import sys
  sys.path.append('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/python_scripts')

  import os
  import numpy as np
  from sklearn import tree
  from sklearn.model_selection import train_test_split, KFold, StratifiedKFold
  from sklearn.metrics import confusion_matrix, classification_report, \
      accuracy_score
  import geopandas as gpd

  # personal librairies
  import classification as cla
  import read_and_write as rw
  import plots

  # 1 --- define parameters
  # inputs
  my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
  sample_filename = os.path.join(my_folder, 'sample_strata_centroides.shp')
  image_filename = os.path.join(my_folder, 'aumelas_orthoirc.tif')

  # Sample parameters
  #test_size = 0.7
  nb_iter =5
  is_point = True
  # if is_point is True
  field_name = 'num'

  # outputs
  suffix = '_v4_regroup'
  out_classif = os.path.join(my_folder, 'ma_classif{}.tif'.format(suffix))
  out_matrix = os.path.join(my_folder, 'ma_matrice{}.png'.format(suffix))
  out_qualite = os.path.join(my_folder, 'mes_qualites{}.png'.format(suffix))


  # 2 --- extract samples
  if not is_point :
      X, Y, t = cla.get_samples_from_roi(image_filename, sample_filename)
  else :
      # get X
      list_row, list_col = rw.get_row_col_from_file(sample_filename, image_filename)
      image = rw.load_img_as_array(image_filename)
      X = image[(list_row, list_col)]

      # get Y
      gdf = gpd.read_file(sample_filename)
      Y = gdf.loc[:, field_name].values
      Y = np.atleast_2d(Y).T

  list_cm = []
  list_accuracy = []
  list_report = []

  # Iter on stratified K fold
  kf = StratifiedKFold(n_splits=nb_iter)
  for train, test in kf.split(X, Y):
      X_train, X_test = X[train], X[test]
      Y_train, Y_test = Y[train], Y[test]

      # 3 --- Train
      #clf = SVC(cache_size=6000)
      clf = tree.DecisionTreeClassifier()
      clf.fit(X_train, Y_train)

      # 4 --- Test
      Y_predict = clf.predict(X_test)

      Y_predict[Y_predict == 4] = 3
      Y_test[Y_test == 4] = 3
      # compute quality
      list_cm.append(confusion_matrix(Y_test, Y_predict))
      list_accuracy.append(accuracy_score(Y_test, Y_predict))
      report = classification_report(Y_test, Y_predict,

                                     labels=np.unique(Y_predict), output_dict=True)

      # store them
      list_report.append(report_from_dict_to_df(report))



  # compute mean of cm
  array_cm = np.array(list_cm)
  mean_cm = array_cm.mean(axis=0)

  # compute mean and std of overall accuracy
  array_accuracy = np.array(list_accuracy)
  mean_accuracy = array_accuracy.mean()
  std_accuracy = array_accuracy.std()

  # compute mean and std of classification report
  array_report = np.array(list_report)
  mean_report = array_report.mean(axis=0)
  std_report = array_report.std(axis=0)
  a_report = list_report[0]
  mean_df_report = pd.DataFrame(mean_report, index=a_report.index,
                                columns=a_report.columns)
  std_df_report = pd.DataFrame(std_report, index=a_report.index,
                               columns=a_report.columns)

  # Display confusion matrix
  plots.plot_cm(mean_cm, np.unique(Y_predict))
  plt.savefig(out_matrix, bbox_inches='tight')

  # Display class metrics
  fig, ax = plt.subplots(figsize=(10, 7))
  ax = mean_df_report.T.plot.bar(ax=ax, yerr=std_df_report.T, zorder=2)
  ax.set_ylim(0.5, 1)
  _ = ax.text(1.5, 0.95, 'OA : {:.2f} +- {:.2f}'.format(mean_accuracy,
                                                        std_accuracy),
              fontsize=14)
  ax.set_title('Class quality estimation')

  # custom : cuteness
  # background color
  ax.set_facecolor('ivory')
  # labels
  x_label = ax.get_xlabel()
  ax.set_xlabel(x_label, fontdict={'fontname': 'Sawasdee'}, fontsize=14)
  y_label = ax.get_ylabel()
  ax.set_ylabel(y_label, fontdict={'fontname': 'Sawasdee'}, fontsize=14)
  # borders
  ax.spines["top"].set_visible(False)
  ax.spines["right"].set_visible(False)
  ax.spines["bottom"].set_visible(False)
  ax.spines["left"].set_visible(False)
  ax.tick_params(axis='x', colors='darkslategrey', labelsize=14)
  ax.tick_params(axis='y', colors='darkslategrey', labelsize=14)
  # grid
  ax.minorticks_on()
  ax.yaxis.grid(which='major', color='darkgoldenrod', linestyle='--',
                linewidth=0.5, zorder=1)
  ax.yaxis.grid(which='minor', color='darkgoldenrod', linestyle='-.',
                linewidth=0.3, zorder=1)
  plt.savefig(out_qualite, bbox_inches='tight')
  ```

  <img src="img/td_classification_part2/output_80_1.png" title="" width="39%">

  <img src="img/td_classification_part2/output_80_2.png" title="" width="60%">

</span>
</div>
<br>
</div>


### Regroupement de classe d'une image classifiée

<div class="block green-block"><p class="title exercice"></p>


Maintenant que vous connaissez l'effet du regroupement sur la qualité de votre classification, visualisez le résultat :

1. Chargez l'image `ma_premiere_classif_scikit.tif` sous forme de tableau ;
2. Effectuez les changements grace à numpy ;
3. Écrivez le résultat dans une image `ma_premiere_classif_scikit_regroup.tif`

<div class="question">
	<input type="checkbox" id="8">
	<p><label for="8">
	Solution
	</label></p>
	<span class="reponse">

  ```python
  import sys
  sys.path.append('/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/python_scripts')

  import os
  import numpy as np
  # personal librairies
  import classification as cla
  import read_and_write as rw
  import plots

  # 1 --- define parameters
  # inputs
  my_folder = '/home/terudel/Documents/cours/2020-2021/SIGMA/teledec_avance/data/aumelas/'
  classif_filename = os.path.join(my_folder, 'ma_premiere_classif_scikit.tif')
  out_regoup_filename = os.path.join(my_folder, 'ma_premiere_classif_scikit_regroup.tif')

  classif = rw.load_img_as_array(classif_filename)
  classif[classif==4] = 3
  ds = rw.open_image(classif_filename)
  rw.write_image(out_regoup_filename, classif, data_set=ds)
  ```

</span>
</div>
<br>
</div>

<br><br><br>
